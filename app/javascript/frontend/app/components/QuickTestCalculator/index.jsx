import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { is, map, addIndex } from 'ramda';
import 'whatwg-fetch';
import Toggler from '../kit/Toggler';
import WarehouseToggler from '../kit/WarehouseToggler';

const POOL_TYPES = [{ id: 'pool', title: 'Pool' }, { id: 'spa', title: 'Spa' }];
const CHEMICAL_TYPES = [{ id: 'Chlorine', title: 'Chlorine' }, { id: 'Bromine', title: 'Bromine' }, { id: 'Salt Water', title: 'Saltwater' }];
const CHEMICAL_WAREHOUS_TYPES = [
  {
    id: 'Sanitizer',
    name: 'Sanitizer',
    items: [
      { id: 'bleach6', name: '6% Bleach' },
      { id: 'bleach8', name: '8% Bleach' },
      { id: 'bleach12', name: '12% Bleach' },
      { id: 'trichlor3', name: 'Trichlor 3" tablets' },
      { id: 'trichlor2', name: 'Trichlor 2" tablets' },
      { id: 'bromine1', name: 'Bromine 1" tablets' },
      { id: 'dichlor', name: 'Dichlor 56%' },
      { id: 'dichlor62', name: 'Dichlor 62%' },
      { id: 'calhypo53', name: 'Cal-hypo 53%' },
      { id: 'calhypo65', name: 'Cal-hypo 65%' },
      { id: 'calhypo73', name: 'Cal-hypo 73%' },
      { id: 'lithiumhypo', name: 'lithium-hypo' },
    ]
  },
  {
    id: 'pH Increaser',
    name: 'pH Increaser',
    items: [
      { id: 'sodaash', name: 'Soda Ash' },
      { id: 'borax', name: 'Borax' },
      { id: 'bakingsoda', name: 'Baking Soda' },
    ],
  },
  {
    id: 'pH Reducer',
    name: 'pH Reducer',
    items: [
      { id: 'muriatic31', name: 'Muriatic Acid 31.45%' },
      { id: 'muriatic15', name: 'Muriatic Acid 15.725%' },
      { id: 'dryacid', name: 'Dry Acid' },
    ],
  },
  {
    id: 'Alkalinity Increaser',
    name: 'Alkalinity Increaser',
    items: [
      { id: 'sodium', name: 'Sodium bicarbonate' },
    ],
  },
  {
    id: 'Calcium Hardness Increaser',
    name: 'Calcium Hardness Increaser',
    items: [
      { id: 'calcium', name: 'Calcium chloride' },
    ],
  },
  {
    id: 'Stabilizer',
    name: 'Stabilizer',
    items: [
      { id: 'cyanuricsolid', name: 'Cyanuric Acid Solid' },
      { id: 'cyanuricliquid', name: 'Cyanuric Acid Liquid' },
    ],
  },
];

const urlParams = (params) => {
  const esc = encodeURIComponent;
  return Object.keys(params)
      .map(k => `${esc(k)}=${esc(params[k])}`)
      .join('&');
};

const CalculatorTitle = () => (
  <div className="section pure-u-1">
    <div className="section-subtitle">Pool & Spa Calculator ssa</div>
    <div className="section-title">Quick Test</div>
  </div>
);

class QuickTestCalculator extends Component {
  static propTypes = {
    name: PropTypes.string
  };

  static defaultProps = {
    name: 'David',
  };

  constructor(props, context) {
    super(props, context);
    this.state = { recommendations: [], loading: false, pool: null, chemical: null, ph: null, fcl: null, ta: null, sanitizerAddIn: null, fclAddIn: null, taAddIn: null, volume: null, warehouse: null };
    this.onValueChange = this.onValueChange.bind(this);
    this.getRecommendations = this.getRecommendations.bind(this);
    this.renderRecommendations = this.renderRecommendations.bind(this);
    this.onWarehouseChange = this.onWarehouseChange.bind(this);
  }

  onValueChange(name, type = 'text') {
    return (item) => {
      let value = null;
      if (type === 'text') {
        value = item.target.value;
      } else {
        value = is(Object, item) ? item.id : item;
      }
      this.setState({ [name]: value });
    };
  }

  onWarehouseChange(warehouse) {
    this.setState({ warehouse });
  }

  getRecommendations() {
    const { ph, pool, volume, fcl, ta, chemical, warehouse } = this.state;
    const values = { ph, pool, volume, fcl, ta, chemical, warehouse };

    fetch(`/quick_test/test.json`, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(values),
    })
        .then((response) => response.json())
        .then(({ data, message }) => {
          const { messages } = data;
          const recommendations = messages ? messages : [message];
          this.setState({ recommendations });
        }).catch((ex) => {
          alert(ex);
        });
  }

  renderRecommendations() {
    const { recommendations } = this.state;
    const hasRecommendations = recommendations.length > 0;
    const text = addIndex(map)((r, index) => <div key={index}>{ r }</div>, recommendations);
    return hasRecommendations && (
      <div>
        <div>Recommended Chemical Dosing</div>
        <hr />
        <div>{ text }</div>
      </div>
    );
  }

  render() {
    console.info(this.state);
    return (
      <div className="pure-g">
        <CalculatorTitle />
        <div className="section pure-u-1">
          <Toggler items={POOL_TYPES} onItemSelect={this.onValueChange('pool', 'toggle')} />
        </div>
        <div className="section pure-u-1">
          <div className="section-label">Pool/ Spa Gallonage</div>
          <div className="pure-g">
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="50,000 gallons" onChange={this.onValueChange('volume', 'text')} />
            </div>
          </div>
        </div>
        <div className="section pure-u-1">
          <Toggler items={CHEMICAL_TYPES} onItemSelect={this.onValueChange('chemical', 'toggle')} />
        </div>
        <div className="section">
          <hr />
        </div>
        <div className="section pure-u-1">
          <div className="section-label">Chemical Warehouse</div>
          <div className="pure-g">
            <div className="pure-u-1">
              <WarehouseToggler items={CHEMICAL_WAREHOUS_TYPES} onChange={this.onWarehouseChange} />
            </div>
          </div>
        </div>
        <div className="section">
          <hr />
        </div>
        <div className="section pure-u-1">
          <div className="section-label">Test Details</div>
          <div className="pure-g">
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="pH" onChange={this.onValueChange('ph', 'text')} />
            </div>
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="fcl" onChange={this.onValueChange('fcl', 'text')} />
            </div>
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="TA" onChange={this.onValueChange('ta', 'text')} />
            </div>
          </div>
        </div>
        <div className="section">
          <hr />
        </div>
        <div className="section pure-u-1">
          <div className="section-label">Extras</div>
          <div className="pure-g">
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="Sanitizer	Add-in" disabled onChange={this.onValueChange('sanitizerAddIn', 'text')} />
            </div>
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="FCL Add-in" disabled onChange={this.onValueChange('fclAddIn', 'text')} />
            </div>
            <div className="pure-u-1-3 pure-col">
              <input type="text" className="input" placeholder="TA Add-in" disabled onChange={this.onValueChange('taAddIn', 'text')} />
            </div>
          </div>
        </div>
        <div className="section">
          <hr />
        </div>
        <div className="section pure-u-1">
          <button className="button" onClick={this.getRecommendations}>Get Chemical Recommendation</button>
        </div>
        <div className="section">
          <hr />
        </div>
        <div className="section pure-u-1">
          { this.renderRecommendations() }
        </div>
      </div>
    );
  }
}

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<QuickTestCalculator name="React" />, document.getElementById('quick-test-calculator'));
});

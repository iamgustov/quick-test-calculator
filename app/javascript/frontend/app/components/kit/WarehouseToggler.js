import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { map, addIndex, find, filter } from 'ramda';

const mapWithIndex = addIndex(map);

export default class WarehouseToggler extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    onChange: (rows) => {},
  };

  constructor(props, context) {
    super(props, context);
    this.renderRow = this.renderRow.bind(this);
    this.onAddNewWarehouse = this.onAddNewWarehouse.bind(this);
    this.onRemoveWarehouse = this.onRemoveWarehouse.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
    this.onRowsChanged = this.onRowsChanged.bind(this);
    this.state = { rows: [] };
  }

  onRowsChanged(rows) {
    const pure = filter((row) => row.category && row.chemical && row.value, rows);
    this.setState({ rows });
    this.props.onChange(pure);
  }

  onValueChange(index, field) {
    return (event) => {
      const { rows } = this.state;
      const row = rows[index];
      row[field] = event.target.value;
      if (field === 'category') {
        row.chemical = undefined;
        row.value = undefined;
      }
      this.onRowsChanged(rows);
    };
  }

  onAddNewWarehouse(event) {
    const { rows } = this.state;
    event.preventDefault();
    rows.push({ id: undefined, category: undefined, chemical: undefined, value: undefined });
    this.onRowsChanged(rows);
  }

  onRemoveWarehouse(index) {
    return (event) => {
      const { rows } = this.state;
      event.preventDefault();
      rows.splice(index, 1);
      this.onRowsChanged(rows);
    };
  }

  renderRow(index) {
    const { items } = this.props;
    const row = this.state.rows[index];
    const item = find((i) => i.id == row.category, items) || { items: [] };
    const chemicals = item.items;
    return (
      <div className="section pure-g" key={index}>
        <div className="pure-u-1-3 pure-col">
          <select className="input" value={row.category} onChange={this.onValueChange(index, 'category')}>
            <option value="">Select one..</option>
            { mapWithIndex((item, index) => <option key={item.id} value={item.id}>{ item.name }</option>, items) }
          </select>
        </div>
        <div className="pure-u-1-3 pure-col">
          <select className="input" value={row.chemical} onChange={this.onValueChange(index, 'chemical')}>
            <option value="">Select one..</option>
            { mapWithIndex((item, index) => <option key={item.id} value={item.id}>{ item.name }</option>, chemicals) }
          </select>
        </div>
        <div className="pure-u-1-3 pure-col">
          <input type="text" className="input" defaultValue={row.value} onChange={this.onValueChange(index, 'value')} />
        </div>
        <div className="section-icon -right" onClick={this.onRemoveWarehouse(index)}>
          X
        </div>
      </div>
    );
  }

  render() {
    const { rows } = this.state;
    return (
      <div className="pure-g">
        <div className="section pure-u-1">
          { mapWithIndex((_, index) => this.renderRow(index), rows) }
        </div>
        <div className="section pure-u-1">
          <a href="#" onClick={this.onAddNewWarehouse}>+ add new chemical warehouse</a>
        </div>
      </div>
    );
  }
}

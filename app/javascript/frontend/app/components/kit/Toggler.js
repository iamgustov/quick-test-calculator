import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { map, addIndex } from 'ramda';

export default class Toggler extends Component {
  static propTypes = {
    items: PropTypes.array,
    onItemSelect: PropTypes.func,
  };

  static defaultProps = {
    onItemSelect: () => {},
  };

  constructor(props, context) {
    super(props, context);
    this.state = { selected: null };
    this.onItemSelect = this.onItemSelect.bind(this);
  }

  onItemSelect(item) {
    return () => {
      this.setState({ selected: item.id });
      this.props.onItemSelect(item);
    };
  }

  renderItem(item, index) {
    const { items } = this.props;
    const columnStyle = `pure-u-1-${items.length}`;
    const style = classNames('toggler-item', { '-no-left-border': index !== 0 }, { '-active': this.state.selected === item.id });
    return (
      <div className={columnStyle} key={index}>
        <div className={style} onClick={this.onItemSelect(item)}>{ item.title }</div>
      </div>
    );
  }

  render() {
    const items = addIndex(map)((item, index) => this.renderItem(item, index), this.props.items);
    return (
      <div className="toggler pure-g">
        { items }
      </div>
    );
  }
}
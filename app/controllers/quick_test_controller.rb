class QuickTestController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :prepare_tester, only: [:old_test]

  def index

  end

  def test
    @pool = create_pool(params)

    @mtest = @pool.mtests.create(
        pH: params[:ph], freeCl: params[:fcl],
        combinedCl: nil, totalAlkalinity: params[:ta],
        calciumHardness: nil, cyanuricAcid: nil,
    )

    @autoval = if @pool.isPool
                 view_context.pooldosagecalc(@pool.id)
               else
                 view_context.spadosagecalc(@pool.id)
               end

    render json: {
        dosage: @autoval,
        message: view_context.generatedosagetextforbeta(@autoval),
        data: {},
        params: params,
    }
  end

  def old_test
    dosage = @pool.isPool ? @tester.pool_dosage(@pool) : @tester.spa_dosage(@pool)
    render json: {
        data: dosage,
        message: @text_generator.dosage_text_for_beta(dosage[:values], @pool),
        xxx: Tester::FakeTextGenerator.new.generatedosagetextforbeta(dosage[:values], @pool)
    }
  end

  ########################### PRIVATE ###########################
  private

  def prepare_tester
    @tester = Tester::Dosage.new
    @text_generator = Tester::TextGenerator.new
    @pool = Calculator::Pool.new
    @pool.isPool = params[:pool] == 'pool'
    @pool.volume = (params[:volume] || 30_000).to_f
    @pool.waterChemistry = params[:chemical] || Calculator::Pool::WATER_CHEMISTRY::CHLORINE
    @pool.create_mtest(ph: params[:ph].to_f, fcl: params[:fcl].to_f, ta: params[:ta].to_f)
    (params[:warehouse] || []).each do |whouse|
      @pool.send "#{whouse[:chemical]}=", true
      @pool.send "#{whouse[:chemical]}Size=", whouse[:value]
    end
  end

  def create_pool(args)
    pool = Pool.new
    pool.isPool = args[:pool] == 'pool'
    pool.volume = (args[:volume] || 30_000).to_f
    pool.spabox = false
    pool.waterChemistry = args[:chemical] || Calculator::Pool::WATER_CHEMISTRY::CHLORINE
    pool.chemical_skin = args[:chemical_skin] || Calculator::Pool::CHEMICAL_SKIN::BASIC
    (args[:warehouse] || []).each do |whouse|
      pool.send "#{whouse[:chemical]}=", true
      pool.send "#{whouse[:chemical]}Size=", whouse[:value]
    end
    pool.save!
    pool
  end
end

require 'cmath'
module ApplicationHelper
  def formatphone(phone)
    @phone = phone
    if @phone.nil? == false && @phone != ""
      @phone = @phone.delete(' ')
      @phone = @phone.delete('-')
      @phone = @phone.delete('(')
      @phone = @phone.delete(')')
      @phone = @phone.delete('.')
      @phone = @phone.delete(' ')
      @phone = @phone.sub("tel+1:","")
      @phone = @phone.sub("+1:","")

      if @phone.mb_chars.length > 10
        @phone = @phone.split(//).last(10).join("").to_s
      end
      @phone = number_to_phone(@phone, country_code: 1, delimiter: "")
    end
    return @phone
  end

  def getonlynumbers(phone)
    @phone = phone
    if @phone.nil? == false && @phone != ""
      @phone = @phone.delete(' ')
      @phone = @phone.delete('-')
      @phone = @phone.delete('(')
      @phone = @phone.delete(')')
      @phone = @phone.delete('.')
      @phone = @phone.delete(' ')
      @phone = @phone.sub("tel+1:","")
      @phone = @phone.sub("+1:","")

      if @phone.mb_chars.length > 10
        @phone = @phone.split(//).last(10).join("").to_s
      end
      #@phone = number_to_phone(@phone, country_code: 1, delimiter: "")
    end
    return @phone
  end

  # TODO REMOVE
  def getnamebyphone(phone)
    return

    @phone = phone
    @phone = getonlynumbers(@phone)
    @customer = Customer.find_by_sql("select * from customers where replace(replace(replace(replace(replace(replace(replace(mPhone,' ',''),'-',''),'(',''),')',''),'.',''),' ',''),'+1','') = '"+@phone+"'")
    @name = ""
    if @customer.blank? == false
      @name = @customer[0]["firstName"]+" "+@customer[0]["lastName"]
    end

    return @name
  end

  # TODO REMOVE
  # Function used to insert an entry in the customerlogs table
  def createcustomerlog(customer_id,pool_id,action,status)
    return

    @clog = Customerlog.new
    @clog.customer_id = customer_id
    @clog.pool_id = pool_id
    @clog.action = action
    @clog.status = status
    if @clog.save
      return true
    else
      return false
    end
  end

  # TODO REMOVE
  # Function used to update an entry in the customerlogs table
  # toUpdate is a status whether to make it false or true
  def updatecustomerlog(pool,status,action,toUpdate)
    return

    @logs = Customerlog.where(customer_id: pool.customer_id, pool_id: pool.id, status: status, action: action).order("updated_at desc").limit(1).take
    if @logs.nil? == false
      @logs.status = toUpdate
      @logs.save
    end
  end

  # TODO REMOVE
  def isexcluded(type,value)
    return

    @status = false
    @exclarray = Text.find_by textit_id: value

    if @exclarray.blank? == false && @exclarray.marked == true
      @status = true
    end

    return @status

  end

  def gettablespoons(value)
    @temptbl = 0
    @temptbl = (((value.to_d) % 1 % 0.25)*16)
    @parts = @temptbl.to_s.split(".")
    @temptbl = @parts.count > 1 ? @parts[0].to_s : 0
    return @temptbl.to_i
  end

  def getquarters(value)
    @temptbl = 0
    @temptbl = ((value.to_d) % 1 / 0.25)
    @parts = @temptbl.to_s.split(".")
    @temptbl = @parts.count > 1 ? @parts[0].to_s : 0
    return @temptbl.to_i
  end

  def getfull(value)
    @temptbl = 0
    @temptbl = (value.to_d).div 1
    #@parts = @temptbl.to_s.split(".")
    #@temptbl = @parts.count > 1 ? @parts[0].to_s : 0
    return @temptbl.to_i
  end

  def getchemtextscoop(value,unit)
    #puts unit
    #check if full and quarter units are present
    if unit.split("~").count > 1
      @parts = unit.split("~")
      @unitf = @parts[0]+"(s)"
      @unitq = @parts[1]+"(s)"
    else
      @unitf = unit
      @unitq = unit
    end

    @text = ""
    @f = getfull(value)
    @q = getquarters(value)
    @t = gettablespoons(value)
    if @f.blank? == false && @f > 0
      @temp_f = getunits(@f.to_i,@unitf)
      @text = @f.to_s+" #{@temp_f}"
    end
    if @q.blank? == false && @q > 0
      if @text.blank?
        if  @q == 2
          @text += "a half cup"
        else
          @temp_unit_ = getunits(@q.to_i,@unitq)
          @text = @q.to_s+" quarter #{@temp_unit_}"
        end
      else
        if @t.blank? || @t <= 0
          if  @q == 2
            @temp_u = getunits(@f.to_i,@unitf)
            @text = @f.to_s+" #{@temp_u}"
            @text += " and a half cup"
          else
            @temp_q = getunits(@q.to_i,@unitq)
            @text += " and "+@q.to_s+" quarter #{@temp_q}"
          end
        else
          @temp_unitq = getunits(@q.to_i,@unitq)
          @text += ", "+@q.to_s+" quarter #{@temp_unitq}"
        end
      end
    end
    if @t.blank? == false && @t > 0
      if @text.blank?
        @temp_tablespoon = getunits(@t.to_i,"scoop(s)")
        @text = @t.to_s+" "+@temp_tablespoon
      else
        @tablespoon = getunits(@t.to_i,"scoop(s)")
        @text = @t.to_s+" "+@tablespoon
      end
    else
      @text = "0 scoop"
    end
    return @text
  end

  def getchemtextml(pool,value,unit)
    @text = ""
    if pool.isPool
      @f = getfull(value)
      @q = getquarters(value)
      @t = gettablespoons(value)
    else
      @t = gettablespoons(value)
    end

    if @t.blank? == false && @t > 0
      @tablespoon = getunits(@t.to_i,"mL")
      @text = @t.to_s+" "+@tablespoon
    else
      @text = " 0 mL"
    end
    return @text
  end

  def getchemtext(value,unit)
    #puts unit
    #check if full and quarter units are present
    if unit.split("~").count > 1
      @parts = unit.split("~")
      @unitf = @parts[0]+"(s)"
      @unitq = @parts[1]+"(s)"
    else
      @unitf = unit
      @unitq = unit
    end

    @text = ""
    @f = getfull(value)
    @q = getquarters(value)
    @t = gettablespoons(value)
    if @f.blank? == false && @f > 0
      @temp_f = getunits(@f.to_i,@unitf)
      @text = @f.to_s+" #{@temp_f}"
    end
    if @q.blank? == false && @q > 0
      if @text.blank?
        if  @q == 2
          @temp_unit_ = getunits(1,@unitq)
          @text += "a half "+@temp_unit
        else
          @temp_unit_ = getunits(@q.to_i,@unitq)
          @text = @q.to_s+" quarter #{@temp_unit_}"
        end
      else
        if @t.blank? || @t <= 0
          if  @q == 2
            @temp_u = getunits(@f.to_i,@unitf)
            @text = @f.to_s+" #{@temp_u}"
            @temp_unit = getunits(1,@unitq)
            @text += " and a half "+@temp_unit
          else
            @temp_q = getunits(@q.to_i,@unitq)
            @text += " and "+@q.to_s+" quarter #{@temp_q}"
          end
        else
          @temp_unitq = getunits(@q.to_i,@unitq)
          @text += ", "+@q.to_s+" quarter #{@temp_unitq}"
        end
      end
    end
    if @t.blank? == false && @t > 0
      if @text.blank?
        @temp_tablespoon = getunits(@t.to_i,"tablespoon(s)")
        @text = @t.to_s+" "+@temp_tablespoon
      else
        @tablespoon = getunits(@t.to_i,"tablespoon(s)")
        @text+= " and "+@t.to_s+" "+@tablespoon
      end
    end
    return @text
  end

  def generatedosagetext(dosage,pool)
    @units = "cup(s)"
    @nonSpaBoxUnit = "tablespoon(s)"
    @tablets = "Tablet(s)"
    @gallons = "Gallon(s)"
    @lbs = "lbs"
    if pool.isPool
      @units = "cup(s)"
      @nonSpaBoxUnit = "cup(s)"
    else
      @units = "packet(s)"
      @nonSpaBoxUnit = "cup(s)"
    end

    @skin = pool.chemical_skin

    @content = ""
    if @skin == "advanced"
      if dosage.trichlor.blank? == false && dosage.trichlor != "" && dosage.trichlor.to_d > 0
        if @content != ""
          @content += ", "
        end
        @tablets = getunits(dosage.trichlor,@tablets)
        @content += dosage.trichlor.to_s+' Trichlor 3\" tablets'
      end

      if dosage.phUp.blank? == false && dosage.phUp != "" && dosage.phUp.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.phUp,@nonSpaBoxUnit).to_s+" Soda Ash"
      end

      if dosage.phDown.blank? == false && dosage.phDown != "" && dosage.phDown.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.phDown,@nonSpaBoxUnit).to_s+" Dry acid"
      end

      if dosage.taUp.blank? == false && dosage.taUp != "" && dosage.taUp.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.taUp,@nonSpaBoxUnit).to_s+" Sodium Bicarbonate"
      end

      if dosage.liquidCl6.blank? == false && dosage.liquidCl6 != "" && dosage.liquidCl6.to_d > 0
        if @content != ""
          @content += ", "
        end
        @s6_liquid = " 6% Liquid Chlorine "
        if pool.option
          @s6_liquid = " Liquid Chlorine "
        end
        @content += getchemtext(dosage.liquidCl6,dosage.lch6unit+"~"+dosage.lch6qunit).to_s+@s6_liquid
      end

      if dosage.liquidCl8.blank? == false && dosage.liquidCl8 != "" && dosage.liquidCl8.to_d > 0
        if @content != ""
          @content += ", "
        end
        @s8_liquid = " 8% Liquid Chlorine "
        if pool.option
          @s8_liquid = " Liquid Chlorine "
        end
        @content += getchemtext(dosage.liquidCl8,dosage.lch8unit+"~"+dosage.lch8qunit).to_s+@s8_liquid
      end

      if dosage.liquidChlorine.blank? == false && dosage.liquidChlorine != "" && dosage.liquidChlorine.to_d > 0
        if @content != ""
          @content += ", "
        end
        @s12_liquid = " 12% Liquid Chlorine "
        if pool.option
          @s12_liquid = " Liquid Chlorine "
        end
        @content += getchemtext(dosage.liquidChlorine,dosage.lch12unit+"~"+dosage.lch12qunit).to_s+@s12_liquid
      end

      if dosage.dichlor.blank? == false && dosage.dichlor != "" && dosage.dichlor.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c56_dichlor = " 56% Dichlor "
        if pool.option
          @c56_dichlor = " Dichlor "
        end
        @content += getchemtext(dosage.dichlor,@nonSpaBoxUnit).to_s+@c56_dichlor
      end

      if dosage.dichlor62Size.blank? == false && dosage.dichlor62Size != "" && dosage.dichlor62Size.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c62_dichlor = " 62% Dichlor "
        if pool.option
          @c62_dichlor = " Dichlor "
        end
        @content += getchemtext(dosage.dichlor62Size,@nonSpaBoxUnit).to_s+@c62_dichlor
      end

      if dosage.calhypo53.blank? == false && dosage.calhypo53 != "" && dosage.calhypo53.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c53_hypo = " 53% Cal-hypo "
        if pool.option
          @c53_hypo = " Cal-hypo "
        end
        @content += getchemtext(dosage.calhypo53,@lbs).to_s+@c53_hypo
      end

      if dosage.calhypo65.blank? == false && dosage.calhypo65 != "" && dosage.calhypo65.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c65_hypo = " 65% Cal-hypo "
        if pool.option
          @c65_hypo = " Cal-hypo "
        end
        @content += getchemtext(dosage.calhypo65,@lbs).to_s+@c65_hypo
      end

      if dosage.calhypo73.blank? == false && dosage.calhypo73 != "" && dosage.calhypo73.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c73_hypo = " 73% Cal-hypo "
        if pool.option
          @c73_hypo = " Cal-hypo "
        end
        @content += getchemtext(dosage.calhypo73,@lbs).to_s+@c73_hypo
      end

      if dosage.phUpBorax.blank? == false && dosage.phUpBorax != "" && dosage.phUpBorax.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.phUpBorax,@nonSpaBoxUnit).to_s+" borax"
      end

      if dosage.muriatic15.blank? == false && dosage.muriatic15 != "" && dosage.muriatic15.to_d > 0
        if @content != ""
          @content += ", "
        end
        @m73_acid = " 15.7% Muriatic Acid "
        if pool.option
          @m73_acid = " Muriatic Acid "
        end
        @content += getchemtext(dosage.muriatic15,dosage.macid15unit+"~"+dosage.macid15qunit).to_s+@m73_acid
      end

      if dosage.muriatic31.blank? == false && dosage.muriatic31 != "" && dosage.muriatic31.to_d > 0
        if @content != ""
          @content += ", "
        end
        @m31_acid = " 31% Muriatic Acid "
        if pool.option
          @m31_acid = " Muriatic Acid "
        end
        @content += getchemtext(dosage.muriatic31,dosage.macid31unit+"~"+dosage.macid31qunit).to_s+@m31_acid
      end

      if dosage.trichlor2Size.blank? == false && dosage.trichlor2Size != "" && dosage.trichlor2Size.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += dosage.trichlor2Size.to_s+' Trichlor 2\" tablets'
      end

      if dosage.bromine1Size.blank? == false && dosage.bromine1Size != "" && dosage.bromine1Size.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += dosage.bromine1Size.to_s+' Bromine 1\" tablets'
      end

      if dosage.lithiumhypoSize.blank? == false && dosage.lithiumhypoSize != "" && dosage.lithiumhypoSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.lithiumhypoSize,@nonSpaBoxUnit).to_s+" Lithium Hypo"
      end

      if dosage.calciumSize.blank? == false && dosage.calciumSize != "" && dosage.calciumSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.calciumSize,@nonSpaBoxUnit).to_s+" Calcium chloride"
      end

      if dosage.cyanuricsolidSize.blank? == false && dosage.cyanuricsolidSize != "" && dosage.cyanuricsolidSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.cyanuricsolidSize,@nonSpaBoxUnit).to_s+" Cyanuric Acid Solid"
      end

      if dosage.cyanuricliquidSize.blank? == false && dosage.cyanuricliquidSize != "" && dosage.cyanuricliquidSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.cyanuricliquidSize,@nonSpaBoxUnit).to_s+" Cyanuric Acid Liquid"
      end

      if dosage.esanitizer.blank? == false && dosage.esanitizer != "" && dosage.esanitizer.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.esanitizer.to_i,"packet(s)")
        @content += dosage.esanitizer.to_s+" "+@packet+" E"
      end

      if dosage.ddryacid.blank? == false && dosage.ddryacid != "" && dosage.ddryacid.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.ddryacid.to_i,"packet(s)")
        @content += dosage.ddryacid.to_s+" "+@packet+" D"
      end

      if dosage.bbakingsoda.blank? == false && dosage.bbakingsoda != "" && dosage.bbakingsoda.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.bbakingsoda.to_i,"packet(s)")
        @content += dosage.bbakingsoda.to_s+" "+@packet+" A"
      end

      if dosage.bsodaash.blank? == false && dosage.bsodaash != "" && dosage.bsodaash.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.bsodaash.to_i,"packet(s)")
        @content += dosage.bsodaash.to_s+" "+@packet+" B"
      end
    elsif @skin == "basic"
      if dosage.trichlor.blank? == false && dosage.trichlor != "" && dosage.trichlor.to_d > 0
        if @content != ""
          @content += ", "
        end
        @tablets = getunits(dosage.trichlor,@tablets)
        @content += dosage.trichlor.to_s+' '+getchemicaltype(pool,"Trichlor 3")+' tablets'
      end

      if dosage.phUp.blank? == false && dosage.phUp != "" && dosage.phUp.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.phUp,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Soda Ash")
      end

      if dosage.phDown.blank? == false && dosage.phDown != "" && dosage.phDown.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.phDown,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Dry Acid")
      end

      if dosage.taUp.blank? == false && dosage.taUp != "" && dosage.taUp.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.taUp,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Sodium Bicarbonate")
      end

      if dosage.liquidCl6.blank? == false && dosage.liquidCl6 != "" && dosage.liquidCl6.to_d > 0
        if @content != ""
          @content += ", "
        end
        @s6_liquid = "6% Liquid Chlorine"
        if pool.option
          @s6_liquid = "Liquid Chlorine"
        end
        @content += getchemtext(dosage.liquidCl6,dosage.lch6unit+"~"+dosage.lch6qunit).to_s+" "+getchemicaltype(pool,@s6_liquid)
      end

      if dosage.liquidCl8.blank? == false && dosage.liquidCl8 != "" && dosage.liquidCl8.to_d > 0
        if @content != ""
          @content += ", "
        end
        @s8_liquid = "8% Liquid Chlorine"
        if pool.option
          @s8_liquid = "Liquid Chlorine"
        end
        @content += getchemtext(dosage.liquidCl8,dosage.lch8unit+"~"+dosage.lch8qunit).to_s+" "+getchemicaltype(pool,@s8_liquid)
      end

      if dosage.liquidChlorine.blank? == false && dosage.liquidChlorine != "" && dosage.liquidChlorine.to_d > 0
        if @content != ""
          @content += ", "
        end
        @s12_liquid = "12% Liquid Chlorine"
        if pool.option
          @s12_liquid = "Liquid Chlorine"
        end
        @content += getchemtext(dosage.liquidChlorine,dosage.lch12unit+"~"+dosage.lch12qunit).to_s+" "+getchemicaltype(pool,@s12_liquid)
      end

      if dosage.dichlor.blank? == false && dosage.dichlor != "" && dosage.dichlor.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c56_dichlor = "56% Dichlor"
        if pool.option
          @c56_dichlor = "Dichlor"
        end
        @content += getchemtext(dosage.dichlor,@units).to_s+" "+getchemicaltype(pool,@c56_dichlor)
      end

      if dosage.dichlor62Size.blank? == false && dosage.dichlor62Size != "" && dosage.dichlor62Size.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c62_dichlor = "62% Dichlor"
        if pool.option
          @c62_dichlor = "Dichlor"
        end
        @content += getchemtext(dosage.dichlor62Size,@units).to_s+" "+getchemicaltype(pool,@c62_dichlor)
      end

      if dosage.calhypo53.blank? == false && dosage.calhypo53 != "" && dosage.calhypo53.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c53_hypo = "53% Cal-hypo"
        if pool.option
          @c53_hypo = "Cal-hypo"
        end
        @content += getchemtext(dosage.calhypo53,@lbs).to_s+" "+getchemicaltype(pool,@c53_hypo)
      end

      if dosage.calhypo65.blank? == false && dosage.calhypo65 != "" && dosage.calhypo65.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c65_hypo = "65% Cal-hypo"
        if pool.option
          @c65_hypo = "Cal-hypo"
        end
        @content += getchemtext(dosage.calhypo65,@lbs).to_s+" "+getchemicaltype(pool,@c65_hypo)
      end

      if dosage.calhypo73.blank? == false && dosage.calhypo73 != "" && dosage.calhypo73.to_d > 0
        if @content != ""
          @content += ", "
        end
        @c73_hypo = "73% Cal-hypo"
        if pool.option
          @c73_hypo = "Cal-hypo"
        end
        @content += getchemtext(dosage.calhypo73,@lbs).to_s+" "+getchemicaltype(pool,@c73_hypo)
      end

      if dosage.phUpBorax.blank? == false && dosage.phUpBorax != "" && dosage.phUpBorax.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.phUpBorax,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Borax")
      end

      if dosage.muriatic15.blank? == false && dosage.muriatic15 != "" && dosage.muriatic15.to_d > 0
        if @content != ""
          @content += ", "
        end
        @m73_acid = "15.7% Muriatic Acid"
        if pool.option
          @m73_acid = "Muriatic Acid"
        end
        @content += getchemtext(dosage.muriatic15,dosage.macid15unit+"~"+dosage.macid15qunit).to_s+" "+getchemicaltype(pool,@m73_acid)
      end

      if dosage.muriatic31.blank? == false && dosage.muriatic31 != "" && dosage.muriatic31.to_d > 0
        if @content != ""
          @content += ", "
        end
        @m31_acid = "31% Muriatic Acid"
        if pool.option
          @m31_acid = "Muriatic Acid"
        end
        @content += getchemtext(dosage.muriatic31,dosage.macid31unit+"~"+dosage.macid31qunit).to_s+" "+getchemicaltype(pool,@m31_acid)
      end

      if dosage.trichlor2Size.blank? == false && dosage.trichlor2Size != "" && dosage.trichlor2Size.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += dosage.trichlor2Size.to_s+" "+getchemicaltype(pool,'Trichlor')
      end

      if dosage.bromine1Size.blank? == false && dosage.bromine1Size != "" && dosage.bromine1Size.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += dosage.bromine1Size.to_s+" "+getchemicaltype(pool,'Bromine')
      end

      if dosage.lithiumhypoSize.blank? == false && dosage.lithiumhypoSize != "" && dosage.lithiumhypoSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.lithiumhypoSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Lithium Hypo")
      end

      if dosage.calciumSize.blank? == false && dosage.calciumSize != "" && dosage.calciumSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.calciumSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Calcium chloride")
      end

      if dosage.cyanuricsolidSize.blank? == false && dosage.cyanuricsolidSize != "" && dosage.cyanuricsolidSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.cyanuricsolidSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Cyanuric Acid Solid")
      end

      if dosage.cyanuricliquidSize.blank? == false && dosage.cyanuricliquidSize != "" && dosage.cyanuricliquidSize.to_d > 0
        if @content != ""
          @content += ", "
        end
        @content += getchemtext(dosage.cyanuricliquidSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Cyanuric Acid Liquid")
      end

      if dosage.esanitizer.blank? == false && dosage.esanitizer != "" && dosage.esanitizer.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.esanitizer.to_i,"packet(s)")
        @content += dosage.esanitizer.to_s+" "+@packet+" "+getchemicaltype(pool,"E")
      end

      if dosage.ddryacid.blank? == false && dosage.ddryacid != "" && dosage.ddryacid.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.ddryacid.to_i,"packet(s)")
        @content += dosage.ddryacid.to_s+" "+@packet+" "+getchemicaltype(pool,"D")
      end

      if dosage.bbakingsoda.blank? == false && dosage.bbakingsoda != "" && dosage.bbakingsoda.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.bbakingsoda.to_i,"packet(s)")
        @content += dosage.bbakingsoda.to_s+" "+@packet+" "+getchemicaltype(pool,"A")
      end

      if dosage.bsodaash.blank? == false && dosage.bsodaash != "" && dosage.bsodaash.to_d > 0
        if @content != ""
          @content += ", "
        end
        @packet = getunits(dosage.bsodaash.to_i,"packet(s)")
        @content += dosage.bsodaash.to_s+" "+@packet+" "+getchemicaltype(pool,"B")
      end
    end
    puts "Dosage %%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    puts @content
    puts "Dosage %%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    return @content
  end

  def getunits(value,unit)
    @temp_unit = ""
    if unit != "mL"
      unit = (unit.downcase).gsub(/\s+/, "")
    end
    if unit == "cup(s)" || unit == "cup" ||  unit == "cups"  ||  unit == "cups(s)"
      if value.to_d > 1
        @temp_unit = "cups"
      else
        @temp_unit = "cup"
      end
    elsif unit == "tablet(s)" || unit == "tablet" || unit == "tablets"
      if value.to_d > 1
        @temp_unit = "Tablets"
      else
        @temp_unit = "Tablet"
      end
    elsif unit  == "gallon(s)" || unit == "gallon" || unit == "gallons"
      if value.to_d > 1
        @temp_unit = "Gallons"
      else
        @temp_unit = "Gallon"
      end
    elsif unit == "packet(s)" || unit == "packet" || unit == "packets"
      if value.to_d > 1
        @temp_unit = "packets"
      else
        @temp_unit = "packet"
      end
    elsif unit == "tablespoon(s)" || unit == "tablespoon" || unit == "tablespoons"
      if value.to_d > 1
        @temp_unit = "tablespoons"
      else
        @temp_unit = "tablespoon"
      end
    elsif unit == "lbs"
      if value.to_d > 1
        @temp_unit = "lbs"
      else
        @temp_unit = "lb"
      end
    else
      if value.to_d > 1
        @temp_unit = unit.gsub("(","").gsub(")","")
      else
        @temp_unit = unit.gsub("(s)","")
      end
    end

    if @current_user_role.blank? == false && @current_user_role == "Lora" && (@temp_unit == "tablespoons" || @temp_unit == "tablespoon")
      @temp_unit = "tsp"
    end

    return @temp_unit
  end

  def iscontactblocked(phonenum)
    @phone = getonlynumbers(phonenum )
    @phone = "tel:+1"+@phone
    @query   = { "urns" => @phone}
    @status = nil
    @messages = HTTParty.get("https://api.textit.in/api/v2/contacts.json",:query=>@query, :headers => { "Authorization" => "Token 4d0246d5a2b11d95c4a84af83855e1f66ff0b5d5"},:verify => false)
    @messages["results"].each do |m|
      @status = m
    end
    return @status
  end

  #Added by Gowtham on 26 Jan 2016 to calculate dosage details for pool
  def pooldosagecalc(p_id)
    puts "==================================Pool Dosage====================================="
    @values = {}
    @pool = Pool.find(p_id)
    # @customer = Customer.find(@pool.customer_id)
    # @role = User.find_by_email(@customer.email)
    # @features = Capability.where(role_id: @role.role_id).to_a
    @role_name = 'ANY' # getuserrole(@customer)
    @auto_pres = true
    # @features.each do |i|
    #   @feature_name = Feature.where(id:i.feature_id).limit(1).pluck(:feature_name).to_a
    #   @feature_name = @feature_name[0]
    #   if @feature_name == "AUTO_PRESCRIPTION"
    #     @auto_pres = true
    #   end
    # end
    if @auto_pres
      if @pool.isPool
        #Inputs
        water_type="pool"

        #Get pool volume
        if @pool.volume.blank? == false
          @pool_vol_gal= @pool.volume #Gallons of pool water
        else
          @pool_vol_gal= 0
        end

        @orp = 0
        @ph = 0
        @ta_i_val = ""

        @sanitizer = @pool.waterChemistry

        #Fetch manual readings
        @mreading = @pool.mtests.last

        @reading_type = ""
        if @mreading.blank? == false
          @reading_type = "manual"
          if @mreading.pH.blank? == false
            @ph = @mreading.pH
          end
          if @mreading.freeCl.blank? == false
            @free_cl = @mreading.freeCl.to_d
          end
          if @mreading.totalAlkalinity.blank? == false
            @ta_i_val = @mreading.totalAlkalinity.to_d
          end
          @daysdiff = ((Time.now - @mreading.created_at)/1.day).round
        end
        if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
          @reading_type = "device"
          unless @pool.epools.blank?
            @pool.epools.each do |epool|
              @orp = epool.oOrp
              unless epool.points.last.blank?  || epool.nil?
                @ph = ph(epool)
                @orp = orp(epool)
                @daysdiff = ((Time.now - epool.points.last.created_at)/1.day).round
                #Rails.logger.info("DAYSDIFF : "+@daysdiff.to_s)
              end # if points check
            end # looping of devices
          end # if pool has device check
        end

        if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
          Rails.logger.info("Returning as readings are greater than 3 days")
          return ""
        end

        #decide range of the system and what to be prescribed
        @range = "notgood"
        @phrange = "notgood"
        @chlrange = "notgood"
        @tarange = ""


        if @role_name == "Lora"
          #ph decider
          if @ph.to_d >= 7.2 && @ph.to_d <= 7.8
            @phrange = "good"
            Rails.logger.info("PH RANGE GOOD")
          end
          #chlorine decider
          if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 600 && @orp.to_d <= 900))
            @chlrange = "good"
            Rails.logger.info("CH RANGE GOOD")
          end
          #ta decider
          if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
            @tarange = "good"
            Rails.logger.info("TA RANGE GOOD")
          elsif @ta_i_val.blank? == false
            @tarange = "notgood"
            Rails.logger.info("TA RANGE NOT GOOD")
          end
        else
          #ph decider
          if @ph.to_d >= 7.4 && @ph.to_d <= 7.6
            @phrange = "good"
            Rails.logger.info("PH RANGE GOOD")
          end
          #chlorine decider
          if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 650 && @orp.to_d <= 900))
            @chlrange = "good"
            Rails.logger.info("CH RANGE GOOD")
          end
          #ta decider
          if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
            @tarange = "good"
            Rails.logger.info("TA RANGE GOOD")
          elsif @ta_i_val.blank? == false
            @tarange = "notgood"
            Rails.logger.info("TA RANGE NOT GOOD")
          end
        end

        #range decider ends

        if @phrange == "good" && @chlrange == "good" && (@tarange == "good" || @tarange == "")
          deletealldosages(@pool)
          Rails.logger.info("Returning as system is in good range")
          return ""
        end

        @can_populate = false

        case @reading_type
          when "manual"
            Rails.logger.info("Manual Reading")
            #if @sanitizer != "Chlorine"
            Rails.logger.info("NOT CHLORINE")
            @FC_i = @free_cl
            @can_populate = true
          #end
          when "device"
            Rails.logger.info("Device Reading")
            #if @sanitizer != "Chlorine"
            Rails.logger.info("NOT CHLORINE")
            @FC_i = (40000-CMath.sqrt(6840000000-9600000*(@orp.to_i)))/5000.0
            @FC_i = @FC_i.real
            if @FC_i < 0
              @FC_i = 0
            end
            @can_populate = true
          #end
        end
        #puts @ph.to_s
        #puts @FC_i.to_s
        #if @ph.to_d <=0 && @FC_i.to_d <= 0
        if @can_populate == false
          #puts @ph.to_s
          #puts @FC_i.to_s
          Rails.logger.info("Returning as readings are not valid")
          return ""
        end

        @pH_i= @ph #Initial pH from 0 to 14

        @acid_type= 0 #Acid type
        if @pool.dryacid.blank? == false && @pool.dryacid && @pool.dryacidSize.blank? == false && @pool.dryacidSize.to_d > 0
          @acid_type = 1
        elsif @pool.muriatic31.blank? == false && @pool.muriatic31 && @pool.muriatic31Size.blank? == false && @pool.muriatic31Size.to_d > 0
          @acid_type = 2
        end

        #check if no chemical is available and use last used acid
        if @acid_type == 0
          Rails.logger.info("Acid name empty.. Going to use last used Acid")
          @lastused = getlastusedacid(@pool)
          @acid_type = @lastused
          Rails.logger.info("Acid name empty.. Going to use last used Acid : "+@acid_type.to_s)
        end

        @TA_i= @ta_i_val.to_d #input('What is the Total Alkalinity (ppm)?: '); #Initial Total Alkalinity (ppm)
        @CYA_i=@cya_i_val#input('What is the Cyanuric Acid level (ppm)?: '); #Initial Cyanuric acid level (ppm)
        if @pool.waterChemistry == "Salt Water"
          @sanitizer = 'Chlorine'
          @SWG = 1 #salt water chlorinator
          #puts "SWG = 1"
        else
          @SWG = 2 #salt water chlorinator
          #puts "SWG = 1"
        end

        if @pool.ozonator
          @ozone = 1 #ozonator
        else
          @ozone = 2 #ozonator
        end

        #Maximum recommended chemical amounts to be added per 10,000 gallons of pool water at any one time:
        #Sodium bicarbonate: 10 lbs.
        #Sodium carbonate (soda ash): 4 lbs.
        #Muriatic acid: 16 fl. oz.
        #Sodium bisulfate: 20 oz.

        # Alkalinity Adjustment
        @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
        @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
        @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
        @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
        @Calc_CPO_NaHCO3=0

        if @SWG==1
          @TA_tar=(@base_maxTA+@base_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
          if @TA_i<=@base_minTA
            @delta_TA=@TA_tar-@TA_i
            @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA
          else
            @delta_TA = 0
            #return @values
          end
        elsif @SWG==2
          @TA_tar=(@acid_maxTA+@acid_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
          if @TA_i<=@acid_minTA
            @delta_TA=@TA_tar-@TA_i
            @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
          elsif @TA_i>=@acid_maxTA+20
            @delta_TA = 0
            puts 'Watch TA, should lower over time using tablets.'
            #return @values
          else
            @delta_TA = 0
            #return @values
          end
        else
          @delta_TA=@TA_tar-@TA_i
        end

        #pH Adjustment
        @Calc_CPO_Na2CO3=0
        @Calc_CPO_NaHSO4=0
        @Calc_CPO_HCl=0
        @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
        @max_NaHSO4=20/16 #Maximum recommended sodium bisulfate single addition per 10,000 gal
        @max_HCl=16/128 #Maximum recommended muriatic acid single addition per 10,000 gal

        if @pH_i<7.0
          @pH_i=7.0
          #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n')
        elsif @pH_i>8.0
          @pH_i=8.0
          #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n')
        end

        #Base
        if (@pH_i<7.4 && @pH_i>=7.2)#7.2<=@pH_i<7.4
          @CPO_Na2CO3=0.375
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)
        elsif (@pH_i<7.2 && @pH_i>=7.0)#7.0<=@pH_i<7.2
          @CPO_Na2CO3=0.5
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)

          #Acid
        elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==1)#7.6<=@pH_i<=7.8 and sodium bisulfite
          @CPO_NaHSO4=0.94
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)
        elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==1)#7.8<@pH_i<=8.0 and sodium bisulfite
          @CPO_NaHSO4=1.25
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)#7.6<=@pH_i<=7.8 and muriatic acid
        elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==2)
          @CPO_HCl=0.094
          @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
        elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==2)#7.8<@pH_i<=8.0 and muriatic acid
          @CPO_HCl=0.125
          @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
        end

        #Adjusting TA adjustment from pH adjustment
        if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
          @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
          @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@pool_vol_gal/10000.0)) #TA increase from pH increase
          #Rails.logger.info("LOGS TRUE PRESET"+@delta_TA.to_s)
          #Rails.logger.info("LOGS TRUE PRESET"+@TAppm_Na2CO3.to_s)
          @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
        else
          @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
        end

        #Sanitizer

        #check for chlorine range before prescribing sanitizer
        if @chlrange == "notgood"
          if @sanitizer == 'Chlorine' #Check for chlorine
            #puts "Primary Sanitizer = 1"
            @minFC=3 # Minimum ideal FC ppm
            @maxFC=4#Maximum ideal FC ppm
            @FC_tar=(@maxFC+@minFC)/2.0#Target FC

            if @FC_i.blank? == false
              @delta_FC=@FC_tar-@FC_i
            else
              @delta_FC=0
            end
            #puts "DELTA = "+@delta_FC.to_s
            if @delta_FC>0 #Check if sanitizer above target
              #puts "INSIDE DELTA CHECK"
              @divideunit = 1
              if @pool.bleach6 && @pool.bleach6Size.blank? == false && @pool.bleach6Size.to_d > 0
                @Cl_type= 5
              elsif @pool.bleach12 && @pool.bleach12Size.blank? == false && @pool.bleach12Size.to_d > 0
                @Cl_type= 4
              elsif (@pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0) || (@pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0) || (@pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0)
                @Cl_type= 0
              elsif @pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d > 0
                @Cl_type= 1
                @divideunit = 0.5375
              elsif @pool.dichlor62 && @pool.dichlor62Size.blank? == false && @pool.dichlor62Size.to_d > 0
                @Cl_type= 2
                @divideunit = 0.5375
              elsif @pool.trichlor3 && @pool.trichlor3Size.blank? == false && @pool.trichlor3Size.to_d > 0
                @Cl_type= 3
              else
                @Cl_type = 0
              end
              #puts "CHEM TYPE = "+@Cl_type.to_s
              #mess @Cl_type= input ('\nSelect Chlorine type by number: \n1:Dichlor 56# ACC\n2:Dichlor 62# ACC\n3:Trichlor\n4:12# Liquid Chlorine\n5:6# Liquid Chlorine (Household Bleach)\nSelection: ');
              if @Cl_type==1
                @Cl_name='Dichlor 56#'
                @Cl_nametemp=('dichlor_56')
                @CPO_Cl=0.15 #Dichlor 56# ACC CPO factor
              elsif @Cl_type==2
                @Cl_name='Dichlor 62#'
                @Cl_nametemp=('dichlor_62')
                @CPO_Cl=0.131 #Dichlor 62# ACC CPO factor
              elsif @Cl_type==3
                @Cl_name='3" Trichlor Tablet(s)'
                @Cl_nametemp=('trichlor')
                @CPO_Cl=0.09375 #Trichlor CPO factor
              elsif @Cl_type==4
                @Cl_name='12# Liquid Chlorine'
                @Cl_nametemp=('lch12')
                @CPO_Cl=0.0836 #12# Liquid Chlorine
              elsif @Cl_type==5
                @Cl_name='6# Liquid Chlorine (Household Bleach)'
                @Cl_nametemp=('lch6')
                @CPO_Cl=0.1672 #Approximation 6# Liquid Chlorine
              elsif @Cl_type==0
                @Cl_name='Calcium Hypochlorite'
                if @pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0
                  @Cl_nametemp=('chyp53')
                elsif @pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0
                  @Cl_nametemp=('chyp65')
                elsif @pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0
                  @Cl_nametemp=('chyp73')
                end
                @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
              else
                @CPO_Cl=0
              end

              @Calc_CPO_Cl=@CPO_Cl*(@pool_vol_gal/10000.0)*@delta_FC

              if @SWG==2 || @SWG==1 #No chlorine generator => manual chlorine
                #check if no chemical is available and use last used sanitizer
                if @Cl_nametemp.blank?
                  Rails.logger.info("Chemical name empty.. Going to use last used sanitizer")
                  @lastused = getlastusedchlorine(@pool)
                  @Cl_type = @lastused
                  Rails.logger.info("Last used sanitizer : "+@lastused.to_s)
                  if @lastused==1
                    @Cl_name='Dichlor 56#'
                    @Cl_nametemp=('dichlor_56')
                    @CPO_Cl=0.15 #Dichlor 56# ACC CPO factor
                    @divideunit = 0.5375
                  elsif @lastused==2
                    @Cl_name='Dichlor 62#'
                    @Cl_nametemp=('dichlor_62')
                    @CPO_Cl=0.131 #Dichlor 62# ACC CPO factor
                    @divideunit = 0.5375
                  elsif @lastused==3
                    @Cl_name='3" Trichlor Tablet(s)'
                    @Cl_nametemp=('trichlor')
                    @CPO_Cl=0.09375 #Trichlor CPO factor
                  elsif @lastused==4
                    @Cl_name='12# Liquid Chlorine'
                    @Cl_nametemp=('lch12')
                    @CPO_Cl=0.0836 #12# Liquid Chlorine
                  elsif @lastused==5
                    @Cl_name='6# Liquid Chlorine (Household Bleach)'
                    @Cl_nametemp=('lch6')
                    @CPO_Cl=0.1672 #Approximation 6# Liquid Chlorine
                  elsif @lastused==0
                    @Cl_name='Calcium Hypochlorite'
                    if @pool.calhypo53
                      @Cl_nametemp=('chyp53')
                    elsif @pool.calhypo65
                      @Cl_nametemp=('chyp65')
                    elsif @pool.calhypo73
                      @Cl_nametemp=('chyp73')
                    end
                    @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                  else
                    @CPO_Cl=0
                  end
                  Rails.logger.info("Chemical name empty.. Going to use last used sanitizer : "+@Cl_name)
                end

                #puts "Chem Name = "+@Cl_name
                #puts "CPO_Cl = "+@CPO_Cl.to_s

                if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                  #puts "@Cl_type<=5 && @Cl_type>=4"
                  if @Calc_CPO_Cl>=0.25 #Check for gallons
                    #puts "@Calc_CPO_Cl>=0.25"
                    @wholegal_Cl=(@Calc_CPO_Cl).floor  #Number of whole gallons of chlorine
                    @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                    if @quartergal_Cl==4
                      @wholegal_Cl=1
                      @quartergal_Cl=@quartergal_Cl-4
                    end
                    #mess fprintf('\nAdd #g gallon(s) and #g quarter gallon(s) of #s to the #s evenly.\n\n', @wholegal_Cl, @quartergal_Cl, @Cl_name, water_type)
                    #puts @wholegal_Cl.to_s
                    #puts @quartergal_Cl.to_s
                    @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                    #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625"
                    @wholecup_Cl=((@Calc_CPO_Cl)*16).floor #Number of whole cups of chlorine
                    @quartercup_Cl=(((@Calc_CPO_Cl)*16-@wholecup_Cl)/0.25).round #Number of quarter cups of chlorine
                    #mess fprintf('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s evenly.\n\n', @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                    #puts @wholecup_Cl.to_s
                    #puts @quartercup_Cl.to_s
                    @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                  else
                    #puts "@Calc_CPO_Cl<0.0625"
                    @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                    #mess fprintf('\nAdd #g tablespoon(s) of #s to the #s evenly.\n\n', @wholetbs_Cl, @Cl_name, water_type)
                    #puts @wholetbs_Cl.to_s
                    @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                  end
                elsif @Calc_CPO_Cl>=0.25 && @Cl_type<3 #Check for less than 1 pound granular
                  #puts "@Calc_CPO_Cl>=0.25 && @Cl_type<3"
                  @wholecup_Cl=(@Calc_CPO_Cl/@divideunit).floor #Assumes 1 cup = 0.5375 lb for dichlor only
                  @quartercup_Cl=(((@Calc_CPO_Cl/@divideunit)-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                  #mess fprintf ('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s by spreading granules evenly.\n\n' , @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                  #puts @wholecup_Cl.to_s
                  #puts @quartercup_Cl.to_s
                  @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3 #Check for greater than zero
                  #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3"
                  @tbs_Cl=((@Calc_CPO_Cl/@divideunit)*16).round #Convert to tbs
                  #mess fprintf ('\nAdd #g tablespoon(s) of #s to the #s by spreading granules evenly.\n\n' , @tbs_Cl, @Cl_name, water_type)
                  #puts @tbs_Cl.to_s
                  @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                elsif @Cl_type==3
                  #puts "@Cl_type==3"
                  @wgt_trichlortab= 8 #Assuming trichlor 3" tablet is 8 oz
                  @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert lbs to ounces
                  @wholetab_Cl=(@ozCalc_CPO_Cl/@wgt_trichlortab).ceil
                  #mess fprintf('Add #g #s to the floater(s) in the #s incrementally if necessary.\n\n', @wholetab_Cl, @Cl_name, water_type)
                  #puts @wholetab_Cl.to_s
                  @values.store(@Cl_nametemp, @wholetab_Cl.to_s+";tbs")
                end
                #elsif @SWG==1 #Salt water chlorine generator and low chlorine
                #puts "@SWG==1"
                #mess fprintf ('Chlorine is low.\nSalt water chlorinator needs to be checked. Supplemental chlorine can be used.\n\n')
              end
            elsif @delta_FC<=0 #Check if chlorine is at or above target
              #mess fprintf ('\nChlorine is within range.\n\n')
            end
          end
        end
        #chlorine range check ends

        if (@daysdiff.blank? == false && @daysdiff > 3) || (@TA_i.blank? ==false && @TA_i.to_d <=0)
          @adj_Calc_CPO_NaHCO3=0
        end

        #check for ta range before prescribing ta
        if @tarange == "notgood"
          #puts @adj_Calc_CPO_NaHCO3
          #Adjusted TA Dose Report
          if (@adj_Calc_CPO_NaHCO3 >= 0.25 && @adj_Calc_CPO_NaHCO3<=(@pool_vol_gal/10000.0)*@max_NaHCO3) #Check for pounds and maximum per 10,000 gallons
            @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
            @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjustment.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
            @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
          elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<=0.25 && @adj_Calc_CPO_NaHCO3<(@pool_vol_gal/10000.0)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
            @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
            @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
            #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjusments.\n\n', @tbs_NaHCO3, @TA_tar)
            @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
          else
            if @adj_Calc_CPO_NaHCO3>=0.25
              @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
              @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
              @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
            elsif (@adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
              @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
              @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @tbs_NaHCO3, @TA_tar)
              @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
            end
          end
        end
        #ta range check ends

        #check for ph range before prescribing ph
        if @phrange == "notgood"
          #pH Dose Report
          if ( @Calc_CPO_Na2CO3 > (@max_Na2CO3*@pool_vol_gal/10000.0))
            @Calc_CPO_Na2CO3 = @max_Na2CO3*@pool_vol_gal/10000.0
          end

          @cups_Na2CO3 = @Calc_CPO_Na2CO3/0.574
          puts "VL : "+@cups_Na2CO3.to_s
          @wholecup_Na2CO3 = 0

          if (@cups_Na2CO3 >= 0.25) #Check for pounds and maximum per 10,000 gallons
            if (@cups_Na2CO3 >= 0.8)
              @wholecup_Na2CO3=@cups_Na2CO3.round #Assumes 1 cup = 0.574 lb
            end
            if (@wholecup_Na2CO3 < @cups_Na2CO3)
              @quartercup_Na2CO3=((@cups_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
            end
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium carbonate to increase pH \n', @wholecup_Na2CO3, @quartercup_Na2CO3)
            @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
          else #less than 1/4 cup
            @Calc_CPO_Na2CO3 = @cups_Na2CO3 * 16 #Convert cups to tbs
            @tbs_Na2CO3 = (@Calc_CPO_Na2CO3).round
            #mess fprintf('Add #g tablespoon(s) of Sodium carbonate to increase pH \n', @tbs_Na2CO3)
            #if @tbs_Na2CO3 > 0
            @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
            #end
          end

          if @acid_type==1
            if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
              @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #Assumes 1 cup = 1 lb
              @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
              @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
            elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000.0) #Check for ounces and max dose per 10,000 gallons
              @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
              @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite to lower pH \n', @tbs_NaHSO4)
              @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
            else #greater than maximum dose per 10,000 gallons
              if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
                @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #assumes 1 cup = 1 lb
                @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
                #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
                @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
              elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
                @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
                @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
                #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @tbs_NaHSO4)
                @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
              end
            end
          elsif @acid_type==2
            if (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
              @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
              @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
              @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
              #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
            elsif (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
              @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
              @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
              @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
              #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid INCREMENTALLY in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
            elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for cups and maximum dose per 10,000 gallons
              @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
              @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
              #mess fprintf('Spread #g cup(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
            elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0)
              @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
              @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
              #mess fprintf('Spread #g cup(s) INCREMENTALLY of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
            end
          end
        end
        #ph range check ends


        puts @values
        Rails.logger.info("DOSAGE AUTO : "+@values.inspect)
        puts "==================================Pool Dosage ends================================="
        return @values
      end
    else
      # AUTO_PRESCRIPTION is not enabled
      return @values
    end
  end

  def spadosagecalc(p_id)
    puts "==================================Spa Dosage ====================================="
    @values = {}
    @pool = Pool.find(p_id)
    # @customer = Customer.find(@pool.customer_id)
    # @role = User.find_by_email(@customer.email)
    # @features = Capability.where(role_id: @role.role_id).to_a
    @role_name = 'ANY' #getuserrole(@customer)
    @auto_pres = true
    # @features.each do |i|
    #   @feature_name = Feature.where(id:i.feature_id).limit(1).pluck(:feature_name).to_a
    #   @feature_name = @feature_name[0]
    #   if @feature_name == "AUTO_PRESCRIPTION"
    #     @auto_pres = true
    #   end
    # end
    if @auto_pres
      if @pool.isPool == false
        @messages = []
        @water_type = "spa"
        if @pool.volume.blank? == false
          @spa_vol_gal = @pool.volume
        else
          @spa_vol_gal = 0
        end
        if @pool.spabox
          @spa_box = true
        else
          @spa_box = false
        end # if spa box check
        #@spa_box = true
        @orp = 0
        @ph = 0
        @ta_i = ""
        @sanitizer = @pool.waterChemistry
        #Fetch manual readins of the customer
        @mreading = @pool.mtests.last

        @reading_type = ""
        if @mreading.blank? == false
          @reading_type = "manual"
          if @mreading.pH.blank? == false
            @ph = @mreading.pH
          end
          if @mreading.freeCl.blank? == false
            @free_cl = @mreading.freeCl.to_d
          end
          if @mreading.totalAlkalinity.blank? == false
            @ta_i = @mreading.totalAlkalinity.to_d
          end
          @daysdiff = ((Time.now - @mreading.created_at)/1.day).round
        end
        if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
          @reading_type = "device"
          unless @pool.epools.blank?
            @pool.epools.each do |epool|
              @orp = epool.oOrp
              unless epool.points.last.blank?  || epool.nil?
                @ph = ph(epool)
                @orp = orp(epool)
                @daysdiff = ((Time.now - epool.points.last.created_at)/1.day).round
              end # if points check
            end # looping of devices
          end # if pool has device check
        end

        if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
          Rails.logger.info("Returning as readings are greater than 3 days")
          return ""
        end

        #decide range of the system and what to be prescribed
        @range = "notgood"
        @phrange = "notgood"
        @chlrange = "notgood"
        @tarange = ""


        if @role_name == "Lora"
          #ph decider
          if @ph.to_d >= 7.2 && @ph.to_d <= 7.8
            @phrange = "good"
            Rails.logger.info("PH RANGE GOOD")
          end
          #chlorine decider
          if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 600 && @orp.to_d <= 900))
            @chlrange = "good"
            Rails.logger.info("CH RANGE GOOD")
          end
          #ta decider
          if @ta_i.blank? == false && @ta_i > 70 && @ta_i <= 90
            @tarange = "good"
            Rails.logger.info("TA RANGE GOOD")
          elsif @ta_i.blank? == false
            @tarange = "notgood"
            Rails.logger.info("TA RANGE NOT GOOD")
          end
        else
          #ph decider
          if @ph.to_d >= 7.4 && @ph.to_d <= 7.6
            @phrange = "good"
            Rails.logger.info("PH RANGE GOOD")
          end
          #chlorine decider
          if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 650 && @orp.to_d <= 900))
            @chlrange = "good"
            Rails.logger.info("CH RANGE GOOD")
          end
          #ta decider
          if @ta_i.blank? == false && @ta_i > 70 && @ta_i <= 90
            @tarange = "good"
            Rails.logger.info("TA RANGE GOOD")
          elsif @ta_i.blank? == false
            @tarange = "notgood"
            Rails.logger.info("TA RANGE NOT GOOD")
          end
        end

        #range decider ends

        if @phrange == "good" && @chlrange == "good" && (@tarange == "good" || @tarange == "")
          deletealldosages(@pool)
          Rails.logger.info("Returning as system is in good range")
          return ""
        end

        @can_populate = false

        case @reading_type
          when "manual"
            Rails.logger.info("Manual reading")
            if @sanitizer == "Bromine"
              Rails.logger.info("BROMINE")
              @br_i = @free_cl
              @can_populate = true
            else
              Rails.logger.info("NOT BROMINE")
              if @sanitizer != "Other"
                Rails.logger.info("NOT OTHERS")
                @fc_i = @free_cl
                @can_populate = true
              end
            end
          when "device"
            puts "Device reading"
            if @sanitizer == "Bromine"
              Rails.logger.info("BROMINE")
              @br_i = 2*(40000-CMath.sqrt(6840000000-9600000*(@orp.to_d)))/5000.0
              @br_i = @br_i.real
              if @br_i < 0
                @br_i = 0
              end
              @can_populate = true
            else
              Rails.logger.info("NOT BROMINE")
              if @sanitizer != "Other"
                Rails.logger.info("NOT OTHERS")
                @fc_i = (40000-CMath.sqrt(6840000000-9600000*(@orp.to_d)))/5000.0
                @fc_i = @fc_i.real
                if @fc_i < 0
                  @fc_i = 0
                end
                @can_populate = true
              end
            end
        end

        #if @ph.to_d <=0 && ((@fc_i.blank? == false && @fc_i.to_d <= 0) || (@br_i.blank? == false && @br_i.to_d <= 0))
        if @can_populate == false
          Rails.logger.info("Returning as readings are not valid")
          return ""
        end
        @ta_i = @ta_i.to_d
        if @pool.waterChemistry == "Salt Water"
          @swg = 1 #salt water chlorinator
          @sanitizer = 'Chlorine'
        else
          @swg = 2 #salt water chlorinator
        end

        if @pool.ozonator
          @ozone = 1 #ozonator
        else
          @ozone = 2 #ozonator
        end

        # Alkalinity Adjustment
        @d_NaHCO3=2.2.*1000 #Density sodium bicarbonate (mg/cm3)
        @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
        @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
        @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
        @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
        @Calc_CPO_NaHCO3=0

        if @swg == 1
          @ta_tar = (@base_maxTA+@base_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
          @delta_TA=@ta_tar-@ta_i
          if @ta_i <= @base_minTA
            @delta_TA=@ta_tar-@ta_i
            @Calc_CPO_NaHCO3 = @CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA
          end
        elsif @swg==2
          @ta_tar=(@acid_maxTA+@acid_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
          @delta_TA=@ta_tar-@ta_i
          if @ta_i<=@acid_minTA
            @delta_TA=@ta_tar-@ta_i
            @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
          elsif @ta_i>=@acid_maxTA+20
            @messages << "Watch TA, should lower over time using tablets."
          end
        else
          @delta_TA=@ta_tar-@ta_i;
        end

        #pH Adjustment
        @Calc_CPO_Na2CO3=0
        @Calc_CPO_NaHSO4=0
        @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
        @max_NaHSO4=20/16.0 #Maximum recommended sodium bisulfate single addition per 10,000 gal

        if @ph<7.0
          @ph=7.0
          @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n"
        elsif @ph>8.0
          @ph=8.0
          @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n"
        end
        puts @ph
        #Base
        if (@ph<7.4 && @ph>=7.2)#7.2<=@ph<7.4
          @CPO_Na2CO3=0.375
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
        elsif (@ph<7.2 && @ph>=7.0)#7.0<=@ph<7.2
          @CPO_Na2CO3=0.5
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
          #Acid
        elsif (@ph<=7.8 && @ph>=7.6)#7.6<=@ph<=7.8
          @CPO_NaHSO4=0.94
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
        elsif (@ph<=8.0 && @ph>7.8)#7.8<@ph<=8.0
          @CPO_NaHSO4=1.25
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
        end

        #Adjusting TA adjustment from pH adjustment
        if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
          @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
          @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@spa_vol_gal/10000.0)) #TA increase from pH increase
          @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
        else
          @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
        end

        #check chlorine range before prescribing sanitizer
        if @chlrange == "notgood"
          if @sanitizer == 'Chlorine' #Check for chlorine
            @minFC=3 # Minimum ideal FC ppm
            @maxFC=4 #Maximum ideal FC ppm
            @FC_tar=(@maxFC+@minFC)/2.0#Target FC
            if @fc_i.blank? == false
              @delta_FC=@FC_tar-@fc_i
            else
              @delta_FC = 0
            end

            if @delta_FC>0 #Check if sanitizer above target
              @divideunit = 1
              if @pool.bleach6 && @pool.bleach6Size.blank? == false && @pool.bleach6Size.to_d > 0
                @Cl_type= 5
              elsif @pool.bleach12 && @pool.bleach12Size.blank? == false && @pool.bleach12Size.to_d > 0
                @Cl_type= 4
              elsif (@pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0) || (@pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0) || (@pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0)
                @Cl_type= 0
              elsif (@pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d > 0) || (@pool.esanitizer.blank? == false && @pool.esanitizer.to_d > 0)
                if @pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d > 0
                  @divideunit = 0.5375
                end
                @Cl_type= 1 #input ('\nSelect Chlorine type by number: \n1:Dichlor 56% ACC\n2:Dichlor 62% ACC\n3:Trichlor\n4:12% Liquid Chlorine\n5:6% Liquid Chlorine (Household Bleach)\nSelection: ');
              elsif @pool.dichlor62 && @pool.dichlor62Size.blank? == false && @pool.dichlor62Size.to_d > 0
                @Cl_type= 2
                @divideunit = 0.5375
              elsif @pool.trichlor3 && @pool.trichlor3Size.blank? == false && @pool.trichlor3Size.to_d > 0
                @Cl_type= 3
              else
                @Cl_type= 0
              end

              if @Cl_type==1
                @Cl_name=('Dichlor 56%')
                @Cl_nametemp=('dichlor_56')
                @CPO_Cl=0.15 #Dichlor 56% ACC CPO factor
              elsif @Cl_type==2
                @Cl_name=('Dichlor 62%')
                @Cl_nametemp=('dichlor_62')
                @CPO_Cl=0.131 #Dichlor 62% ACC CPO factor
              elsif @Cl_type==3
                @Cl_name=('Trichlor')
                @Cl_nametemp=('trichlor')
                @CPO_Cl=0.09375 #Trichlor CPO factor
              elsif @Cl_type==4
                @Cl_name=('12% Liquid Chlorine')
                @Cl_nametemp=('lch12')
                @CPO_Cl=0.0836 #12% Liquid Chlorine
              elsif @Cl_type==5
                @Cl_name=('6% Liquid Chlorine (Household Bleach)')
                @Cl_nametemp=('lch6')
                @CPO_Cl=0.1672 #Approximation 6% Liquid Chlorine
              elsif @Cl_type==0
                @Cl_name='Calcium Hypochlorite'
                if @pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0
                  @Cl_nametemp=('chyp53')
                elsif @pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0
                  @Cl_nametemp=('chyp65')
                elsif @pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0
                  @Cl_nametemp=('chyp73')
                end
                @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
              else
                @CPO_Cl=0
              end

              #check if no chemical is available and use last used sanitizer
              if @Cl_nametemp.blank?
                Rails.logger.info("Chemical name empty.. Going to use last used sanitizer")
                @lastused = getlastusedchlorine(@pool)
                @Cl_type = @lastused
                Rails.logger.info("Last used sanitizer : "+@lastused.to_s)
                if @lastused==1
                  @Cl_name=('Dichlor 56%')
                  @Cl_nametemp=('dichlor_56')
                  @CPO_Cl=0.15 #Dichlor 56% ACC CPO factor
                  @divideunit = 0.5375
                elsif @lastused==2
                  @Cl_name=('Dichlor 62%')
                  @Cl_nametemp=('dichlor_62')
                  @CPO_Cl=0.131 #Dichlor 62% ACC CPO factor
                  @divideunit = 0.5375
                elsif @lastused==3
                  @Cl_name=('Trichlor')
                  @Cl_nametemp=('trichlor')
                  @CPO_Cl=0.09375 #Trichlor CPO factor
                elsif @lastused==4
                  @Cl_name=('12% Liquid Chlorine')
                  @Cl_nametemp=('lch12')
                  @CPO_Cl=0.0836 #12% Liquid Chlorine
                elsif @lastused==5
                  @Cl_name=('6% Liquid Chlorine (Household Bleach)')
                  @Cl_nametemp=('lch6')
                  @CPO_Cl=0.1672 #Approximation 6% Liquid Chlorine
                elsif @lastused==0
                  @Cl_name='Calcium Hypochlorite'
                  if @pool.calhypo53
                    @Cl_nametemp=('chyp53')
                  elsif @pool.calhypo65
                    @Cl_nametemp=('chyp65')
                  elsif @pool.calhypo73
                    @Cl_nametemp=('chyp73')
                  end
                  @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                else
                  @CPO_Cl=0
                end
                Rails.logger.info("Chemical name empty.. Going to use last used sanitizer : "+@Cl_name)
              end
              if @swg==2 || @swg==1 #No chlorine generator => manual chlorine
                #puts "TYPE : "+@Cl_type.to_s
                #puts "NAME : "+@Cl_name
                @Calc_CPO_Cl=@CPO_Cl*@spa_vol_gal/10000.0*@delta_FC
                if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                  if @Calc_CPO_Cl>=0.25 #Check for gallons
                    @wholegal_Cl=@Calc_CPO_Cl.floor  #Number of whole gallons of chlorine
                    @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                    @messages << "\nAdd "+ @wholegal_Cl.to_s+" gallon(s) and "+ @quartergal_Cl.to_s+" quarter gallon(s) of "+ @Cl_name +" to the "+ @water_type+" evenly.\n\n"
                    @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                    @wholecup_Cl=(@Calc_CPO_Cl*16).floor #Number of whole cups of chlorine
                    @quartercup_Cl=(((@Calc_CPO_Cl*16)-@wholecup_Cl)/0.25).round #Number of quarter cups of chlorine
                    @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                    @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                  elsif @Calc_CPO_Cl<0.0625
                    @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                    @messages << "\nAdd "+@wholetbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                    @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                  end
                elsif @Calc_CPO_Cl>=0.25 && @spa_box==false #Check for less than 1 pound and does not use spa box
                  @wholecup_Cl=(@Calc_CPO_Cl/@divideunit).floor #Assumes 1 cup = 0.5375 lb only for dichlor
                  @quartercup_Cl=(((@Calc_CPO_Cl/@divideunit)-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                  @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\nor by filling tab dispensers.\n\n"
                  @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @sanitizer == "Chlorine" #Check for greater than zero and does not use spa box
                  @ozCalc_CPO_Cl=(@Calc_CPO_Cl/@divideunit)*16 #Convert to ounces
                  @tbs_Cl=(@ozCalc_CPO_Cl*2).ceil #Convert to tbs
                  if @spa_box==true #Uses spa box
                    @quarteroz_Cl=@ozCalc_CPO_Cl/0.25 #Total number of 0.25oz chlorine bags
                    @bags_Cl=@quarteroz_Cl.ceil #Number of whole 0.25oz chlorine packets
                    @messages << "\nAdd "+@bags_Cl.to_s+", 0.25 oz package(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                    @values.store(@Cl_nametemp, @bags_Cl.to_s+";pac")
                  elsif @spa_box==false #Does not use spa box
                    @messages << "\nAdd "+@tbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                    @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                  end
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @sanitizer == "Bromine" #Check for greater than zero
                  @messages << "\nAdd 2 bromine tablets in tab dispensers.\n\n"
                  @values.store("bromine", "2")
                end
                #elsif @swg==1 #Salt water chlorine generator and low chlorine
                #  @messages << "Chlorine is low.\nSalt water chlorinator, circulation, and salt concentration need to be checked.\n\n"
              end
            elsif @delta_FC<=0 #Check if chlorine is at or above target
              @messages << "\nChlorine is within range, re-fill tablets if needed.\n\n"
            end
          elsif @sanitizer == "Bromine" #Bromine system
            @minBr=4 #Minimum ideal Br ppm
            @maxBr=6 #Maximum ideal Br ppm
            @Br_tar=(@maxBr+@minBr)/2 #Target Br
            @delta_Br=@Br_tar-@br_i #Difference of bromine target and initial concentration
            if BigDecimal(@delta_Br.to_s) > 0
              @messages << "Bromine low, refill Bromine tablets.\n\n"
            else
              @messages << "Bromine in range, check tablets.\n\n"
            end
          end
        end
        #chlorine range check ends

        if (@daysdiff.blank? == false && @daysdiff > 3) || (@ta_i.blank? == false && @ta_i.to_d <=0)
          @adj_Calc_CPO_NaHCO3=0
        end

        #check for ta range before prescribing ta
        if @tarange == "notgood"
          #Adjusted TA Dose Report #spaboxcondition
          if (@adj_Calc_CPO_NaHCO3>=0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3 && @spa_box == false) #Check for pounds and maximum per 10,000 gallons
            @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
            @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
            @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
          elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
            @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
            @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
            if @spa_box==true #Uses spa box
              @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
              @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
              @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\nThis will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjusments. \nThis will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
            end
          else
            if @adj_Calc_CPO_NaHCO3>=0.25 && @spa_box == false
              @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
              @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
            elsif (@adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
              @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
              @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
              if @spa_box==true #Uses spa box
                @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
                @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
                @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
              end
            end
          end
        end
        #ta range check ends

        #check for ph range before prescribing ph
        if @phrange == "notgood"
          #pH Dose Report
          if (@Calc_CPO_Na2CO3>=0.25 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for pounds and maximum per 10,000 gallons
            @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
            @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate to increase pH \n"
            @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
          elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for oz. and maximum per 10,000 gallons
            @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
            @tbs_Na2CO3=(@Calc_CPO_Na2CO3*2).round #Converts oz to tbs
            if @spa_box==true #Uses spa box
              @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
              @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
              @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate to increase pH \n"
              @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate to increase pH \n"
              @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
            end
          else #greater than maximum dose per 10,000 gallons
            if @Calc_CPO_Na2CO3>=0.25
              @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
              @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
              @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
            elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0)
              @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
              @tbs_Na2CO3=(@Calc_CPO_CPO_Na2CO3*2).round #Converts oz to tbs
              if @spa_box==true #Uses spa box
                @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
                @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
                @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
              end
            end
          end
          #puts @Calc_CPO_NaHSO4
          if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
            @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #Assumes 1 cup = 0.574 lb
            @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite to lower pH \n"
            @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
          elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000) #Check for ounces and max dose per 10,000 gallons
            @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16) #Convert to ounces
            @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
            if @spa_box==true #Uses spa box
              @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
              @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
              @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite to lower pH \n"
              @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite to lower pH \n"
              @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
            end
          else #greater than maximum dose per 10,000 gallons
            if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
              @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #assumes 1 cup = 0.574 lb
              @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
              @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
            elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
              @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16) #Convert to ounces
              @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
              if @spa_box==true #Uses spa box
                @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
                @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
                @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
              end
            end
          end
        end
        #ph range check ends
        puts @messages
        puts @values
        Rails.logger.info("DOSAGE AUTO : "+@values.inspect)
      end #if spa check
      puts "==================================Spa Dosage ends================================="
      return @values
    else
      # AUTO_PRESCRIPTION is not enabled
      return @values
    end
  end

  def processchemstock(pool)
    @pool = pool
    @chemical_amount = 2
    if @pool.isPool == false
      @chemical_amount = 0.75
    end
    @low = ""
    if @pool.trichlor2.blank? == false && @pool.trichlor2 && @pool.trichlor2Size.blank? == false && @pool.trichlor2Size.to_d <= @chemical_amount
      @low += "~Trichlor:"+@pool.trichlor2Size.to_s
    end
    if @pool.bromine1.blank? == false && @pool.bromine1 && @pool.bromine1Size.blank? == false && @pool.bromine1Size.to_d <= @chemical_amount
      @low += "~Bromine:"+@pool.bromine1Size.to_s
    end
    if @pool.lithiumhypo.blank? == false && @pool.lithiumhypo && @pool.lithiumhypoSize.blank? == false && @pool.lithiumhypoSize.to_d <= @chemical_amount
      @low += "~Lithium Hypo:"+@pool.lithiumhypoSize.to_s
    end
    if @pool.trichlor3.blank? == false && @pool.trichlor3 && @pool.trichlor3Size.blank? == false && @pool.trichlor3Size.to_d <= @chemical_amount
      @low += "~Trichlor 3:"+@pool.trichlor3Size.to_s
    end
    if @pool.bleach6.blank? == false && @pool.bleach6 && @pool.bleach6Size.blank? == false && @pool.bleach6Size.to_d <= @chemical_amount
      @low += "~Liquid Chlorine 6%:"+@pool.bleach6Size.to_s
    end
    if @pool.bleach8.blank? == false && @pool.bleach8 && @pool.bleach8Size.blank? == false && @pool.bleach8Size.to_d <= @chemical_amount
      @low += "~Liquid Chlorine 8%:"+@pool.bleach8Size.to_s
    end
    if @pool.bleach12.blank? == false && @pool.bleach12 && @pool.bleach12Size.blank? == false && @pool.bleach12Size.to_d <= @chemical_amount
      @low += "~Liquid Chlorine 12%:"+@pool.bleach12Size.to_s
    end
    if @pool.dichlor.blank? == false && @pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d <= @chemical_amount
      @low += "~Dichlor:"+@pool.dichlorSize.to_s
    end
    if @pool.dichlor62.blank? == false && @pool.dichlor62 && @pool.dichlor62Size.blank? == false && @pool.dichlor62Size.to_d <= @chemical_amount
      @low += "~Dichlor 62:"+@pool.dichlor62Size.to_s
    end
    if @pool.calhypo53.blank? == false && @pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d <= @chemical_amount
      @low += "~Cal Hypo 53:"+@pool.calhypo53Size.to_s
    end
    if @pool.calhypo65.blank? == false && @pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d <= @chemical_amount
      @low += "~Cal Hypo 65:"+@pool.calhypo65Size.to_s
    end
    if @pool.calhypo73.blank? == false && @pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d <= @chemical_amount
      @low += "~Cal Hypo 73:"+@pool.calhypo73Size.to_s
    end
    if @pool.calcium.blank? == false && @pool.calcium && @pool.calciumSize.blank? == false && @pool.calciumSize.to_d <= @chemical_amount
      @low += "~Calcium:"+@pool.calciumSize.to_s
    end
    if @pool.cyanuricsolid.blank? == false && @pool.cyanuricsolid && @pool.cyanuricsolidSize.blank? == false && @pool.cyanuricsolidSize.to_d <= @chemical_amount
      @low += "~Cyanuric Solid:"+@pool.cyanuricsolidSize.to_s
    end
    if @pool.cyanuricliquid.blank? == false && @pool.cyanuricliquid && @pool.cyanuricliquidSize.blank? == false && @pool.cyanuricliquidSize.to_d <= @chemical_amount
      @low += "~Cyanuric Liquid:"+@pool.cyanuricliquidSize.to_s
    end

    #Extra chemicals section ends

    if @pool.sodaash.blank? == false && @pool.sodaash && @pool.sodaashSize.blank? == false && @pool.sodaashSize.to_d <= @chemical_amount
      @low += "~Soda Ash:"+@pool.sodaashSize.to_s
    end
    if @pool.sodium.blank? == false && @pool.sodium && @pool.sodiumSize.blank? == false && @pool.sodiumSize.to_d <= @chemical_amount
      @low += "~Sodium:"+@pool.sodiumSize.to_s
    end
    if @pool.borax.blank? == false && @pool.borax && @pool.boraxSize.blank? == false && @pool.boraxSize.to_d <= @chemical_amount
      @low += "~Borax:"+@pool.boraxSize.to_s
    end
    if @pool.dryacid.blank? == false && @pool.dryacid && @pool.dryacidSize.blank? == false && @pool.dryacidSize.to_d <= @chemical_amount
      @low += "~Dry Acid:"+@pool.dryacidSize.to_s
    end
    if @pool.muriatic15.blank? == false && @pool.muriatic15 && @pool.muriatic15Size.blank? == false && @pool.muriatic15Size.to_d <= @chemical_amount
      @low += "~Muriatic Acid 15%:"+@pool.muriatic15Size.to_s
    end
    if @pool.muriatic31.blank? == false && @pool.muriatic31 && @pool.muriatic31Size.blank? == false && @pool.muriatic31Size.to_d <= @chemical_amount
      @low += "~Muriatic Acid 31%:"+@pool.muriatic31Size.to_s
    end

    #For spa
    if @pool.isPool == false  && @pool.spabox
      if @pool.esanitizer.blank? == false && @pool.esanitizer.to_d <= @chemical_amount
        @low += "~Packet E:"+@pool.esanitizer.to_s
      end
      if pool.ddryacid.blank? == false && @pool.ddryacid.to_d <= @chemical_amount
        @low += "~Packet D:"+@pool.ddryacid.to_s
      end
      if @pool.bbakingsoda.blank? == false && @pool.bbakingsoda.to_d <= @chemical_amount
        @low += "~Packet B:"+@pool.bbakingsoda.to_s
      end

      if @pool.bsodaash.blank? == false && @pool.bsodaash.to_d <= @chemical_amount
        @low += "~Packet A:"+@pool.bbakingsoda.to_s
      end
    end
    Rails.logger.info("Low Chemicals : "+@low)
    return @low
  end


  #function gets the low chemicals after substracting the new dosage from stock

  def processchemstockbeforecomplete(pool,dosage)
    @pool = pool
    @low = ""
    if @pool.trichlor2.blank? == false && @pool.trichlor2 && @pool.trichlor2Size.blank? == false && dosage.trichlor2Size.blank? == false && (@pool.trichlor2Size.to_d - dosage.trichlor2Size.to_d) <= 0
      @low += "~Trichlor:"+(@pool.trichlor2Size.to_d - dosage.trichlor2Size.to_d).to_s
    end
    if @pool.bromine1.blank? == false && @pool.bromine1 && @pool.bromine1Size.blank? == false && dosage.bromine1Size.blank? == false && (@pool.bromine1Size.to_d - dosage.bromine1Size.to_d) <= 0
      @low += "~Bromine:"+(@pool.bromine1Size.to_d - dosage.bromine1Size.to_d).to_s
    end
    if @pool.lithiumhypo.blank? == false && @pool.lithiumhypo && @pool.lithiumhypoSize.blank? == false && dosage.lithiumhypoSize.blank? == false && (@pool.lithiumhypoSize.to_d - dosage.lithiumhypoSize.to_d) <= 0
      @low += "~Lithium Hypo:"+(@pool.lithiumhypoSize.to_d - dosage.lithiumhypoSize.to_d).to_s
    end
    if @pool.calcium.blank? == false && @pool.calcium && @pool.calciumSize.blank? == false && dosage.calciumSize.blank? == false && (@pool.calciumSize.to_d - dosage.calciumSize.to_d) <= 0
      @low += "~Calcium:"+(@pool.calciumSize.to_d - dosage.calciumSize.to_d).to_s
    end
    if @pool.cyanuricsolid.blank? == false && @pool.cyanuricsolid && @pool.cyanuricsolidSize.blank? == false && dosage.cyanuricsolidSize.blank? == false && (@pool.cyanuricsolidSize.to_d - dosage.cyanuricsolidSize.to_d) <= 0
      @low += "~Cyanuric Solid:"+(@pool.cyanuricsolidSize.to_d - dosage.cyanuricsolidSize.to_d).to_s
    end
    if @pool.cyanuricliquid.blank? == false && @pool.cyanuricliquid && @pool.cyanuricliquidSize.blank? == false && dosage.cyanuricliquidSize.blank? == false  && (@pool.cyanuricliquidSize.to_d - dosage.cyanuricliquidSize.to_d) <= 0
      @low += "~Cyanuric Liquid:"+(@pool.cyanuricliquidSize.to_d - dosage.cyanuricliquidSize.to_d).to_s
    end

    #Extra chemicals section ends

    if @pool.trichlor3.blank? == false && @pool.trichlor3 && @pool.trichlor3Size.blank? == false && dosage.trichlor.blank? == false && (@pool.trichlor3Size.to_d - dosage.trichlor.to_d) <= 0
      @low += "~Trichlor 3:"+(@pool.trichlor3Size.to_d - dosage.trichlor.to_d).to_s
    end
    if @pool.sodaash.blank? == false && @pool.sodaash && @pool.sodaashSize.blank? == false && dosage.phUp.blank? == false && (@pool.sodaashSize.to_d - dosage.phUp.to_d) <= 0
      @low += "~Soda Ash:"+(@pool.sodaashSize.to_d - dosage.phUp.to_d).to_s
    end
    if @pool.dryacid.blank? == false && @pool.dryacid && @pool.dryacidSize.blank? == false && dosage.phDown.blank? == false && (@pool.dryacidSize.to_d - dosage.phDown.to_d) <= 0
      @low += "~Dry Acid:"+(@pool.dryacidSize.to_d - dosage.phDown.to_d).to_s
    end
    if @pool.sodium.blank? == false && @pool.sodium && @pool.sodiumSize.blank? == false && dosage.taUp.blank? == false && (@pool.sodiumSize.to_d - dosage.taUp.to_d) <= 0
      @low += "~Sodium:"+(@pool.sodiumSize.to_d - dosage.taUp.to_d).to_s
    end
    if @pool.bleach6.blank? == false && @pool.bleach6 && @pool.bleach6Size.blank? == false && dosage.liquidCl6.blank? == false && (@pool.bleach6Size.to_d - dosage.liquidCl6.to_d) <= 0
      @low += "~Liquid Chlorine 6%:"+(@pool.bleach6Size.to_d - dosage.liquidCl6.to_d).to_s
    end
    if @pool.bleach8.blank? == false && @pool.bleach8 && @pool.bleach8Size.blank? == false && dosage.liquidCl8.blank? == false && (@pool.bleach8Size.to_d - dosage.liquidCl8.to_d) <= 0
      @low += "~Liquid Chlorine 8%:"+(@pool.bleach8Size.to_d - dosage.liquidCl8.to_d).to_s
    end
    if @pool.bleach12.blank? == false && @pool.bleach12 && @pool.bleach12Size.blank? == false && dosage.liquidChlorine.blank? == false && (@pool.bleach12Size.to_d - dosage.liquidChlorine.to_d) <= 0
      @low += "~Liquid Chlorine 12%:"+(@pool.bleach12Size.to_d - dosage.liquidChlorine.to_d).to_s
    end
    if @pool.dichlor.blank? == false && @pool.dichlor && @pool.dichlorSize.blank? == false && dosage.dichlor.blank? == false && (@pool.dichlorSize.to_d - dosage.dichlor.to_d) <= 0
      @low += "~Dichlor:"+(@pool.dichlorSize.to_d - dosage.dichlor.to_d).to_s
    end
    if @pool.dichlor62.blank? == false && @pool.dichlor62 && @pool.dichlor62Size.blank? == false && dosage.dichlor62Size.blank? == false && (@pool.dichlor62Size.to_d - dosage.dichlor62Size.to_d) <= 0
      @low += "~Dichlor 62:"+(@pool.dichlor62Size.to_d - dosage.dichlor62Size.to_d).to_s
    end
    if @pool.calhypo53.blank? == false && @pool.calhypo53 && @pool.calhypo53Size.blank? == false && dosage.calhypo53.blank? == false && (@pool.calhypo53Size.to_d - dosage.calhypo53.to_d) <= 0
      @low += "~Cal Hypo 53:"+(@pool.calhypo53Size.to_d - dosage.calhypo53.to_d).to_s
    end
    if @pool.calhypo65.blank? == false && @pool.calhypo65 && @pool.calhypo65Size.blank? == false && dosage.calhypo65.blank? == false &&(@pool.calhypo65Size.to_d - dosage.calhypo65.to_d) <= 0
      @low += "~Cal Hypo 65:"+(@pool.calhypo65Size.to_d - dosage.calhypo65.to_d).to_s
    end
    if @pool.calhypo73.blank? == false && @pool.calhypo73 && @pool.calhypo73Size.blank? == false && dosage.calhypo73.blank? == false && (@pool.calhypo73Size.to_d - dosage.calhypo73.to_d) <= 0
      @low += "~Cal Hypo 73:"+(@pool.calhypo73Size.to_d - dosage.calhypo73.to_d).to_s
    end
    if @pool.borax.blank? == false && @pool.borax && @pool.boraxSize.blank? == false && dosage.phUpBorax.blank? == false && (@pool.boraxSize.to_d - dosage.phUpBorax.to_d) <= 0
      @low += "~Borax:"+(@pool.boraxSize.to_d - dosage.phUpBorax.to_d).to_s
    end
    if @pool.muriatic15.blank? == false && @pool.muriatic15 && @pool.muriatic15Size.blank? == false && dosage.muriatic15.blank? == false && (@pool.muriatic15Size.to_d - dosage.muriatic15.to_d) <= 0
      @low += "~Muriatic Acid 15%:"+(@pool.muriatic15Size.to_d - dosage.muriatic15.to_d).to_s
    end
    if @pool.muriatic31.blank? == false && @pool.muriatic31 && @pool.muriatic31Size.blank? == false && dosage.muriatic31.blank? == false && (@pool.muriatic31Size.to_d - dosage.muriatic31.to_d) <= 0
      @low += "~Muriatic Acid 31%:"+(@pool.muriatic31Size.to_d - dosage.muriatic31.to_d).to_s
    end

    #For spa
    if @pool.isPool == false  && @pool.spabox
      if @pool.esanitizer.blank? == false && dosage.esanitizer.blank? == false && (@pool.esanitizer.to_d - dosage.esanitizer.to_d) <= 0
        @low += "~Packet E:"+(@pool.esanitizer.to_d - dosage.esanitizer.to_d).to_s
      end
      if pool.ddryacid.blank? == false && dosage.ddryacid.blank? == false && (@pool.ddryacid.to_d - dosage.ddryacid.to_d) <= 0
        @low += "~Packet D:"+(@pool.ddryacid.to_d - dosage.ddryacid.to_d).to_s
      end
      if @pool.bbakingsoda.blank? == false && dosage.bbakingsoda.blank? == false && (@pool.bbakingsoda.to_d - dosage.bbakingsoda.to_d) <= 0
        @low += "~Packet B:"+(@pool.bbakingsoda.to_d - dosage.bbakingsoda.to_d).to_s
      end

      if @pool.bsodaash.blank? == false && dosage.bsodaash.blank? == false && (@pool.bsodaash.to_d - dosage.bsodaash.to_d) <= 0
        @low += "~Packet A:"+(@pool.bsodaash.to_d - dosage.bsodaash.to_d).to_s
      end
    end
    Rails.logger.info("CREATE PRESC########Low Chemicals : "+@low)
    return @low
  end

  #check and notify about the chemical stock low
  def notifylowstock(where,pool,dosage)
    @content = ""
    if where == "create"
      @content = processchemstockbeforecomplete(pool,dosage)
      @message = Message.find_by_short("CHEMICALS_GOING_LOW")
    elsif where == "done"
      @message = Message.find_by_short("CHEMICALS_STOCK_LOW")
      @content = processchemstock(pool)
    end

    if @content.blank? == false
      @raw = @content
      @details = getchemicallowcontentformemail(@content)
      @content = @message.content.gsub("[details]", @details)
      Rails.logger.info("Content YES")
      @customer = Customer.find(@pool.customer_id)
      if @customer.blank? == false
        Rails.logger.info("Customer YES")
        updatelowstockcontact(@customer.email,@raw)
        #@trigger = Trigger.find_by_short("CHEMICAL_LOW")
        #if @trigger.blank? == false
        #	Rails.logger.info("Trigger YES")
        #	@body = "The chemicals stock getting low : Details - "+@content
        #	SesMailer.customeremail("gowtham@tentsoftware.com","Chemicals Low for "+@customer.email,@body).deliver
        #triggerautopilot(@customer.email,@trigger.trigger_id)
        #	updatelowstockcontact(@customer.email)
        #	puts "NEED TO SEND CHEM LOW EMAIL"+@content.to_s
        #end
      end
    end
    Rails.logger.info("Content Returnding : "+@content)
    return @content

  end

  #process the raw low chemical content and make it for email
  def getchemicallowcontentformemail(content)
    @content = content.split("~")
    @texthere = ""
    #Rails.logger.info("Content Orig : "+@content.inspect)
    @content = @content.uniq
    #Rails.logger.info("Content Uniq : "+@content.inspect)
    @temp_iter = 0
    @chem_type_arr = []
    @content.each_with_index do |chem,index|
      if chem != ""
        #puts chem.inspect
        @chem_name = chem.split(":")[0]
        @chem_amount = chem.split(":")[1]
        #puts "Chem : "+@chem_name
        #Rails.logger.info("Chem : "+@chem_name.to_s)
        @chem_type_arr << getchemicallowtype(@chem_name)
      end
    end

    #puts @chem_type_arr.inspect
    @chem_type_arr = @chem_type_arr.uniq
    #puts @chem_type_arr.inspect
    @chem_type_arr.each do |chem_type|
      if chem_type == @chem_type_arr.last && @temp_iter > 0
        @texthere += " and "
      else
        if @texthere.blank? == false
          @texthere += ", "
        end
        @temp_iter = @temp_iter+1
      end
      @texthere += chem_type
    end
    #Rails.logger.info("Text Final : "+@texthere.to_s)
    return @texthere
  end

  def getuserrole(customer)
    @role = ""
    @user = User.where(email:customer.email).first
    if @user
      @user_role = Role.where(id: @user.role_id).limit(1).pluck(:role_name)
      if @user_role
        @role = @user_role[0]
      end
    end
    return @role
  end

  def triggerautopilot(email,triggerid)

    @email = email

    HTTParty.post("https://api2.autopilothq.com/v1/trigger/#{triggerid}/contact/#{@email}", :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7"},  :verify => false)
    #puts HTTParty.get("https://api2.autopilothq.com/v1/triggers", :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7"},  :verify => false)
    #puts HTTParty.get("https://api2.autopilothq.com/v1/contacts/bookmark", :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7"},  :verify => false)
    #headers = {
    #  :autopilotapikey => 'b6f2e3661bc04743a2e401da6af403d7'
    #}

    #response = RestClient.post 'https://api2.autopilothq.com/v1/trigger/#{triggerid}/contact/#{@email}', headers
    #puts response
  end

  # Function used to add a contact to auto pilot
  def addcontactautopilot(emailid)
    @list_id = "contactlist_B1A9F53A-E1AE-4FB1-A5F5-9FD2014DD7F6" # Prefix "contactlist_" to the list id
    @contact = {:FirstName=>"",:LastName=>"",:Email=>emailid}
    @custom = {:Sutro=>"Orange Silicon Valley"}
    @body = {:contact => @contact, :custom => @custom}.to_json
    response = HTTParty.post("https://api2.autopilothq.com/v1/contact", :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7","Content-Type"=>"application/json"},:body => @body,:verify => false)
    addcontacttoalist(emailid,@list_id)
  end

  #Function used to add a contact to a list
  def addcontacttoalist(emailid,list_id)
    response = HTTParty.post("https://api2.autopilothq.com/v1/list/"+list_id+"/contact/"+emailid, :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7","Content-Type"=>"application/json"},:verify => false)
  end

  def time_diff_dosing_lora(start_time, end_time)
    seconds_diff = (start_time - end_time).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    #"#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
    @timediff = ""

    @diff = Rails.application.config.loradosinghours.to_i - ((hours.to_s.rjust(2, '0').to_i * 60)+(minutes.to_s.rjust(2, '0').to_i))
    #puts @diff
    #@timediff = distance_of_time_in_words(@diff.to_i * 60 *60)

    @timediff = @diff

    #if hours.to_s.rjust(2, '0').to_i > 0
    #@timediff = hours.to_s.rjust(2, '0')+" Hours "
    #end
    #if minutes.to_s.rjust(2, '0').to_i > 0
    #@timediff = minutes.to_s.rjust(2, '0')+" Minutes "
    #end
    @timediff

    #"#{mins.to_s.rjust(2, '0')}"
  end


  def spadosagecalcapi(p_id,ph,orp,ta)
    puts "==================================Spa Dosage ====================================="
    @values = {}
    @pool = Pool.find(p_id)
    @customer = Customer.find(@pool.customer_id)
    @role = User.find_by_email(@customer.email)
    @features = Capability.where(role_id: @role.role_id).to_a
    @auto_pres = false
    @features.each do |i|
      @feature_name = Feature.where(id:i.feature_id).limit(1).pluck(:feature_name).to_a
      @feature_name = @feature_name[0]
      if @feature_name == "AUTO_PRESCRIPTION"
        @auto_pres = true
      end
    end
    if @auto_pres
      if @pool.isPool == false
        @messages = []
        @water_type = "spa"
        if @pool.volume.blank? == false
          @spa_vol_gal = @pool.volume
        else
          @spa_vol_gal = 0
        end
        if @pool.spabox
          @spa_box = true
        else
          @spa_box = false
        end # if spa box check

        @orp = 0
        @ph = 0
        @free_cl = 0
        @ta_i = 0
        @daysdiff = 0

        #Fetch manual readins of the customer
        @mreading = @pool.mtests.last

        if ph.blank? == false
          @ph = ph
          if @ph.to_d < 7
            @ph = 7
          end
          if @ph.to_d > 8
            @ph = 8
          end
        end

        if orp.blank? == false
          @free_cl = orp
        end

        if ta.blank? == false
          @ta_i = ta
        end

        @sanitizer = @pool.waterChemistry



        #Testing Done

        if @pool.waterChemistry == "Salt Water"
          @swg = 1 #salt water chlorinator
        else
          @swg = 2 #salt water chlorinator
        end

        #Decide Primary sanitizer
        #if @sanitizer == "Bromine"
        if @daysdiff.blank? == false && @daysdiff <= 3 && @sanitizer == "Bromine"
          @br_i = 2*(40000-CMath.sqrt(6840000000-9600000*(@orp.to_d)))/5000.0
          #if @br_i.to_d < 0
          #	@br_i = 0
          #end

          @orp_i = @orp.to_d
          @primary_sanitizer = 1
        elsif @sanitizer != "Bromine" && @sanitizer != "Other" && @swg == 2
          @fc_i = (40000-CMath.sqrt(6840000000-9600000*(@orp.to_d)))/5000.0
          #if @fc_i.to_d < 0
          #	@fc_i = 0
          #end
          @orp_i = @orp.to_d
          @primary_sanitizer = 2
        else
          @fc_i = 0
          @orp_i = @orp.to_d
          @primary_sanitizer = 3
        end

        if @sanitizer == "Bromine" && @daysdiff < 3
          @br_i = @free_cl
        elsif @sanitizer != "Bromine" && @sanitizer != "Other" && @swg == 2 && @daysdiff < 3
          @fc_i = @free_cl
        end

        if @pool.ozonator
          @ozone = 1 #ozonator
        else
          @ozone = 2 #ozonator
        end

        # Alkalinity Adjustment
        @d_NaHCO3=2.2.*1000 #Density sodium bicarbonate (mg/cm3)
        @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
        @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
        @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
        @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
        @Calc_CPO_NaHCO3=0

        if @swg == 1
          @ta_tar = (@base_maxTA+@base_minTA)/2 #Calculates target for the total alkalinity average of ideal range
          if @ta_i <= @base_minTA
            @delta_TA=@ta_tar-@ta_i
            @Calc_CPO_NaHCO3 = @CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA
          end
        elsif @swg==2
          @ta_tar=(@acid_maxTA+@acid_minTA)/2 #Calculates target for the total alkalinity average of ideal range
          @delta_TA=@ta_tar-@ta_i
          if @ta_i<=@acid_minTA
            @delta_TA=@ta_tar-@ta_i
            @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
          elsif @ta_i>=@acid_maxTA+20
            @messages << "Watch TA, should lower over time using tablets."
          end
        else
          @delta_TA=@ta_tar-@ta_i;
        end

        #pH Adjustment
        @Calc_CPO_Na2CO3=0
        @Calc_CPO_NaHSO4=0
        @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
        @max_NaHSO4=20/16.0 #Maximum recommended sodium bisulfate single addition per 10,000 gal

        if @ph<7.0
          @ph=7.0
          @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n"
        elsif @ph>8.0
          @ph=8.0
          @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n"
        end
        puts @ph
        #Base
        if (@ph<7.4 && @ph>=7.2)#7.2<=@ph<7.4
          @CPO_Na2CO3=0.375
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
        elsif (@ph<7.2 && @ph>=7.0)#7.0<=@ph<7.2
          @CPO_Na2CO3=0.5
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
          #Acid
        elsif (@ph<=7.8 && @ph>=7.6)#7.6<=@ph<=7.8
          @CPO_NaHSO4=0.94
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
        elsif (@ph<=8.0 && @ph>7.8)#7.8<@ph<=8.0
          @CPO_NaHSO4=1.25
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
        end

        #Adjusting TA adjustment from pH adjustment
        if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
          @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
          @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@spa_vol_gal/10000.0)) #TA increase from pH increase
          @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
        else
          @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
        end

        if @primary_sanitizer==3 #Check for ORP input with chlorine
          @primary_sanitizer=2
        end

        if @primary_sanitizer==4 #Check for ORP input with bromine
          @primary_sanitizer=1
        end

        if @sanitizer == 'Chlorine' #Check for chlorine
          @minFC=2 # Minimum ideal FC ppm
          @maxFC=4 #Maximum ideal FC ppm
          @FC_tar=(@maxFC+@minFC)/2#Target FC
          @delta_FC=@FC_tar-@fc_i
          if @delta_FC>0 #Check if sanitizer above target
            if @swg==2 #No chlorine generator => manual chlorine
              if @pool.dichlor || @pool.esanitizer.to_d > 0
                @Cl_type= 1 #input ('\nSelect Chlorine type by number: \n1:Dichlor 56% ACC\n2:Dichlor 62% ACC\n3:Trichlor\n4:12% Liquid Chlorine\n5:6% Liquid Chlorine (Household Bleach)\nSelection: ');
              elsif @pool.dichlor62
                @Cl_type= 2
              elsif @pool.trichlor3
                @Cl_type= 3
              elsif @pool.bleach6
                @Cl_type= 5
              elsif @pool.bleach12
                @Cl_type= 4
              elsif @pool.calhypo53
                @Cl_type= 0
              else
                @Cl_type= 0
              end

              if @Cl_type==1
                @Cl_name=('Dichlor 56%')
                @Cl_nametemp=('dichlor_56')
                @CPO_Cl=0.15 #Dichlor 56% ACC CPO factor
              elsif @Cl_type==2
                @Cl_name=('Dichlor 62%')
                @Cl_nametemp=('dichlor_62')
                @CPO_Cl=0.131 #Dichlor 62% ACC CPO factor
              elsif @Cl_type==3
                @Cl_name=('Trichlor')
                @Cl_nametemp=('trichlor')
                @CPO_Cl=0.09375 #Trichlor CPO factor
              elsif @Cl_type==4
                @Cl_name=('12% Liquid Chlorine')
                @Cl_nametemp=('lch12')
                @CPO_Cl=0.0836 #12% Liquid Chlorine
              elsif @Cl_type==5
                @Cl_name=('6% Liquid Chlorine (Household Bleach)')
                @Cl_nametemp=('lch6')
                @CPO_Cl=0.0418 #Approximation 6% Liquid Chlorine
              elsif @Cl_type==0
                @Cl_name='Calcium Hypochlorite'
                @Cl_nametemp=('chyp53')
                @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
              else
                @CPO_Cl=0
              end

              #puts "TYPE : "+@Cl_type.to_s
              #puts "NAME : "+@Cl_name
              @Calc_CPO_Cl=@CPO_Cl*@spa_vol_gal/10000.0*@delta_FC
              if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                if @Calc_CPO_Cl>=0.25 #Check for gallons
                  @wholegal_Cl=@Calc_CPO_Cl.floor  #Number of whole gallons of chlorine
                  @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                  @messages << "\nAdd "+ @wholegal_Cl +" gallon(s) and "+ @quartergal_Cl+" quarter gallon(s) of "+ @Cl_name +" to the "+ @water_type+" evenly.\n\n"
                  @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                  @wholecup_Cl=(@Calc_CPO_Cl/16).floor #Number of whole cups of chlorine
                  @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.0625).round #Number of quarter cups of chlorine
                  @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                  @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                elsif @Calc_CPO_Cl<0.0625
                  @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                  @messages << "\nAdd "+@wholetbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                  @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                end
              elsif @Calc_CPO_Cl>=0.25 && @spa_box==false #Check for less than 1 pound and does not use spa box
                @wholecup_Cl=@Calc_CPO_Cl.floor #Assumes 1 cup = 1 lb
                @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\nor by filling tab dispensers.\n\n"
                @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
              elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @primary_sanitizer==2 #Check for greater than zero and does not use spa box
                @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert to ounces
                @tbs_Cl=(@ozCalc_CPO_Cl*2).ceil #Convert to tbs
                if @spa_box==true #Uses spa box
                  @quarteroz_Cl=@ozCalc_CPO_Cl/0.25 #Total number of 0.25oz chlorine bags
                  @bags_Cl=@quarteroz_Cl.round #Number of whole 0.25oz chlorine packets
                  @messages << "\nAdd "+@bags_Cl.to_s+", 0.25 oz package(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                  @values.store(@Cl_nametemp, @bags_Cl.to_s+";pac")
                elsif @spa_box==false #Does not use spa box
                  @messages << "\nAdd "+@tbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                  @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                end
              elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @primary_sanitizer==1 #Check for greater than zero
                @messages << "\nAdd 2 bromine tablets in tab dispensers.\n\n"
                @values.store("bromine", "2")
              end
            elsif @swg==1 #Salt water chlorine generator and low chlorine
              @messages << "Chlorine is low.\nSalt water chlorinator, circulation, and salt concentration need to be checked.\n\n"
            end
          elsif @delta_FC<=0 #Check if chlorine is at or above target
            @messages << "\nChlorine is within range, re-fill tablets if needed.\n\n"
          end
        elsif @primary_sanitizer==1 #Bromine system
          @minBr=4 #Minimum ideal Br ppm
          @maxBr=6 #Maximum ideal Br ppm
          @Br_tar=(@maxBr+@minBr)/2 #Target Br
          @delta_Br=@Br_tar-@br_i #Difference of bromine target and initial concentration
          if BigDecimal(@delta_Br.to_s) > 0
            @messages << "Bromine low, refill Bromine tablets.\n\n"
          else
            @messages << "Bromine in range, check tablets.\n\n"
          end
        end
        if (@daysdiff.blank? == false && @daysdiff > 3) || (@ta_i.blank? == false && @ta_i.to_d <=0)
          @adj_Calc_CPO_NaHCO3=0
        end

        #Adjusted TA Dose Report #spaboxcondition
        if (@adj_Calc_CPO_NaHCO3>=0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3 && @spa_box == false) #Check for pounds and maximum per 10,000 gallons
          @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
          @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
          @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
          @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
        elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
          @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
          @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
          if @spa_box==true #Uses spa box
            @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
            @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
            @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\nThis will also increase the pH.\n\n"
            @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
          elsif @spa_box==false #Does not use spa box
            @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjusments. \nThis will also increase the pH.\n\n"
            @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
          end
        else
          if @adj_Calc_CPO_NaHCO3>=0.25 && @spa_box == false
            @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
            @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
            @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
          elsif (@adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
            @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
            @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
            if @spa_box==true #Uses spa box
              @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
              @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
              @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
            end
          end
        end

        #pH Dose Report
        if (@Calc_CPO_Na2CO3>=0.25 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for pounds and maximum per 10,000 gallons
          @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
          @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
          @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate to increase pH \n"
          @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
        elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for oz. and maximum per 10,000 gallons
          @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
          @tbs_Na2CO3=(@Calc_CPO_Na2CO3*2).round #Converts oz to tbs
          if @spa_box==true #Uses spa box
            @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
            @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
            @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate to increase pH \n"
            @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
          elsif @spa_box==false #Does not use spa box
            @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate to increase pH \n"
            @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
          end
        else #greater than maximum dose per 10,000 gallons
          if @Calc_CPO_Na2CO3>=0.25
            @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
            @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
            @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
          elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0)
            @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
            @tbs_Na2CO3=(@Calc_CPO_CPO_Na2CO3*2).round #Converts oz to tbs
            if @spa_box==true #Uses spa box
              @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
              @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
              @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
              @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
              @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
            end
          end
        end
        #puts @Calc_CPO_NaHSO4
        if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
          @wholecup_NaHSO4=(@Calc_CPO_NaHSO4/0.574).floor #Assumes 1 cup = 0.574 lb
          @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
          @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite to lower pH \n"
          @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
        elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000) #Check for ounces and max dose per 10,000 gallons
          @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16)/0.574 #Convert to ounces
          @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
          if @spa_box==true #Uses spa box
            @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
            @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
            @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite to lower pH \n"
            @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
          elsif @spa_box==false #Does not use spa box
            @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite to lower pH \n"
            @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
          end
        else #greater than maximum dose per 10,000 gallons
          if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
            @wholecup_NaHSO4=(@Calc_CPO_NaHSO4/0.574).floor #assumes 1 cup = 0.574 lb
            @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
            @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
          elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
            @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16)/0.574 #Convert to ounces
            @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
            if @spa_box==true #Uses spa box
              @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
              @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
              @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
              @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
              @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
            end
          end
        end

        puts @messages
      end #if spa check
      puts "==================================Spa Dosage ends================================="
      return generatedosagetextforbeta(@values)
    else
      # AUTO_PRESCRIPTION is not enabled
      return @values
    end
  end


  #Added by Gowtham on 26 Jan 2016 to calculate dosage details for pool
  def pooldosagecalcapi(p_id,ph,orp,ta)
    puts "==================================Pool Dosage====================================="
    @values = {}
    @pool = Pool.find(p_id)
    @customer = Customer.find(@pool.customer_id)
    @role = User.find_by_email(@customer.email)
    @features = Capability.where(role_id: @role.role_id).to_a
    @auto_pres = false
    @features.each do |i|
      @feature_name = Feature.where(id:i.feature_id).limit(1).pluck(:feature_name).to_a
      @feature_name = @feature_name[0]
      if @feature_name == "AUTO_PRESCRIPTION"
        @auto_pres = true
      end
    end
    if @auto_pres
      puts "Feature ENABLED"
      if @pool.isPool
        #Inputs
        water_type="pool"

        #Get pool volume
        if @pool.volume.blank? == false
          @pool_vol_gal= @pool.volume #Gallons of pool water
        else
          @pool_vol_gal= 0
        end

        @orp = 0
        @ph = 0

        @ta_i_val = 0
        @cya_i_val = 0
        @free_cl = 0

        if ph.blank? == false
          @ph = ph
          if @ph.to_d < 7
            @ph = 7
          end
          if @ph.to_d > 8
            @ph = 8
          end
        end

        if orp.blank? == false
          @free_cl = orp
        end

        if ta.blank? == false
          @ta_i_val = ta
        end


        @sanitizer = @pool.waterChemistry

        @daysdiff = 0



        @pH_i= @ph #Initial pH from 0 to 14

        #Decide primary sanitizer
        @Primary_sanitizer= 0 #Check for bromine or chlorine spa

        if @sanitizer == 'Chlorine'
          #if @daysdiff.blank? == false && @daysdiff >= 3
          @Primary_sanitizer = 1
        else
          @Primary_sanitizer = 2
        end

        @ORP_i = @orp

        if @Primary_sanitizer==1 && (@free_cl <= 0)
          #@FC_i= @free_cl #Initial Free Chlorine (ppm)
          # @ORP_i=@orp #Initial ORP level (mV)
          @FC_i = (40000-CMath.sqrt(6840000000-9600000*(@ORP_i)))/5000.0

          #if @FC_i.to_d < 0
          #	@FC_i = 0
          #end
        else
          @FC_i = @free_cl
          @ORP_i=@orp #Initial ORP level (mV)
        end
        if @Primary_sanitizer==2 && @daysdiff < 3
          @FC_i = @free_cl
        end

        @acid_type= 0 #Acid type
        if @pool.dryacid
          @acid_type = 1
        elsif @pool.muriatic31
          @acid_type = 2
        end

        @TA_i= @ta_i_val #input('What is the Total Alkalinity (ppm)?: '); #Initial Total Alkalinity (ppm)
        @CYA_i=@cya_i_val#input('What is the Cyanuric Acid level (ppm)?: '); #Initial Cyanuric acid level (ppm)
        if @pool.waterChemistry == "Salt Water"
          @SWG = 1 #salt water chlorinator
          #puts "SWG = 1"
        else
          @SWG = 2 #salt water chlorinator
          #puts "SWG = 1"
        end

        if @pool.ozonator
          @ozone = 1 #ozonator
        else
          @ozone = 2 #ozonator
        end

        #Maximum recommended chemical amounts to be added per 10,000 gallons of pool water at any one time:
        #Sodium bicarbonate: 10 lbs.
        #Sodium carbonate (soda ash): 4 lbs.
        #Muriatic acid: 16 fl. oz.
        #Sodium bisulfate: 20 oz.

        # Alkalinity Adjustment
        @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
        @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
        @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
        @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
        @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
        @Calc_CPO_NaHCO3=0

        if @SWG==1
          @TA_tar=(@base_maxTA+@base_minTA)/2 #Calculates target for the total alkalinity average of ideal range
          if @TA_i<=@base_minTA
            @delta_TA=@TA_tar-@TA_i
            @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA
          else
            @delta_TA = 0
            #return @values
          end
        elsif @SWG==2
          @TA_tar=(@acid_maxTA+@acid_minTA)/2 #Calculates target for the total alkalinity average of ideal range
          if @TA_i<=@acid_minTA
            @delta_TA=@TA_tar-@TA_i
            @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
          elsif @TA_i>=@acid_maxTA+20
            @delta_TA = 0
            puts 'Watch TA, should lower over time using tablets.'
            #return @values
          else
            @delta_TA = 0
            #return @values
          end
        else
          @delta_TA=@TA_tar-@TA_i
        end

        #pH Adjustment
        @Calc_CPO_Na2CO3=0
        @Calc_CPO_NaHSO4=0
        @Calc_CPO_HCl=0
        @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
        @max_NaHSO4=20/16 #Maximum recommended sodium bisulfate single addition per 10,000 gal
        @max_HCl=16/128 #Maximum recommended muriatic acid single addition per 10,000 gal

        if @pH_i<7.0
          @pH_i=7.0
          #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n')
        elsif @pH_i>8.0
          @pH_i=8.0
          #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n')
        end

        #Base
        if (@pH_i<7.4 && @pH_i>=7.2)#7.2<=@pH_i<7.4
          @CPO_Na2CO3=0.375
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)
        elsif (@pH_i<7.2 && @pH_i>=7.0)#7.0<=@pH_i<7.2
          @CPO_Na2CO3=0.5
          @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)

          #Acid
        elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==1)#7.6<=@pH_i<=7.8 and sodium bisulfite
          @CPO_NaHSO4=0.94
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)
        elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==1)#7.8<@pH_i<=8.0 and sodium bisulfite
          @CPO_NaHSO4=1.25
          @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)#7.6<=@pH_i<=7.8 and muriatic acid
        elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==2)
          @CPO_HCl=0.094
          @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
        elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==2)#7.8<@pH_i<=8.0 and muriatic acid
          @CPO_HCl=0.125
          @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
        end

        #Adjusting TA adjustment from pH adjustment
        if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
          @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
          @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@pool_vol_gal/10000.0)) #TA increase from pH increase
          #Rails.logger.info("LOGS TRUE PRESET"+@delta_TA.to_s)
          #Rails.logger.info("LOGS TRUE PRESET"+@TAppm_Na2CO3.to_s)
          @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
        else
          @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
        end

        #Sanitizer

        if @sanitizer == 'Chlorine' #Check for chlorine
          #puts "Primary Sanitizer = 1"
          @minFC=2 # Minimum ideal FC ppm
          @maxFC=4#Maximum ideal FC ppm
          @FC_tar=(@maxFC+@minFC)/2#Target FC
          @delta_FC=@FC_tar-@FC_i
          #puts "DELTA = "+@delta_FC.to_s
          if @delta_FC>0 #Check if sanitizer above target
            #puts "INSIDE DELTA CHECK"
            if @SWG==2 #No chlorine generator => manual chlorine
              if @pool.dichlor
                @Cl_type= 1
              elsif @pool.dichlor62
                @Cl_type= 2
              elsif @pool.trichlor3
                @Cl_type= 3
              elsif @pool.bleach6
                @Cl_type= 5
              elsif @pool.bleach12
                @Cl_type= 4
              elsif @pool.calhypo53
                @Cl_type= 0
              else
                @Cl_type = 0
              end
              #puts "CHEM TYPE = "+@Cl_type.to_s
              #mess @Cl_type= input ('\nSelect Chlorine type by number: \n1:Dichlor 56# ACC\n2:Dichlor 62# ACC\n3:Trichlor\n4:12# Liquid Chlorine\n5:6# Liquid Chlorine (Household Bleach)\nSelection: ');
              if @Cl_type==1
                @Cl_name='Dichlor 56#'
                @Cl_nametemp=('dichlor_56')
                @CPO_Cl=0.15 #Dichlor 56# ACC CPO factor
              elsif @Cl_type==2
                @Cl_name='Dichlor 62#'
                @Cl_nametemp=('dichlor_62')
                @CPO_Cl=0.131 #Dichlor 62# ACC CPO factor
              elsif @Cl_type==3
                @Cl_name='3" Trichlor Tablet(s)'
                @Cl_nametemp=('trichlor')
                @CPO_Cl=0.09375 #Trichlor CPO factor
              elsif @Cl_type==4
                @Cl_name='12# Liquid Chlorine'
                @Cl_nametemp=('lch12')
                @CPO_Cl=0.0836 #12# Liquid Chlorine
              elsif @Cl_type==5
                @Cl_name='6# Liquid Chlorine (Household Bleach)'
                @Cl_nametemp=('lch6')
                @CPO_Cl=0.0418 #Approximation 6# Liquid Chlorine
              elsif @Cl_type==0
                @Cl_name='Calcium Hypochlorite'
                @Cl_nametemp=('chyp53')
                @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
              else
                @CPO_Cl=0
              end


              #puts "Chem Name = "+@Cl_name
              #puts "CPO_Cl = "+@CPO_Cl.to_s
              @Calc_CPO_Cl=@CPO_Cl*(@pool_vol_gal/10000.0)*@delta_FC
              if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                #puts "@Cl_type<=5 && @Cl_type>=4"
                if @Calc_CPO_Cl>=0.25 #Check for gallons
                  #puts "@Calc_CPO_Cl>=0.25"
                  @wholegal_Cl=(@Calc_CPO_Cl).floor  #Number of whole gallons of chlorine
                  @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                  if @quartergal_Cl==4
                    @wholegal_Cl=1
                    @quartergal_Cl=@quartergal_Cl-4
                  end
                  #mess fprintf('\nAdd #g gallon(s) and #g quarter gallon(s) of #s to the #s evenly.\n\n', @wholegal_Cl, @quartergal_Cl, @Cl_name, water_type)
                  #puts @wholegal_Cl.to_s
                  #puts @quartergal_Cl.to_s
                  @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                  #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625"
                  @wholecup_Cl=((@Calc_CPO_Cl)/16).floor #Number of whole cups of chlorine
                  @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.0625).round #Number of quarter cups of chlorine
                  #mess fprintf('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s evenly.\n\n', @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                  #puts @wholecup_Cl.to_s
                  #puts @quartercup_Cl.to_s
                  @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                elsif @Calc_CPO_Cl<0.0625
                  #puts "@Calc_CPO_Cl<0.0625"
                  @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                  #mess fprintf('\nAdd #g tablespoon(s) of #s to the #s evenly.\n\n', @wholetbs_Cl, @Cl_name, water_type)
                  #puts @wholetbs_Cl.to_s
                  @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                end
              elsif @Calc_CPO_Cl>=0.25 && @Cl_type<3 #Check for less than 1 pound granular
                #puts "@Calc_CPO_Cl>=0.25 && @Cl_type<3"
                @wholecup_Cl=(@Calc_CPO_Cl).floor #Assumes 1 cup = 1 lb
                @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                #mess fprintf ('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s by spreading granules evenly.\n\n' , @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                #puts @wholecup_Cl.to_s
                #puts @quartercup_Cl.to_s
                @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
              elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3 #Check for greater than zero
                #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3"
                @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert to ounces
                @tbs_Cl=(@ozCalc_CPO_Cl*2).round #Convert to tbs
                #mess fprintf ('\nAdd #g tablespoon(s) of #s to the #s by spreading granules evenly.\n\n' , @tbs_Cl, @Cl_name, water_type)
                #puts @tbs_Cl.to_s
                @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
              end
            elsif @SWG==1 #Salt water chlorine generator and low chlorine
              #puts "@SWG==1"
              #mess fprintf ('Chlorine is low.\nSalt water chlorinator needs to be checked. Supplemental chlorine can be used.\n\n')
            elsif @Cl_type==3
              #puts "@Cl_type==3"
              @wgt_trichlortab= 0.7 #Assuming trichlor 3" tablet is 0.7 oz
              @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert lbs to ounces
              @wholetab_Cl=ceil(@ozCalc_CPO_Cl/@wgt_trichlortab)
              #mess fprintf('Add #g #s to the floater(s) in the #s incrementally if necessary.\n\n', @wholetab_Cl, @Cl_name, water_type)
              #puts @wholetab_Cl.to_s
              @values.store(@Cl_nametemp, @wholetab_Cl.to_s+";tbs")
            end
          elsif @delta_FC<=0 #Check if chlorine is at or above target
            #mess fprintf ('\nChlorine is within range.\n\n')
          end
        end

        if (@daysdiff.blank? == false && @daysdiff > 3) || (@TA_i.blank? ==false && @TA_i.to_d <=0)
          @adj_Calc_CPO_NaHCO3=0
        end
        #puts @adj_Calc_CPO_NaHCO3
        #Adjusted TA Dose Report
        if (@adj_Calc_CPO_NaHCO3 >= 0.25 && @adj_Calc_CPO_NaHCO3<=(@pool_vol_gal/10000.0)*@max_NaHCO3) #Check for pounds and maximum per 10,000 gallons
          @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
          @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
          #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjustment.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
          @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
        elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<=0.25 && @adj_Calc_CPO_NaHCO3<(@pool_vol_gal/10000)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
          @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
          @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
          #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjusments.\n\n', @tbs_NaHCO3, @TA_tar)
          @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
        else
          if @adj_Calc_CPO_NaHCO3>=0.25
            @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
            @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
            @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
          elsif (@adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
            @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
            @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
            #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @tbs_NaHCO3, @TA_tar)
            @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
          end
        end

        #pH Dose Report
        if (@Calc_CPO_Na2CO3>=0.25 && @Calc_CPO_Na2CO3<=@max_Na2CO3*@pool_vol_gal/10000.0) #Check for pounds and maximum per 10,000 gallons
          @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
          @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
          #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium carbonate to increase pH \n', @wholecup_Na2CO3, @quartercup_Na2CO3)
          @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
        elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0 && @Calc_CPO_Na2CO3<=@max_Na2CO3*@pool_vol_gal/10000.0) #Check for oz. and maximum per 10,000 gallons
          @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
          @tbs_Na2CO3=(@Calc_CPO_Na2CO3*2).round #Converts oz to tbs
          #mess fprintf('Add #g tablespoon(s) of Sodium carbonate to increase pH \n', @tbs_Na2CO3)
          @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
        else #greater than maximum dose per 10,000 gallons
          if @Calc_CPO_Na2CO3>=0.25
            @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
            @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium carbonate INCREMENTALLY to increase pH \n', @wholecup_Na2CO3, @quartercup_Na2CO3)
            @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
          elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0)
            @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
            @tbs_Na2CO3=(Calc_CPO_CPO_Na2CO3*2).round #Converts oz to tbs
            #mess fprintf('Add #g tablespoon(s) of Sodium carbonate INCREMENTALLY to increase pH \n', @tbs_Na2CO3)
            @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
          end
        end

        if @acid_type==1
          if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
            @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #Assumes 1 cup = 1 lb
            @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
            @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
          elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000) #Check for ounces and max dose per 10,000 gallons
            @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
            @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
            #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite to lower pH \n', @tbs_NaHSO4)
            @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
          else #greater than maximum dose per 10,000 gallons
            if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
              @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #assumes 1 cup = 1 lb
              @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
              @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
            elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
              @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
              @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @tbs_NaHSO4)
              @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
            end
          end
        elsif @acid_type==2
          if (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
            @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
            @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
            @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
            #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
            @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
          elsif (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
            @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
            @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
            @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
            #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid INCREMENTALLY in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
            @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
          elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for cups and maximum dose per 10,000 gallons
            @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
            @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
            #mess fprintf('Spread #g cup(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
            @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
          elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0)
            @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
            @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
            #mess fprintf('Spread #g cup(s) INCREMENTALLY of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
            @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
          end
        end
        #puts @values
        return generatedosagetextforbeta(@values)
      end
      puts "==================================Pool Dosage ends================================="
    else
      # AUTO_PRESCRIPTION is not enabled
      return @values
    end
  end


  def generatedosagetextforbeta(autoval)
    @autoval = autoval
    puts @autoval
    @dsg = Dosage.new
    @count = 0
    @text = ""

    @unit = "cup(s)"
    @tablets = "Tablet(s)"
    @gallons = "Gallon(s)"
    @lbs = "lbs"
    if @pool.isPool
      @unit = "cup"
    else
      @unit = "packet(s)"
    end

    if @autoval["bromine"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["bromine"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @tempf = @autoval["bromine"].to_d
      end
      @dsg.bromine1Size  = @tempf.to_d+(@tempq.to_d*0.25)

      if @tempf > 0 && @tempq > 0
        @tablets = getunits(@tempq,@tablets)
        @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@tablets} of Bromine, "
      elsif @tempf > 0 && @tempq <= 0
        @tablets = getunits(@tempf,@tablets)
        @text += " "+@tempf.to_s+" #{@tablets} of Bromine, "
      elsif @tempf <= 0 && @tempq > 0
        @tablets = getunits(@tempq,@tablets)
        @text += " "+@tempq.to_s+" #{@tablets} of Bromine, "
      end
    end

    if @autoval["dichlor_56"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["dichlor_56"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["dichlor_56"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["dichlor_56"].to_d/16
        else
          @tempf = @autoval["dichlor_56"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.dichlor = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Dichlor 56%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
        end
      elsif @pool.isPool == false && @pool.spabox
        puts @unit
        puts "##"
        @dsg.esanitizer = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          puts @unit
          puts "##"
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Dichlor 56%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
        end
      end
    end

    if @autoval["sodium_carbonate"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["sodium_carbonate"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d rescue 0
      else
        @temp_arr = @autoval["sodium_carbonate"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["sodium_carbonate"].to_d/16
        else
          @tempf = @autoval["sodium_carbonate"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.phUp = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Soda Ash, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf.to_i,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Soda Ash, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Soda Ash, "
        end
      elsif @pool.isPool == false && @pool.spabox
        @dsg.bsodaash = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Soda Ash, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf.to_i,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Soda Ash, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Soda Ash, "
        end
      end
    end

    if @autoval["sodium_bisulfite"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["sodium_bisulfite"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["sodium_bisulfite"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["sodium_bisulfite"].to_d/16
        else
          @tempf = @autoval["sodium_bisulfite"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.phDown  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dry Acid, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Dry Acid, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Dry Acid, "
        end
      elsif @pool.isPool == false && @pool.spabox
        @dsg.ddryacid  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dry Acid, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Dry Acid, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Dry Acid, "
        end
      end
    end

    if @autoval["sodium_bicarbonate"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["sodium_bicarbonate"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["sodium_bicarbonate"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["sodium_bicarbonate"].to_d/16
        else
          @tempf = @autoval["sodium_bicarbonate"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.taUp = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf.to_i,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of sodium bicarbonate, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
        end
      elsif @pool.isPool == false && @pool.spabox
        @dsg.bbakingsoda  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf.to_i,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of sodium bicarbonate, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq.to_i,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
        end
      end
    end

    if @autoval["trichlor"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["trichlor"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @tempf = @autoval["trichlor"].to_d
      end
      @dsg.trichlor  = @tempf.to_d+(@tempq.to_d*0.25)
      if @tempf > 0 && @tempq > 0
        @tablets = getunits(@tempq,@tablets)
        @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@tablets} of trichlor, "
      elsif @tempf > 0 && @tempq <= 0
        @tablets = getunits(@tempf,@tablets)
        @text += " "+@tempf.to_s+" #{@tablets} of trichlor, "
      elsif @tempf <= 0 && @tempq > 0
        @tablets = getunits(@tempq,@tablets)
        @text += " "+@tempq.to_s+" #{@tablets} of trichlor, "
      end
    end

    if @autoval["lch6"]
      @tempf = 0
      @tempq = 0
      @count += 1
      #@tempauto = @autoval["lch6"].split('~')
      @t1 = @autoval["lch6"].split(";")
      @dunit = @t1[1]
      @tempauto = @t1[0].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["lch6"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @tempauto[0].to_d/16
        else
          @tempf = @tempauto[0].to_d
        end
      end

      if @dunit == "cup"
        @dunit = "cup"
      elsif @dunit == "gal"
        @dunit = "gallon"
      end

      @dsg.lch6unit = @dunit
      @dsg.lch6qunit = @dunit
      @dsg.liquidCl6  = @tempf.to_d+(@tempq.to_d*0.25)
      if @tempf > 0 && @tempq > 0
        @unit = getunits(@tempq,@unit)
        @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Liquid Chlorine 6%, "
      elsif @tempf > 0 && @tempq <= 0
        @unit = getunits(@tempf,@unit)
        @text += " "+@tempf.to_s+" #{@unit} of Liquid Chlorine 6%, "
      elsif @tempf <= 0 && @tempq > 0
        @unit = getunits(@tempq,@unit)
        @text += " "+@tempq.to_s+" #{@unit} of Liquid Chlorine 6%, "
      end
    end

    if @autoval["lch12"]
      @tempf = 0
      @tempq = 0
      @count += 1
      #@tempauto = @autoval["lch12"].split('~')
      @t1 = @autoval["lch12"].split(";")
      @dunit = @t1[1]
      @tempauto = @t1[0].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["lch12"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @tempauto[0].to_d/16
        else
          @tempf = @tempauto[0].to_d
        end
      end

      if @dunit == "cup"
        @dunit = "cup"
      elsif @dunit == "gal"
        @dunit = "gallon"
      end

      @dsg.lch12unit = @dunit
      @dsg.lch12qunit = @dunit
      @dsg.liquidChlorine  = @tempf.to_d+(@tempq.to_d*0.25)
      if @tempf > 0 && @tempq > 0
        @unit = getunits(@tempq,@unit)
        @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Liquid Chlorine 12%, "
      elsif @tempf > 0 && @tempq <= 0
        @unit = getunits(@tempf,@unit)
        @text += " "+@tempf.to_s+" #{@unit} of Liquid Chlorine 12%, "
      elsif @tempf <= 0 && @tempq > 0
        @unit = getunits(@tempq,@unit)
        @text += " "+@tempq.to_s+" #{@unit} of Liquid Chlorine 12%, "
      end
    end


    if @autoval["dichlor_62"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["dichlor_62"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["dichlor_62"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["dichlor_62"].to_d/16
        else
          @temp_arr = @autoval["dichlor_62"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["dichlor_62"].to_d/16
          else
            @tempf = @autoval["dichlor_62"].to_d
          end
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.dichlor62Size  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Dichlor 62%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
        end
      else
        @dsg.dichlor62Size  = @tempf.to_d/16+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Dichlor 62%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
        end
      end
    end

    if @autoval["chyp53"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["chyp53"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["chyp53"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["chyp53"].to_d/16
        else
          @tempf = @autoval["chyp53"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.calhypo53  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@lbs)
          @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 53%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
        end
      elsif @pool.isPool == false && @pool.spabox
        @dsg.calhypo53  = @tempf.to_d/16+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@lbs)
          @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 53%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
        end
      end
    end

    if @autoval["chyp65"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["chyp65"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["chyp65"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["chyp65"].to_d/16
        else
          @tempf = @autoval["chyp65"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.calhypo65  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@lbs)
          @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 65%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
        end
      elsif @pool.isPool == false && @pool.spabox
        @dsg.calhypo65  = @tempf.to_d/16+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@lbs)
          @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 65%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@lbs)
          @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
        end
      end
    end

    if @autoval["chyp73"]
      @tempf = 0
      @tempq = 0
      @count += 1
      @tempauto = @autoval["chyp73"].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["chyp73"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @autoval["chyp73"].to_d/16
        else
          @tempf = @autoval["chyp73"].to_d
        end
      end
      if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
        @dsg.calhypo73  = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 73%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
        end
      elsif @pool.isPool == false && @pool.spabox
        @dsg.calhypo73  = @tempf.to_d/16+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 73%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
        end
      end
    end

    if @autoval["muriatic_acid"]
      @tempf = 0
      @tempq = 0
      @count += 1
      #@tempauto = @autoval["muriatic_acid"].split('~')
      @t1 = @autoval["muriatic_acid"].split(";")
      @dunit = @t1[1]
      @tempauto = @t1[0].split('~')
      if @tempauto.length > 1
        @tempf = @tempauto[0].to_d
        @tempq = @tempauto[1].to_d
      else
        @temp_arr = @autoval["muriatic_acid"].split(";")
        @tablespoon = false
        if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
          @tablespoon = true
        end
        if @tablespoon
          @tempf = @tempauto[0].to_d/16
        else
          @tempf = @tempauto[0].to_d
        end
      end

      if @dunit == "cup"
        @dunit = "cup"
      elsif @dunit == "gal"
        @dunit = "gallon"
      end

      @dsg.macid31unit = @dunit
      @dsg.macid31qunit = @dunit
      @dsg.muriatic31  = @tempf.to_d+(@tempq.to_d*0.25)
      if @tempf > 0 && @tempq > 0
        @gallons = getunits(@tempq,@gallons)
        @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@gallons} of muriatic acid, "
      elsif @tempf > 0 && @tempq <= 0
        @gallons = getunits(@tempf,@gallons)
        @text += " "+@tempf.to_s+" #{@gallons} of muriatic acid, "
      elsif @tempf <= 0 && @tempq > 0
        @gallons = getunits(@tempq,@gallons)
        @text += " "+@tempq.to_s+" #{@gallons} of muriatic acid, "
      end
    end

    @mtest = @pool.mtests.last
    if @mtest.blank? == false && @mtest.freeCl.blank? == false && @mtest.freeCl.to_d < 3 && @pool.waterChemistry == "Salt Water"
      @count += 1
    end

    if @count > 0
      deletealldosages(@pool)
      @phdosage = nil
      @orpdosage = nil

      @epool = nil # Epool.find_by_pool_id(@pool.id)
      if @epool
        @point = Point.find_by_sql("select * from points where epool_id = "+@epool.id.to_s+" and extAddr = '"+@epool.extAddr+"' and date(created_at) <= DATE(NOW()) order by created_at desc limit 1")
        @point.each do |point|
          if @epool.device_type == "L"
            @phdosage = phlorasingle(@epool,point)
            @orpdosage = orplorasingle(@epool,point)
          else
            @phdosage = phsingle(@epool,point)
            @orpdosage = orpsingle(@epool,point)
          end
        end
      end

      if @mtest.blank? == false && @mtest.freeCl.blank? == false && @mtest.freeCl.to_d < 3 && @pool.waterChemistry == "Salt Water"
        @dsg.saltwater = true
      end
      #dsaf
      @dsg.ph = @phdosage
      @dsg.orp = @orpdosage
      @dsg.pool_id = @pool.id
      @dsg.customer_id = @pool.customer_id
      @dsg.status = false
      @dsg.approved = true
      @dsg.save

      senddosagependingsms(@dsg,@pool)

      #process chemical low check
      #notifylowstock("create",@pool,@dsg)
      #chemical low check ends

      #@dos = Dosage.last
      #customerdone(@dos.id)
    end

    #notify administrator if the pool in bad state and no prescription generated
    notifyadminofthestate(@pool)

    return generatedosagetext(@dsg,@pool)
  end

  def senddosagependingsms(dosage,pool)
    return
    @dosagehere = dosage
    @poolhere = pool
    @customer = Customer.find(@poolhere.customer_id)
    @role_id = User.where(email: @customer.email).limit(1).pluck(:role_id)
    @user_role = Role.where(id: @role_id).limit(1).pluck(:role_name)
    @message = nil
    if @poolhere.isPool
      @message = Message.find_by_short("POOL_DOSAGE")
    else
      @message = Message.find_by_short("SPA_DOSAGE")
    end
    if @user_role[0] == "BetaCustomer"
      customerdonedosage(@dosagehere.id)
      Rails.logger.info("APPROVE BETA DOSAGE SAVED")
      if @dosagehere.prescription.blank? == false
        @smscontent = @dosagehere.prescription
      else
        @smscontent = generatedosagetext(@dosagehere,@poolhere)

        if @message.content.blank?
          if @poolhere.isPool
            @smscontent = "Please add "+@smscontent+" around pool"
          else
            @smscontent = "Please put in "+@smscontent+" around spa"
          end
        else
          if @smscontent.blank? && @dosagehere.saltwater
            @smscontent = "Please [saltwater]"
          else
            @smscontent = @message.content.gsub! "[prescription]", @smscontent
          end
        end
      end
      if @message != nil &&@message.status
        #add chemical low text to the prescription content
        if @user_role[0] == "BetaCustomer"
          @smscontent = @smscontent+" "+notifylowstock("done",@poolhere,@dosagehere)
        else
          @smscontent = @smscontent+" "+notifylowstock("create",@poolhere,@dosagehere)
        end
        sendpressms(@smscontent,@poolhere)
        Rails.logger.info("SMS SENT")
      else
        if Rails.application.config.siteurl == "http://localhost:3000" || Rails.application.config.siteurl == "http://sutrodev-env.elasticbeanstalk.com"
          SesMailer.customeremail("andrew@mysutro.com,support@mysutro.com,gowtham@tentsoftware.com","DEV - Dosage Text SMS content for #{@customer.email}",@smscontent).deliver
        else
          SesMailer.customeremail("andrew@mysutro.com,support@mysutro.com,gowtham@tentsoftware.com","PROD - Dosage Text SMS content for #{@customer.email}",@smscontent).deliver
        end
      end
    else
      sendsms('DOSING_NEEDED',@poolhere)
    end
  end

  def deletealldosages(pool)
    Rails.logger.info("DELETING ALL PRESCRIPTIONS")
    @d = Dosage.where(pool_id: pool.id,customer_id: pool.customer_id, status: 0).delete_all
    Rails.logger.info("DELETING ALL PRESCRIPTIONS DONE")
  end

  # TODO REMOVE
  def apiprescget(data)
    return

    @tempparams = data
    puts @tempparams
    @output = ""
    if @tempparams["phone"].blank? == false
      @ph = @tempparams["ph"].to_d
      @chlorine = @tempparams["chlorine"].to_d
      if @tempparams["ta"].blank? || @tempparams["ta"].downcase == "skip"
        @ta = nil
      else
        @ta = @tempparams["ta"].to_d
      end
      Rails.logger.info("PHONE PARAM : "+@tempparams["phone"].to_s)
      @phone = getonlynumbers(params["phone"])
      Rails.logger.info("PHONE CONVR : "+@phone.to_s)
      @customer = Customer.find_by_sql("select * from customers where replace(replace(replace(replace(replace(replace(replace(mPhone,' ',''),'-',''),'(',''),')',''),'.',''),' ',''),'+1','') = '"+@phone+"'")
      if @customer.blank? == false
        @pool = Pool.find_by_customer_id(@customer[0]["id"])
        if @pool.blank? == false
          Rails.logger.info("PH : "+@ph.to_s)
          Rails.logger.info("ORP : "+@chlorine.to_s)
          Rails.logger.info("TA : "+@ta.to_s)
          @mtest = Mtest.new
          @mtest.pool_id = @pool.id
          @mtest.pH = @ph
          @mtest.freeCl = @chlorine
          @mtest.totalAlkalinity = @ta

          if @mtest.save
            if @pool.isPool
              if (@ph.to_d < 7.2 || @ph.to_d > 7.6) || @chlorine.to_d <= 2
                @output = pooldosagecalc(@pool.id)
                generatedosagetextforbeta(@output)
              else
                @output = "poolgood"
              end
            else
              if (@ph.to_d < 7.2 || @ph.to_d > 7.6) || @chlorine.to_d <= 3
                @output = spadosagecalc(@pool.id)
                generatedosagetextforbeta(@output)
              else
                @output = "spagood"
              end
            end
            sendbetamtestemail("BETA_MANUAL_TEST",@pool)
          end
        end
      end #if customer blank
    end #if email blank
    puts @output
    #respond_to do |format|
    render :json => @output
    #end
  end

  #for other controllers to access dosage module using pool id
  def populatepresc(data)
    @tempparams = data
    puts @tempparams
    @output = ""
    if @tempparams["pool_id"].blank? == false
      @ph = 0
      @orp = 0
      @ta = 0
      if @tempparams["ph"].blank? == false
        @ph = @tempparams["ph"].to_d
      end

      if @tempparams["orp"].blank? == false
        @orp = @tempparams["orp"].to_d
      end

      if @tempparams["ta"].blank? == false
        @ta = @tempparams["ta"].to_d
      end

      @pool = Pool.find(@tempparams["pool_id"])
      if @pool.blank? == false
        if @pool.isPool
          @output = pooldosagecalc(@pool.id)
        else
          @output = spadosagecalc(@pool.id)
        end
        generatedosagetextforbeta(@output)
      end
    end #if pool id blank
    puts "POPULATED AUTO PRESC : "+@output.inspect
    #respond_to do |format|
    #render :json => @output
    return @output
    #end
  end


  def getloradayformat(date)
    #@day = date.strftime("%A")
    #@today = Date.todaystrftime("%A")
    @ret = ""
    @diff = ((Time.zone.now - date) / 1.day).to_i
    if @diff.blank? == false
      if @diff == 0
        @ret = "Today"
      elsif @diff == 1
        @ret = "Yesterday"
      elsif @diff == 2
        @ret = "2 days ago"
      elsif @diff == 3
        @ret = "3 days ago"
      elsif @diff == 4
        @ret = "4 days ago"
      elsif @diff == 5
        @ret = "5 days ago"
      elsif @diff > 5 && @diff <= 29
        @ret = "a month ago"
      elsif @diff > 29 && @diff < 365
        @ret = (@diff/31).to_s+" months ago"
      elsif @diff > 365
        @ret = (@diff/365).to_s+" years ago"
      end
    end
    return @ret
  end

  def getbetadayformat(date)
    #@day = date.strftime("%A")
    #@today = Date.todaystrftime("%A")
    @ret = ""
    @diff = ((Time.zone.now - date) / 1.day).to_i
    if @diff.blank? == false
      if @diff == 0
        @ret = "Today"
      elsif @diff == 1
        @ret = "Yesterday"
      elsif @diff == 2
        @ret = "2 Days ago"
      elsif @diff == 3
        @ret = "3 Days ago"
      elsif @diff == 4
        @ret = "4 Days ago"
      elsif @diff == 5
        @ret = "5 Days ago"
      elsif @diff > 5 && @diff <= 31
        @ret = "A Month ago"
      elsif @diff > 31 && @diff < 365
        if @diff/31 > 1
          @ret = (@diff/31).to_s+" Months ago"
        else
          @ret = "A Month ago"
        end
      elsif @diff > 365
        @ret = (@diff/365).to_s+" Years ago"
      end
    end
    return @ret
  end

  def getchemlength(pool)
    @pool = pool
    @count = 0
    if @pool.trichlor2 && @pool.trichlor2Size.nil? == false && @pool.trichlor2Size.to_d > 0
      @count += 1
    end

    if @pool.bromine1 && @pool.bromine1Size.nil? == false && @pool.bromine1Size.to_d > 0
      @count += 1
    end

    if @pool.lithiumhypo && @pool.lithiumhypoSize.nil? == false && @pool.lithiumhypoSize.to_d > 0
      @count += 1
    end

    if @pool.calcium && @pool.calciumSize.nil? == false && @pool.calciumSize.to_d > 0
      @count += 1
    end

    if @pool.cyanuricsolid && @pool.cyanuricsolidSize.nil? == false && @pool.cyanuricsolidSize.to_d > 0
      @count += 1
    end

    if @pool.cyanuricliquid && @pool.cyanuricliquidSize.nil? == false && @pool.cyanuricliquidSize.to_d > 0
      @count += 1
    end

    if @pool.spabox && @pool.esanitizer.nil? == false && @pool.esanitizer.to_d > 0
      @count += 1
    end

    if @pool.spabox && @pool.ddryacid.nil? == false && @pool.ddryacid.to_d > 0
      @count += 1
    end

    if @pool.spabox && @pool.bbakingsoda.nil? == false && @pool.bbakingsoda.to_d > 0
      @count += 1
    end

    if @pool.spabox && @pool.bsodaash.nil? == false && @pool.bsodaash.to_d > 0
      @count += 1
    end
    if @pool.trichlor3 && @pool.trichlor3Size.nil? == false && @pool.trichlor3Size.to_d > 0
      @count += 1
    end

    if @pool.sodaash && @pool.sodaashSize.nil? == false && @pool.sodaashSize.to_d > 0
      @count += 1
    end

    if @pool.dryacid && @pool.dryacidSize.nil? == false && @pool.dryacidSize.to_d > 0
      @count += 1
    end

    if @pool.sodium && @pool.sodiumSize.nil? == false && @pool.sodiumSize.to_d > 0
      @count += 1
    end

    if @pool.bleach6 && @pool.bleach6Size.nil? == false && @pool.bleach6Size.to_d > 0
      @count += 1
    end

    if @pool.bleach8 && @pool.bleach8Size.nil? == false && @pool.bleach8Size.to_d > 0
      @count += 1
    end

    if @pool.bleach12 && @pool.bleach12Size.nil? == false && @pool.bleach12Size.to_d > 0
      @count += 1
    end

    if @pool.dichlor && @pool.dichlorSize.nil? == false && @pool.dichlorSize.to_d > 0
      @count += 1
    end

    if @pool.dichlor62 && @pool.dichlor62Size.nil? == false && @pool.dichlor62Size.to_d > 0
      @count += 1
    end

    if @pool.calhypo53 && @pool.calhypo53Size.nil? == false && @pool.calhypo53Size.to_d > 0
      @count += 1
    end

    if @pool.calhypo65 && @pool.calhypo65Size.nil? == false && @pool.calhypo65Size.to_d > 0
      @count += 1
    end

    if @pool.calhypo73 && @pool.calhypo73Size.nil? == false && @pool.calhypo73Size.to_d > 0
      @count += 1
    end

    if @pool.borax && @pool.boraxSize.nil? == false && @pool.boraxSize.to_d > 0
      @count += 1
    end

    if @pool.muriatic15 && @pool.muriatic15Size.nil? == false && @pool.muriatic15Size.to_d > 0
      @count += 1
    end

    if @pool.muriatic31 && @pool.muriatic31Size.nil? == false && @pool.muriatic31Size.to_d > 0
      @count += 1
    end
    return @count
  end


  #return last used chlorine
  def getlastusedchlorine(pool)
    @pool = pool
    @lastdose = Dosage.where(:pool_id => @pool.id).last
    if @lastdose.blank? == false
      if pool.isPool == false
        if @lastdose.liquidCl6.blank? == false && @lastdose.liquidCl6.to_d > 0
          @Cl_type= 5
        elsif @lastdose.liquidChlorine.blank? == false && @lastdose.liquidChlorine.to_d > 0
          @Cl_type= 4
        elsif (@lastdose.calhypo53.blank? == false && @lastdose.calhypo53.to_d > 0) || (@lastdose.calhypo65.blank? == false && @lastdose.calhypo65.to_d > 0) || (@lastdose.calhypo73.blank? == false && @lastdose.calhypo73.to_d > 0)
          @Cl_type= 0
        elsif (@lastdose.dichlor.blank? == false && @lastdose.dichlor.to_d > 0) || (@lastdose.esanitizer.blank? == false && @lastdose.esanitizer.to_d > 0)
          @Cl_type= 1
        elsif @lastdose.dichlor62Size.blank? == false && @lastdose.dichlor62Size.to_d > 0
          @Cl_type= 2
        elsif @lastdose.trichlor.blank? == false && @lastdose.trichlor.to_d > 0
          @Cl_type= 3
        else
          @Cl_type = 0
        end
      else
        if @lastdose.liquidCl6.blank? == false && @lastdose.liquidCl6.to_d > 0
          @Cl_type= 5
        elsif @lastdose.liquidChlorine.blank? == false && @lastdose.liquidChlorine.to_d > 0
          @Cl_type= 4
        elsif (@lastdose.calhypo53.blank? == false && @lastdose.calhypo53.to_d > 0) || (@lastdose.calhypo65.blank? == false && @lastdose.calhypo65.to_d > 0) || (@lastdose.calhypo73.blank? == false && @lastdose.calhypo73.to_d > 0)
          @Cl_type= 0
        elsif @lastdose.dichlor.blank? == false && @lastdose.dichlor.to_d > 0
          @Cl_type= 1
        elsif @lastdose.dichlor62Size.blank? == false && @lastdose.dichlor62Size.to_d > 0
          @Cl_type= 2
        elsif @lastdose.trichlor.blank? == false && @lastdose.trichlor.to_d > 0
          @Cl_type= 3
        else
          @Cl_type = 0
        end
      end
    end

    return @Cl_type
  end

  #return last used acid
  def getlastusedacid(pool)
    @pool = pool
    @lastdose = Dosage.where(:pool_id => @pool.id).last
    @type = 0
    if @lastdose.blank? == false
      if @lastdose.phDown.blank? == false && @lastdose.phDown.to_d > 0
        @type = 1
      elsif @lastdose.muriatic31.blank? == false && @lastdose.muriatic31.to_d > 0
        @type = 2
      end
    end
    return @type
  end

  # TODO REMOVE
  def notifyadminofthestate(pool)
    return

    @pool = pool
    @customer = Customer.find(@pool.customer_id)
    @role_name = getuserrole(@customer)

    #decide range
    @orp = 0
    @ph = 0
    @ta_i_val = ""

    #Fetch manual readings
    @mreading = @pool.mtests.last
    @daysdiff = ""
    @reading_type = ""
    if @mreading.blank? == false
      @reading_type = "manual"
      if @mreading.pH.blank? == false
        @ph = @mreading.pH
      end
      if @mreading.freeCl.blank? == false
        @free_cl = @mreading.freeCl.to_d
      end
      if @mreading.totalAlkalinity.blank? == false
        @ta_i_val = @mreading.totalAlkalinity.to_d
      end
      @daysdiff = ((Time.now - @mreading.created_at)/1.day).round
    end
    if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
      @reading_type = "device"
      unless @pool.epools.blank?
        @pool.epools.each do |epool|
          @orp = epool.oOrp
          unless epool.points.last.blank?  || epool.nil?
            @ph = ph(epool)
            @orp = orp(epool)
            @daysdiff = ((Time.now - epool.points.last.created_at)/1.day).round
            #Rails.logger.info("DAYSDIFF : "+@daysdiff.to_s)
          end # if points check
        end # looping of devices
      end # if pool has device check
    end

    if @daysdiff.blank? == false && @daysdiff >= 3
      Rails.logger.info("NOTIFYADMIN - Returning as readings are greater than 3 days")
      return ""
    end

    #decide range of the system and what to be prescribed
    @range = "notgood"
    @phrange = "notgood"
    @chlrange = "notgood"
    @tarange = ""


    if @role_name == "Lora"
      #ph decider
      if @ph.to_d >= 7.2 && @ph.to_d <= 7.8
        @phrange = "good"
        Rails.logger.info("NOTIFYADMIN - PH RANGE GOOD")
      end
      #chlorine decider
      if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 600 && @orp.to_d <= 900))
        @chlrange = "good"
        Rails.logger.info("NOTIFYADMIN - CH RANGE GOOD")
      end
      #ta decider
      if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
        @tarange = "good"
        Rails.logger.info("NOTIFYADMIN - TA RANGE GOOD")
      elsif @ta_i_val.blank? == false
        @tarange = "notgood"
        Rails.logger.info("NOTIFYADMIN - TA RANGE NOT GOOD")
      end
    else
      #ph decider
      if @ph.to_d >= 7.4 && @ph.to_d <= 7.6
        @phrange = "good"
        Rails.logger.info("NOTIFYADMIN - PH RANGE GOOD")
      end
      #chlorine decider
      if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 650 && @orp.to_d <= 900))
        @chlrange = "good"
        Rails.logger.info("NOTIFYADMIN - CH RANGE GOOD")
      end
      #ta decider
      if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
        @tarange = "good"
        Rails.logger.info("NOTIFYADMIN - TA RANGE GOOD")
      elsif @ta_i_val.blank? == false
        @tarange = "notgood"
        Rails.logger.info("NOTIFYADMIN - TA RANGE NOT GOOD")
      end
    end

    #range decider ends

    if @phrange == "good" && @chlrange == "good" && (@tarange == "good" || @tarange == "")
      deletealldosages(@pool)
      Rails.logger.info("NOTIFYADMIN - Returning as system is in good range")
      return ""
    else
      Rails.logger.info("NOTIFYADMIN - NOT GOOD RANGE")
      @dosage = Dosage.where(pool_id: @pool.id).where(status: false).limit(1)
      @count = getnoofchemicalsprescribed(@dosage[0])
      Rails.logger.info("NOTIFYADMIN - CHEMICALS COUNT: "+@count.to_s)

      if @count <= 0
        #send admin notification
        sendnotgoodnodosageemail("BAD_STATE_NO_DOSAGE",@pool,@reading_type)
      end
    end
  end

  #return no of chemicals prescribed in a dosage
  def getnoofchemicalsprescribed(dosage)
    @count = 0
    if dosage.blank? == false
      if dosage.trichlor2Size.blank? == false
        @count += 1
      end
      if dosage.bromine1Size.blank? == false
        @count += 1
      end
      if dosage.lithiumhypoSize.blank? == false
        @count += 1
      end
      if dosage.calciumSize.blank? == false
        @count += 1
      end
      if dosage.cyanuricsolidSize.blank? == false
        @count += 1
      end
      if dosage.cyanuricliquidSize.blank? == false
        @count += 1
      end

      #Extra chemicals section ends

      if dosage.trichlor.blank? == false
        @count += 1
      end
      if dosage.phUp.blank? == false
        @count += 1
      end
      if dosage.phDown.blank? == false
        @count += 1
      end
      if dosage.taUp.blank? == false
        @count += 1
      end
      if dosage.liquidCl6.blank? == false
        @count += 1
      end
      if dosage.liquidCl8.blank? == false
        @count += 1
      end
      if dosage.liquidChlorine.blank? == false
        @count += 1
      end
      if dosage.dichlor.blank? == false
        @count += 1
      end
      if dosage.dichlor62Size.blank? == false
        @count += 1
      end
      if dosage.calhypo53.blank? == false
        @count += 1
      end
      if dosage.calhypo65.blank? == false
        @count += 1
      end
      if dosage.calhypo73.blank? == false
        @count += 1
      end
      if dosage.phUpBorax.blank? == false
        @count += 1
      end
      if dosage.muriatic15.blank? == false
        @count += 1
      end
      if dosage.muriatic31.blank? == false
        @count += 1
      end

      #For spa
      if dosage.esanitizer.blank? == false
        @count += 1
      end
      if dosage.ddryacid.blank? == false
        @count += 1
      end
      if dosage.bbakingsoda.blank? == false
        @count += 1
      end

      if dosage.bsodaash.blank? == false
        @count += 1
      end
    end

    return @count
  end

  # TODO REMOVE
  def sendnotgoodnodosageemail(emailid,pool,readingtype)
    return

    @emails = Emails.find_by_short(emailid)
    if @emails.status
      @customer  = Customer.find(pool.customer_id)

      @user = User.find_by_email(@customer.email)

      @greeting = @customer.firstName+" "+@customer.lastName
      @greeting_first = @customer.firstName
      @greeting_last = @customer.lastName
      @email = @customer.email
      @phone = @customer.mPhone

      @ph = ""
      @orp = ""
      @freeCl = ""
      @ta = ""
      @temprtr = ""

      if readingtype == "device"
        unless pool.epools.blank?
          pool.epools.each do |epool|
            unless epool.points.last.blank?  || epool.nil?
              @ph = ph(epool)
              @orp = orp(epool)
              @temprtr = temperature(epool)
            end
          end
        end
      elsif readingtype == "manual"
        @manual_measurement = pool.mtests.last
        if @manual_measurement.nil? == false
          @ph = @manual_measurement.pH
          @freeCl = @manual_measurement.freeCl
          @ta = @manual_measurement.totalAlkalinity
        end
      end
      @pool_url = Rails.application.config.siteurl+url_for("/trguytbyhe/encs?app="+Base64.encode64("?customer_id="+@customer.id.to_s+"?pool_id="+pool.id.to_s))
      @subject = @emails.subject.to_s
      @subject = @subject.gsub("[greeting]", @greeting)
      @subject = @subject.gsub("[greeting_first]", @greeting_first)
      @subject = @subject.gsub("[greeting_last]", @greeting_last)

      @content = @emails.content
      @content = @content.to_s
      @content = @content.gsub("[greeting]", @greeting)
      @content = @content.gsub("[greeting_first]", @greeting_first)
      @content = @content.gsub("[greeting_last]", @greeting_last)
      @content = @content.gsub("[email]", @email)
      @content = @content.gsub("[phone]", @phone.to_s)

      @reading_details = ""
      if readingtype == "device"
        @reading_details += "<b><h4>Device readings</h4></b>"
        @reading_details += "<p><b>pH : </b>"+@ph.to_s
        @reading_details += "</p><p><b>ORP : </b>"+@orp.to_s
        @reading_details += "</p><p><b>Temperature : </b>"+@temprtr.to_s+"</p>"
      elsif readingtype == "manual"
        @reading_details += "<b><h4>Manual readings</h4></b>"
        @reading_details += "<p><b>pH : </b>"+@ph.to_s
        @reading_details += "</p><p><b>Free Chlorine : </b>"+@freeCl.to_s
        @reading_details += "</p><p><b>Total Alkalinity : </b>"+@ta.to_s+"</p>"
      end

      @content = @content.gsub("[readingdetails]", @reading_details)
      @content = @content.gsub("[url]", @pool_url)

      SesMailer.customeremail(@emails.toaddress,@subject,@content).deliver
    else
      smslogger.info "===========Email not sent. #{emailid}  email is  not enabled================"
    end
  end


  #function adds the contact to the low contact list
  def updatelowstockcontact(emailid,content)
    Rails.logger.info("Trigger for Low Stock "+emailid)
    @details = getchemicallowcontentfortrigger(content)

    @list_id = "contactlist_1D3FBC5E-706F-4FEC-B414-047439DD7726" # Prefix "contactlist_" to the list id

    @body = '{
			"contact":{
				"FirstName":"",
				"LastName": "",
			    "Email": "'+emailid+'",
			    "custom": {
			      "string--LowChem1": "'+@details[0]+'",
			      "string--LowChem2": "'+@details[1]+'",
			      "string--LowChem3": "'+@details[2]+'",
			      "string--LowChem4": "'+@details[3]+'",
			      "string--LowChem5": "'+@details[4]+'"
			    }
			}
		}'
    response = HTTParty.post("https://api2.autopilothq.com/v1/contact", :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7","Content-Type"=>"application/json"},:body => @body,:verify => false)
    #puts response
    response = HTTParty.post("https://api2.autopilothq.com/v1/list/"+@list_id+"/contact/"+emailid, :headers => { "autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7","Content-Type"=>"application/json"},:verify => false)
    #puts response
  end

  #process the raw low chemical content and make it for tirgger
  def getchemicallowcontentfortrigger(content)
    @contenthere = content.split("~")
    @text = []
    @iter = 0
    @contenthere.each_with_index do |chem,index|
      if chem != ""
        #puts chem.inspect
        @chem_name = chem.split(":")[0]
        #puts @chem_name
        if @chem_name == "Trichlor"
          @text[@iter] = "chlor-T1"
        elsif @chem_name == "Bromine"
          @text[@iter] = "chlor-TB3"
        elsif @chem_name == "Lithium Hypo"
          @text[@iter] = "LiShock-G"
        elsif @chem_name == "Calcium"
          @text[@iter] = "chup-G"
        elsif @chem_name == "Cyanuric Solid"
          @text[@iter] = "CYA"
        elsif @chem_name == "Cyanuric Liquid"
          @text[@iter] = "STB"
        elsif @chem_name == "Trichlor 3"
          @text[@iter] = "chlor-T3"
        elsif @chem_name == "Soda Ash"
          @text[@iter] = "pHup-A"
        elsif @chem_name == "Dry Acid"
          @text[@iter] = "pHdown-G"
        elsif @chem_name == "Sodium"
          @text[@iter] = "BiCarb-G"
        elsif @chem_name == "Liquid Chlorine 6%"
          @text[@iter] = "chlor-L6"
        elsif @chem_name == "Liquid Chlorine 8%"
          @text[@iter] = "chlor-L8"
        elsif @chem_name == "Liquid Chlorine 12%"
          @text[@iter] = "chlor-L12"
        elsif @chem_name == "Dichlor"
          @text[@iter] = "chlor-G56"
        elsif @chem_name == "Dichlor 62"
          @text[@iter] = "chlor-G62"
        elsif @chem_name == "Cal Hypo 53"
          @text[@iter] = "CalShock-G53"
        elsif @chem_name == "Cal Hypo 65"
          @text[@iter] = "CalShock-G65"
        elsif @chem_name == "Cal Hypo 73"
          @text[@iter] = "CalShock-G73"
        elsif @chem_name == "Borax"
          @text[@iter] = "pHup-B"
        elsif @chem_name == "Muriatic Acid 15%"
          @text[@iter] = "pHdown-L15"
        elsif @chem_name == "Muriatic Acid 31%"
          @text[@iter] = "pHdown-L31"
        elsif @chem_name == "Packet E"
          @text[@iter] = "SpaChlor"
        elsif @chem_name == "Packet D"
          @text[@iter] = "SpaDown"
        elsif @chem_name == "Packet B"
          @text[@iter] = "SpaUp"
        elsif @chem_name == "Packet A"
          @text[@iter] = "SpaTA"
        end

        @iter += 1
      end
    end
    if @iter < 5
      while @iter <= 4  do
        @text[@iter] = ""
        @iter += 1
      end
    end

    return @text
  end

  #process the chemical and return its type
  def getchemicallowtype(chemical)
    @chem_name = chemical
    if @chem_name != ""
      if @chem_name == "Trichlor"
        @text = "Sanitizer"
      elsif @chem_name == "Bromine"
        @text = "Sanitizer"
      elsif @chem_name == "Lithium Hypo"
        @text = "Sanitizer"
      elsif @chem_name == "Calcium"
        @text = "Calcuim Hardness Increaser"
      elsif @chem_name == "Cyanuric Solid"
        @text = "Stabilizer"
      elsif @chem_name == "Cyanuric Liquid"
        @text = "Stabilizer"
      elsif @chem_name == "Trichlor 3"
        @text = "Sanitizer"
      elsif @chem_name == "Soda Ash"
        @text = "pH Up"
      elsif @chem_name == "Dry Acid"
        @text = "pH Down"
      elsif @chem_name == "Sodium"
        @text = "TA Up"
      elsif @chem_name == "Liquid Chlorine 6%"
        @text = "Sanitizer"
      elsif @chem_name == "Liquid Chlorine 8%"
        @text = "Sanitizer"
      elsif @chem_name == "Liquid Chlorine 12%"
        @text = "Sanitizer"
      elsif @chem_name == "Dichlor"
        @text = "Sanitizer"
      elsif @chem_name == "Dichlor 62"
        @text = "Sanitizer"
      elsif @chem_name == "Cal Hypo 53"
        @text = "Sanitizer"
      elsif @chem_name == "Cal Hypo 65"
        @text = "Sanitizer"
      elsif @chem_name == "Cal Hypo 73"
        @text = "Sanitizer"
      elsif @chem_name == "Borax"
        @text = "pH Up"
      elsif @chem_name == "Muriatic Acid 15%"
        @text = "pH Down"
      elsif @chem_name == "Muriatic Acid 31%"
        @text = "pH Down"
      elsif @chem_name == "Packet E"
        @text = "SpaChlor"
      elsif @chem_name == "Packet D"
        @text = "SpaDown"
      elsif @chem_name == "Packet B"
        @text = "SpaUp"
      elsif @chem_name == "Packet A"
        @text = "SpaTA"
      end
    end

    return @text
  end

  #process the chemical and return its type
  #this method is specifically used for generating the dosage text def generatedosagetext()
  def getchemicaltype(pool,chemical)
    @sanitizer = "Pool Shock"
    @phup = "pH Up"
    @phdown = "pH Down"
    @taup = "TA Up"
    @san = "Pool Shock"
    if pool.isPool == false
      @san = "Sanitizer"
      @sanitizer = "E"
      @phup = "B"
      @phdown = "D"
      @taup = "A"
    end
    @chem_name = chemical
    if @chem_name != ""
      if @chem_name == "Trichlor"
        @text = "Sanitizer"
      elsif @chem_name == "Bromine"
        @text = "Sanitizer"
      elsif @chem_name == "Lithium Hypo"
        @text = "Sanitizer"
      elsif @chem_name == "Calcium chloride"
        @text = "Calcuim Hardness Increaser"
      elsif @chem_name == "Cyanuric Acid Solid"
        @text = "Stabilizer"
      elsif @chem_name == "Cyanuric Acid Liquid"
        @text = "Stabilizer"
      elsif @chem_name == "Trichlor 3"
        @text = "Sanitizer"
      elsif @chem_name == "Soda Ash"
        @text = "pH Up"
      elsif @chem_name == "Dry Acid" || @chem_name == "Vinegar"
        @text = "pH Down"
      elsif @chem_name == "Sodium Bicarbonate"
        @text = "TA Up"
      elsif @chem_name == "6% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
        @text = "Liquid Chlorine"
      elsif @chem_name == "8% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
        @text = "Liquid Chlorine"
      elsif @chem_name == "12% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
        @text = "Liquid Chlorine"
      elsif @chem_name == "56% Dichlor" || @chem_name == "Dichlor"
        @text = @san
      elsif @chem_name == "62% Dichlor" || @chem_name == "Dichlor"
        @text = @san
      elsif @chem_name == "53% Cal-hypo" || @chem_name == "Cal-hypo"
        @text = @san
      elsif @chem_name == "65% Cal-hypo" || @chem_name == "Cal-hypo"
        @text = @san
      elsif @chem_name == "73% Cal-hypo" || @chem_name == "Cal-hypo"
        @text = @san
      elsif @chem_name == "Borax"
        @text = "pH Up"
      elsif @chem_name == "15.7% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "15% Muriatic Acid"
        @text = "pH Down"
      elsif @chem_name == "31% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "31.45% Muriatic Acid"
        @text = "pH Down"
      elsif @chem_name == "E"
        @text = @sanitizer
      elsif @chem_name == "D"
        @text = @phdown
      elsif @chem_name == "B"
        @text = @phup
      elsif @chem_name == "A"
        @text = @taup
      end
    end

    return @text
  end

  #process the chemical and return its type
  #this method is specifically used for generating the dosage text def generatedosagetext()
  def getchemicaltypeforskin(pool,chemical)
    @chem_name = chemical
    @san = "Pool Shock"
    @sanitizer = "Pool Shock"
    @phup = "pH Up"
    @phdown = "pH Down"
    @taup = "TA Up"
    @skin = @pool.chemical_skin
    if pool.isPool == false
      @san = "Sanitizer"
      @sanitizer = "E"
      @phup = "B"
      @phdown = "D"
      @taup = "A"
    end
    if @chem_name != ""
      if @skin == "basic"
        if @chem_name == "Trichlor"
          @text = "Sanitizer"
        elsif @chem_name == "Bromine"
          @text = "Sanitizer"
        elsif @chem_name == "Lithium Hypo"
          @text = "Sanitizer"
        elsif @chem_name == "Calcium chloride"
          @text = "Calcuim Hardness Increaser"
        elsif @chem_name == "Cyanuric Acid Solid"
          @text = "Stabilizer"
        elsif @chem_name == "Cyanuric Acid Liquid"
          @text = "Stabilizer"
        elsif @chem_name == "Trichlor 3"
          @text = "Sanitizer"
        elsif @chem_name == "Soda Ash"
          @text = "pH Up"
        elsif @chem_name == "Dry Acid" || @chem_name == "Vinegar"
          @text = "pH Down"
        elsif @chem_name == "Sodium Bicarbonate"
          @text = "TA Up"
        elsif @chem_name == "6% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
          @text = "Liquid Chlorine"
        elsif @chem_name == "8% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
          @text = "Liquid Chlorine"
        elsif @chem_name == "12% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
          @text = "Liquid Chlorine"
        elsif @chem_name == "56% Dichlor" || @chem_name == "Dichlor"
          @text = @san
        elsif @chem_name == "62% Dichlor" || @chem_name == "Dichlor"
          @text = @san
        elsif @chem_name == "53% Cal-hypo" || @chem_name == "Cal-hypo"
          @text = @san
        elsif @chem_name == "65% Cal-hypo" || @chem_name == "Cal-hypo"
          @text = @san
        elsif @chem_name == "73% Cal-hypo" || @chem_name == "Cal-hypo"
          @text = @san
        elsif @chem_name == "Borax"
          @text = "pH Up"
        elsif @chem_name == "15.7% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "15% Muriatic Acid"
          @text = "pH Down"
        elsif @chem_name == "31% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "31.45% Muriatic Acid"
          @text = "pH Down"
        elsif @chem_name == "E"
          @text = @sanitizer
        elsif @chem_name == "D"
          @text = @phdown
        elsif @chem_name == "B"
          @text = @phup
        elsif @chem_name == "A"
          @text = @taup
        end
      elsif @skin == "advanced"
        @text = @chem_name
      end
    end

    return @text
  end

  #function returns chemical code for given chemical
  #this codes and chemical names are only used for refill button click redirect on beta dashboard
  def getchemicalcode(chemical)
    @text = ""
    if chemical != ""
      #puts chem.inspect
      @chem_name = chemical.downcase
      #puts @chem_name
      if @chem_name == "trichlor"
        @text = "chlor-T"
      elsif @chem_name == "bromine"
        @text = "chlor-TB"
      elsif @chem_name == "lithium hypo"
        @text = "lishock"
      elsif @chem_name == "calcium"
        @text = "chup-G"
      elsif @chem_name == "cyanuric solid"
        @text = "CYA"
      elsif @chem_name == "cyanuric liquid"
        @text = "STB"
      elsif @chem_name == "trichlor 3"
        @text = "chlor-T"
      elsif @chem_name == "soda ash"
        @text = "phup-A"
      elsif @chem_name == "dry acid"
        @text = "phdown-G"
      elsif @chem_name == "sodium"
        @text = "Bicarb-G"
      elsif @chem_name == "bleach 6%"
        @text = "chlor-L"
      elsif @chem_name == "bleach 8%"
        @text = "chlor-L"
      elsif @chem_name == "bleach 12%"
        @text = "chlor-L"
      elsif @chem_name == "dichlor"
        @text = "chlor-G"
      elsif @chem_name == "dichlor 62"
        @text = "chlor-G"
      elsif @chem_name == "cal-hypo 53"
        @text = "calshock"
      elsif @chem_name == "cal-hypo 65"
        @text = "calshock"
      elsif @chem_name == "cal-hypo 73"
        @text = "calshock"
      elsif @chem_name == "borax"
        @text = "phup-B"
      elsif @chem_name == "muriatic acid 15%"
        @text = "phdown-L"
      elsif @chem_name == "muriatic acid 31%"
        @text = "phdown-L"
      elsif @chem_name == "packets e"
        @text = "spachlor"
      elsif @chem_name == "packets d"
        @text = "spadown"
      elsif @chem_name == "packets b"
        @text = "spaup"
      elsif @chem_name == "packets a"
        @text = "spata"
      end
    end

    return @text
  end


  #check and notify about the chemical stock low
  def islowstock(pool)
    @content = ""
    @content = processchemstock(pool)
    return @content
  end

  #return beta upload directory
  def getsourceuploadfolder
    return "/local/PoolUploads/images"
  end

  #return beta upload directory
  def getdestinationuploadfolder
    return "/var/app/current/public/images/uploads"
  end

  #Remove from stock when manual dosing is done by a customer
  #this is used by ony senddosagependingsms function here
  def customerdonedosage(presc_id)
    @dosage = Dosage.find(presc_id)

    #Reduce dosage chemical amount from the customer warehouse
    @pool = Pool.find(@dosage.pool_id)

    #find measurement based on pool type [pool/spa]
    @unit = 1
    @nonSpaBoxUnit = 0.5
    @bleach = 16
    @muriatic = 16
    @soda_ash_cup = 0.574
    @dichlor = 0.5375 #1 cup == 0.5375 lb == 16 tbs
    @spabox = 16 #1lbs = oz
    # Spa
    @bsodaash_pac = 0.25
    @ddryacid_pac = 0.5
    @bbakingsoda_pac = 0.5
    @esanitizer_pac = 0.25

    if @pool.isPool
      @unit = 1
      @nonSpaBoxUnit = 1
    else
      @unit = 1
      @nonSpaBoxUnit = 1
    end


    #Store extra chemicals for spa details
    if @pool.trichlor2Size.blank? == false && @dosage.trichlor2Size.blank? == false
      @pool.trichlor2Size = @pool.trichlor2Size.to_d - (@dosage.trichlor2Size.to_d*0.0375)
      if @pool.trichlor2Size.to_d < 0
        @pool.trichlor2Size = 0
      end
    end
    if @pool.bromine1Size.blank? == false && @dosage.bromine1Size.blank? == false
      @pool.bromine1Size = @pool.bromine1Size.to_d - (@dosage.bromine1Size.to_d*0.03125)
      if @pool.bromine1Size.to_d < 0
        @pool.bromine1Size = 0
      end
    end
    if @pool.lithiumhypoSize.blank? == false && @dosage.lithiumhypoSize.blank? == false
      @pool.lithiumhypoSize = @pool.lithiumhypoSize.to_d - (@dosage.lithiumhypoSize.to_d)/@nonSpaBoxUnit
      if @pool.lithiumhypoSize.to_d < 0
        @pool.lithiumhypoSize = 0
      end
    end
    if @pool.calciumSize.blank? == false && @dosage.calciumSize.blank? == false
      @pool.calciumSize = @pool.calciumSize.to_d - (@dosage.calciumSize.to_d)/@nonSpaBoxUnit
      if @pool.calciumSize.to_d < 0
        @pool.calciumSize = 0
      end
    end
    if @pool.cyanuricsolidSize.blank? == false && @dosage.cyanuricsolidSize.blank? == false
      @pool.cyanuricsolidSize = @pool.cyanuricsolidSize.to_d - (@dosage.cyanuricsolidSize.to_d)/@nonSpaBoxUnit
      if @pool.cyanuricsolidSize.to_d < 0
        @pool.cyanuricsolidSize = 0
      end
    end
    if @pool.cyanuricliquidSize.blank? == false && @dosage.cyanuricliquidSize.blank? == false
      @pool.cyanuricliquidSize = @pool.cyanuricliquidSize.to_d - (@dosage.cyanuricliquidSize.to_d)/@nonSpaBoxUnit
      if @pool.cyanuricliquidSize.to_d < 0
        @pool.cyanuricliquidSize = 0
      end
    end

    #Extra chemicals section ends

    if @pool.trichlor3Size.blank? == false && @dosage.trichlor.blank? == false
      @pool.trichlor3Size = @pool.trichlor3Size.to_d - (@dosage.trichlor.to_d*0.04375)
      if @pool.trichlor3Size.to_d < 0
        @pool.trichlor3Size = 0
      end
    end
    if @pool.sodaashSize.blank? == false && @dosage.phUp.blank? == false
      @pool.sodaashSize = @pool.sodaashSize.to_d - (@dosage.phUp.to_d)*@soda_ash_cup
      if @pool.sodaashSize.to_d < 0
        @pool.sodaashSize = 0
      end
    end
    if @pool.dryacidSize.blank? == false && @dosage.phDown.blank? == false
      @pool.dryacidSize = @pool.dryacidSize.to_d - (@dosage.phDown.to_d)/@nonSpaBoxUnit
      if @pool.dryacidSize.to_d < 0
        @pool.dryacidSize = 0
      end
    end
    if @pool.sodiumSize.blank? == false && @dosage.taUp.blank? == false
      @pool.sodiumSize = @pool.sodiumSize.to_d - (@dosage.taUp.to_d)/@nonSpaBoxUnit
      if @pool.sodiumSize.to_d < 0
        @pool.sodiumSize = 0
      end
    end
    if @pool.bleach6Size.blank? == false && @dosage.liquidCl6.blank? == false
      if @dosage.lch6unit.downcase  == "cup" && @dosage.lch6qunit.downcase  == "cup"
        @pool.bleach6Size = @pool.bleach6Size.to_d - (@dosage.liquidCl6.to_d)/@bleach
      elsif @dosage.lch6unit.downcase  == "gallon" && @dosage.lch6qunit.downcase  == "gallon"
        @pool.bleach6Size = @pool.bleach6Size.to_d - (@dosage.liquidCl6.to_d)
      else
        @pool.bleach6Size = @pool.bleach6Size.to_d - (@dosage.liquidCl6.to_d)
      end
      if @pool.bleach6Size.to_d < 0
        @pool.bleach6Size = 0
      end
    end
    if @pool.bleach8Size.blank? == false && @dosage.liquidCl8.blank? == false
      if @dosage.lch8unit.downcase  == "cup" && @dosage.lch8qunit.downcase  == "cup"
        @pool.bleach8Size = @pool.bleach8Size.to_d - (@dosage.liquidCl8.to_d)/@bleach
      elsif @dosage.lch8unit.downcase  == "gallon" && @dosage.lch8qunit.downcase  == "gallon"
        @pool.bleach8Size = @pool.bleach8Size.to_d - (@dosage.liquidCl8.to_d)
      else
        @pool.bleach8Size = @pool.bleach8Size.to_d - (@dosage.liquidCl8.to_d)
      end
      if @pool.bleach8Size.to_d < 0
        @pool.bleach8Size = 0
      end
    end
    if @pool.bleach12Size.blank? == false && @dosage.liquidChlorine.blank? == false
      if @dosage.lch12unit.downcase  == "cup" && @dosage.lch12qunit.downcase  == "cup"
        @pool.bleach12Size = @pool.bleach12Size.to_d - (@dosage.liquidChlorine.to_d)/@bleach
      elsif @dosage.lch12unit.downcase  == "gallon" && @dosage.lch12qunit.downcase  == "gallon"
        @pool.bleach12Size = @pool.bleach12Size.to_d - (@dosage.liquidChlorine.to_d)
      else
        @pool.bleach12Size = @pool.bleach12Size.to_d - (@dosage.liquidChlorine.to_d)
      end
      if @pool.bleach12Size.to_d < 0
        @pool.bleach12Size = 0
      end
    end
    if @pool.dichlorSize.blank? == false && @dosage.dichlor.blank? == false
      #puts @pool.dichlorSize.to_d
      #puts @dosage.dichlor.to_d
      #puts @pool.dichlorSize.to_d - (@dosage.dichlor.to_d)/@nonSpaBoxUnit
      @pool.dichlorSize = @pool.dichlorSize.to_d - (@dosage.dichlor.to_d*@dichlor)/@nonSpaBoxUnit
      if @pool.dichlorSize.to_d < 0
        @pool.dichlorSize = 0
      end
    end
    if @pool.dichlor62Size.blank? == false && @dosage.dichlor62Size.blank? == false
      #puts @pool.dichlor62Size.to_d
      #puts @dosage.dichlor62Size.to_d
      #puts @pool.dichlor62Size.to_d - (@dosage.dichlor62Size.to_d)/@nonSpaBoxUnit
      @pool.dichlor62Size = @pool.dichlor62Size.to_d - (@dosage.dichlor62Size.to_d*@dichlor)/@nonSpaBoxUnit
      if @pool.dichlor62Size.to_d < 0
        @pool.dichlor62Size = 0
      end
    end
    if @pool.calhypo53Size.blank? == false && @dosage.calhypo53.blank? == false
      @pool.calhypo53Size = @pool.calhypo53Size.to_d - (@dosage.calhypo53.to_d)/@nonSpaBoxUnit
      if @pool.calhypo53Size.to_d < 0
        @pool.calhypo53Size = 0
      end
    end
    if @pool.calhypo65Size.blank? == false && @dosage.calhypo65.blank? == false
      @pool.calhypo65Size = @pool.calhypo65Size.to_d - (@dosage.calhypo65.to_d)/@nonSpaBoxUnit
      if @pool.calhypo65Size.to_d < 0
        @pool.calhypo65Size = 0
      end
    end
    if @pool.calhypo73Size.blank? == false && @dosage.calhypo73.blank? == false
      @pool.calhypo73Size = @pool.calhypo73Size.to_d - (@dosage.calhypo73.to_d)/@nonSpaBoxUnit
      if @pool.calhypo73Size.to_d < 0
        @pool.calhypo73Size = 0
      end
    end
    if @pool.boraxSize.blank? == false && @dosage.phUpBorax.blank? == false
      @pool.boraxSize = @pool.boraxSize.to_d - (@dosage.phUpBorax.to_d)/@nonSpaBoxUnit
      if @pool.boraxSize.to_d < 0
        @pool.boraxSize = 0
      end
    end
    if @pool.muriatic15Size.blank? == false && @dosage.muriatic15.blank? == false
      if @dosage.macid15unit.downcase  == "cup" && @dosage.macid15qunit.downcase  == "cup"
        @pool.muriatic15Size = @pool.muriatic15Size.to_d - (@dosage.muriatic15.to_d) / @muriatic
      elsif @dosage.macid15unit.downcase  == "gallon" && @dosage.macid15qunit.downcase  == "gallon"
        @pool.muriatic15Size = @pool.muriatic15Size.to_d - (@dosage.muriatic15.to_d)
      else
        @pool.muriatic15Size = @pool.muriatic15Size.to_d - (@dosage.muriatic15.to_d)
      end
      if @pool.muriatic15Size.to_d < 0
        @pool.muriatic15Size = 0
      end
    end
    if @pool.muriatic31Size.blank? == false && @dosage.muriatic31.blank? == false
      if @dosage.macid31unit.downcase  == "cup" && @dosage.macid31qunit.downcase  == "cup"
        @pool.muriatic31Size = @pool.muriatic31Size.to_d - (@dosage.muriatic31.to_d) / @muriatic
      elsif @dosage.macid31unit.downcase  == "gallon" && @dosage.macid31qunit.downcase  == "gallon"
        @pool.muriatic31Size = @pool.muriatic31Size.to_d - (@dosage.muriatic31.to_d)
      else
        @pool.muriatic31Size = @pool.muriatic31Size.to_d - (@dosage.muriatic31.to_d)
      end
      if @pool.muriatic31Size.to_d < 0
        @pool.muriatic31Size = 0
      end
    end

    #For spa
    if @pool.isPool == false  && @pool.spabox
      if @pool.esanitizer.blank? == false && @dosage.esanitizer.blank? == false
        @pool.esanitizer = @pool.esanitizer.to_d - (@dosage.esanitizer.to_d*@esanitizer_pac)/@spabox
        if @pool.esanitizer.to_d < 0
          @pool.esanitizer = 0
        end
      end
      if @pool.ddryacid.blank? == false && @dosage.ddryacid.blank? == false
        @pool.ddryacid = @pool.ddryacid.to_d - (@dosage.ddryacid.to_d*@ddryacid_pac)/@spabox
        if @pool.ddryacid.to_d < 0
          @pool.ddryacid = 0
        end
      end
      if @pool.bbakingsoda.blank? == false && @dosage.bbakingsoda.blank? == false
        @pool.bbakingsoda = @pool.bbakingsoda.to_d - (@dosage.bbakingsoda.to_d*@bbakingsoda_pac)/@spabox
        if @pool.bbakingsoda.to_d < 0
          @pool.bbakingsoda = 0
        end
      end

      if @pool.bsodaash.blank? == false && @dosage.bsodaash.blank? == false
        @pool.bsodaash = @pool.bsodaash.to_d - (@dosage.bsodaash.to_d*@bsodaash_pac)/@spabox
        if @pool.bsodaash.to_d < 0
          @pool.bsodaash = 0
        end
      end
    end

    @pool.save
  end

end

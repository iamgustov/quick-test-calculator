module Calculator
  class Pool
    attr_accessor :id, :isPool, :volume, :waterChemistry, :chemical_skin,
                  :bleach6, :bleach6Size, :bleach8, :bleach8Size, :bleach12, :bleach12Size, :trichlor3, :trichlor3Size,
                  :trichlor2, :trichlor2Size, :bromine1, :bromine1Size, :dichlor, :dichlorSize, :dichlor62, :dichlor62Size,
                  :calhypo53, :calhypo53Size, :calhypo65, :calhypo65Size, :calhypo73, :calhypo73Size, :lithiumhypo, :lithiumhypoSize,
                  :sodaash, :sodaashSize, :borax, :boraxSize, :bakingsoda, :bakingsodaSize, :muriatic31, :muriatic31Size,
                  :muriatic15, :muriatic15Size, :dryacid, :dryacidSize, :sodium, :sodiumSize, :calcium, :calciumSize,
                  :cyanuricsolid, :cyanuricsolidSize, :cyanuricliquid, :cyanuricliquidSize,
                  :ozonator,
                  :spabox, :esanitizer, :option, :bsodaash,
                  :mtests, :customer_id, :dosages

    module WATER_CHEMISTRY
      CHLORINE = 'Chlorine'
      BROMINE = 'Bromine'
      SALT_WATER = 'Salt Water'
      MINERAL = 'Mineral'
    end

    module CHEMICAL_SKIN
      BASIC = 'basic'
      ADVANCED = 'advanced'
    end

    def initialize
      @id = 1
      @isPool = false
      @volume = 30000
      @waterChemistry = Pool::WATER_CHEMISTRY::MINERAL
      @chemical_skin = Pool::CHEMICAL_SKIN::ADVANCED
      @spabox = false
      @esanitizer = 0
      @mtests = []
      @dosages = []
    end

    def mtests
      @mtests
    end

    def create_mtest(ph:, fcl:, ta:)
      @mtests << Mtest.new(ph: ph, fcl: fcl, ta: ta)
    end

    def dosages
      @dosages
    end
  end
end
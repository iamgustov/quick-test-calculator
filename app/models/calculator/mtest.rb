module Calculator
  class Mtest
    attr_accessor :pH, :freeCl, :combinedCl, :totalAlkalinity, :calciumHardness, :cyanuricAcid, :created_at

    def initialize(ph:, fcl:, ta:)
      @pH = ph
      @freeCl = fcl
      @totalAlkalinity = ta
      @created_at = Time.now
    end
  end
end
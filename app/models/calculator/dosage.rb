module Calculator
  class Dosage
    attr_accessor :trichlor, :phUp, :phDown, :taUp, :liquidCl6, :liquidCl8,
                  :liquidChlorine, :dichlor, :calhypo53, :calhypo65, :calhypo73,
                  :phUpBorax, :muriatic15, :muriatic31, :status, :prescription,
                  :trichlor2Size, :bromine1Size, :lithiumhypoSize, :calciumSize,
                  :cyanuricsolidSize, :cyanuricliquidSize, :esanitizer, :ddryacid,
                  :bbakingsoda, :ph, :orp, :ph_complete, :orp_complete, :dichlor62Size,
                  :bsodaash, :approved, :lch6unit, :lch8unit, :lch12unit, :macid15unit,
                  :macid31unit, :lch6qunit, :lch8qunit, :lch12qunit, :macid15qunit,
                  :macid31qunit, :saltwater, :bsodaash, :pool_id, :customer_id

    def save(pool)
      pool.dosages << self
    end
  end
end
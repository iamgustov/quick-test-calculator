# require './text_generator'
# require '../pool'

module Tester
  class Dosage
    def _pool_dosage(ph, orp, ta, pool = nil)
      puts "==================================Pool Dosage====================================="
      @values = {}
      @pool = pool || Pool.new
      @auto_pres = true
      if @auto_pres
        puts "Feature ENABLED"
        if @pool.isPool
          #Inputs
          water_type="pool"

          #Get pool volume
          if @pool.volume.nil? == false
            @pool_vol_gal= @pool.volume #Gallons of pool water
          else
            @pool_vol_gal= 0
          end

          @orp = 0
          @ph = 0

          @ta_i_val = 0
          @cya_i_val = 0
          @free_cl = 0

          if ph.nil? == false
            @ph = ph
            if @ph.to_f < 7
              @ph = 7
            end
            if @ph.to_f > 8
              @ph = 8
            end
          end

          if orp.nil? == false
            @free_cl = orp
          end

          if ta.nil? == false
            @ta_i_val = ta
          end


          @sanitizer = @pool.waterChemistry

          @daysdiff = 0



          @pH_i= @ph #Initial pH from 0 to 14

          #Decide primary sanitizer
          @Primary_sanitizer= 0 #Check for bromine or chlorine spa

          if @sanitizer == 'Chlorine'
            #if @daysdiff.nil? == false && @daysdiff >= 3
            @Primary_sanitizer = 1
          else
            @Primary_sanitizer = 2
          end

          @ORP_i = @orp

          if @Primary_sanitizer==1 && (@free_cl <= 0)
            #@FC_i= @free_cl #Initial Free Chlorine (ppm)
            # @ORP_i=@orp #Initial ORP level (mV)
            @FC_i = (40000-Math.sqrt(6840000000-9600000*(@ORP_i)))/5000.0

            #if @FC_i.to_f < 0
            #	@FC_i = 0
            #end
          else
            @FC_i = @free_cl
            @ORP_i=@orp #Initial ORP level (mV)
          end
          if @Primary_sanitizer==2 && @daysdiff < 3
            @FC_i = @free_cl
          end

          @acid_type= 0 #Acid type
          if @pool.dryacid
            @acid_type = 1
          elsif @pool.muriatic31
            @acid_type = 2
          end

          @TA_i= @ta_i_val #input('What is the Total Alkalinity (ppm)?: '); #Initial Total Alkalinity (ppm)
          @CYA_i=@cya_i_val#input('What is the Cyanuric Acid level (ppm)?: '); #Initial Cyanuric acid level (ppm)
          if @pool.waterChemistry == "Salt Water"
            @SWG = 1 #salt water chlorinator
            #puts "SWG = 1"
          else
            @SWG = 2 #salt water chlorinator
            #puts "SWG = 1"
          end

          if @pool.ozonator
            @ozone = 1 #ozonator
          else
            @ozone = 2 #ozonator
          end

          #Maximum recommended chemical amounts to be added per 10,000 gallons of pool water at any one time:
          #Sodium bicarbonate: 10 lbs.
          #Sodium carbonate (soda ash): 4 lbs.
          #Muriatic acid: 16 fl. oz.
          #Sodium bisulfate: 20 oz.

          # Alkalinity Adjustment
          @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
          @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
          @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
          @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
          @Calc_CPO_NaHCO3=0

          if @SWG==1
            @TA_tar=(@base_maxTA+@base_minTA)/2 #Calculates target for the total alkalinity average of ideal range
            if @TA_i<=@base_minTA
              @delta_TA=@TA_tar-@TA_i
              @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA
            else
              @delta_TA = 0
              #return @values
            end
          elsif @SWG==2
            @TA_tar=(@acid_maxTA+@acid_minTA)/2 #Calculates target for the total alkalinity average of ideal range
            if @TA_i<=@acid_minTA
              @delta_TA=@TA_tar-@TA_i
              @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
            elsif @TA_i>=@acid_maxTA+20
              @delta_TA = 0
              puts 'Watch TA, should lower over time using tablets.'
              #return @values
            else
              @delta_TA = 0
              #return @values
            end
          else
            @delta_TA=@TA_tar-@TA_i
          end

          #pH Adjustment
          @Calc_CPO_Na2CO3=0
          @Calc_CPO_NaHSO4=0
          @Calc_CPO_HCl=0
          @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
          @max_NaHSO4=20/16 #Maximum recommended sodium bisulfate single addition per 10,000 gal
          @max_HCl=16/128 #Maximum recommended muriatic acid single addition per 10,000 gal

          if @pH_i<7.0
            @pH_i=7.0
            #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n')
          elsif @pH_i>8.0
            @pH_i=8.0
            #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n')
          end

          #Base
          if (@pH_i<7.4 && @pH_i>=7.2)#7.2<=@pH_i<7.4
            @CPO_Na2CO3=0.375
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)
          elsif (@pH_i<7.2 && @pH_i>=7.0)#7.0<=@pH_i<7.2
            @CPO_Na2CO3=0.5
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)

            #Acid
          elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==1)#7.6<=@pH_i<=7.8 and sodium bisulfite
            @CPO_NaHSO4=0.94
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)
          elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==1)#7.8<@pH_i<=8.0 and sodium bisulfite
            @CPO_NaHSO4=1.25
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)#7.6<=@pH_i<=7.8 and muriatic acid
          elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==2)
            @CPO_HCl=0.094
            @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
          elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==2)#7.8<@pH_i<=8.0 and muriatic acid
            @CPO_HCl=0.125
            @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
          end

          #Adjusting TA adjustment from pH adjustment
          if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
            @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
            @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@pool_vol_gal/10000.0)) #TA increase from pH increase
            #Rails.logger.info("LOGS TRUE PRESET"+@delta_TA.to_s)
            #Rails.logger.info("LOGS TRUE PRESET"+@TAppm_Na2CO3.to_s)
            @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
          else
            @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
          end

          #Sanitizer

          if @sanitizer == 'Chlorine' #Check for chlorine
            #puts "Primary Sanitizer = 1"
            @minFC=2 # Minimum ideal FC ppm
            @maxFC=4#Maximum ideal FC ppm
            @FC_tar=(@maxFC+@minFC)/2#Target FC
            @delta_FC=@FC_tar-@FC_i
            # puts "DELTA = "+@delta_FC.to_s
            if @delta_FC>0 #Check if sanitizer above target
              #puts "INSIDE DELTA CHECK"
              if @SWG==2 #No chlorine generator => manual chlorine
                if @pool.dichlor
                  @Cl_type= 1
                elsif @pool.dichlor62
                  @Cl_type= 2
                elsif @pool.trichlor3
                  @Cl_type= 3
                elsif @pool.bleach6
                  @Cl_type= 5
                elsif @pool.bleach12
                  @Cl_type= 4
                elsif @pool.calhypo53
                  @Cl_type= 0
                else
                  @Cl_type = 0
                end
                # puts "CHEM TYPE = "+@Cl_type.to_s
                #mess @Cl_type= input ('\nSelect Chlorine type by number: \n1:Dichlor 56# ACC\n2:Dichlor 62# ACC\n3:Trichlor\n4:12# Liquid Chlorine\n5:6# Liquid Chlorine (Household Bleach)\nSelection: ');
                if @Cl_type==1
                  @Cl_name='Dichlor 56#'
                  @Cl_nametemp=('dichlor_56')
                  @CPO_Cl=0.15 #Dichlor 56# ACC CPO factor
                elsif @Cl_type==2
                  @Cl_name='Dichlor 62#'
                  @Cl_nametemp=('dichlor_62')
                  @CPO_Cl=0.131 #Dichlor 62# ACC CPO factor
                elsif @Cl_type==3
                  @Cl_name='3" Trichlor Tablet(s)'
                  @Cl_nametemp=('trichlor')
                  @CPO_Cl=0.09375 #Trichlor CPO factor
                elsif @Cl_type==4
                  @Cl_name='12# Liquid Chlorine'
                  @Cl_nametemp=('lch12')
                  @CPO_Cl=0.0836 #12# Liquid Chlorine
                elsif @Cl_type==5
                  @Cl_name='6# Liquid Chlorine (Household Bleach)'
                  @Cl_nametemp=('lch6')
                  @CPO_Cl=0.0418 #Approximation 6# Liquid Chlorine
                elsif @Cl_type==0
                  @Cl_name='Calcium Hypochlorite'
                  @Cl_nametemp=('chyp53')
                  @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                else
                  @CPO_Cl=0
                end


                # puts "Chem Name = "+@Cl_name
                # puts "CPO_Cl = "+@CPO_Cl.to_s
                @Calc_CPO_Cl=@CPO_Cl*(@pool_vol_gal/10000.0)*@delta_FC
                # puts "Calc CPO Cl = "+@Calc_CPO_Cl.to_s
                if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                  #puts "@Cl_type<=5 && @Cl_type>=4"
                  if @Calc_CPO_Cl>=0.25 #Check for gallons
                    #puts "@Calc_CPO_Cl>=0.25"
                    @wholegal_Cl=(@Calc_CPO_Cl).floor  #Number of whole gallons of chlorine
                    @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                    if @quartergal_Cl==4
                      @wholegal_Cl=1
                      @quartergal_Cl=@quartergal_Cl-4
                    end
                    #mess fprintf('\nAdd #g gallon(s) and #g quarter gallon(s) of #s to the #s evenly.\n\n', @wholegal_Cl, @quartergal_Cl, @Cl_name, water_type)
                    # puts @wholegal_Cl.to_s
                    # puts @quartergal_Cl.to_s
                    @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                    #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625"
                    @wholecup_Cl=((@Calc_CPO_Cl)/16).floor #Number of whole cups of chlorine
                    @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.0625).round #Number of quarter cups of chlorine
                    #mess fprintf('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s evenly.\n\n', @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                    #puts @wholecup_Cl.to_s
                    #puts @quartercup_Cl.to_s
                    @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                  elsif @Calc_CPO_Cl<0.0625
                    #puts "@Calc_CPO_Cl<0.0625"
                    @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                    #mess fprintf('\nAdd #g tablespoon(s) of #s to the #s evenly.\n\n', @wholetbs_Cl, @Cl_name, water_type)
                    #puts @wholetbs_Cl.to_s
                    @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                  end
                elsif @Calc_CPO_Cl>=0.25 && @Cl_type<3 #Check for less than 1 pound granular
                  #puts "@Calc_CPO_Cl>=0.25 && @Cl_type<3"
                  @wholecup_Cl=(@Calc_CPO_Cl).floor #Assumes 1 cup = 1 lb
                  @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                  #mess fprintf ('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s by spreading granules evenly.\n\n' , @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                  #puts @wholecup_Cl.to_s
                  #puts @quartercup_Cl.to_s
                  @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3 #Check for greater than zero
                  #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3"
                  @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert to ounces
                  @tbs_Cl=(@ozCalc_CPO_Cl*2).round #Convert to tbs
                  #mess fprintf ('\nAdd #g tablespoon(s) of #s to the #s by spreading granules evenly.\n\n' , @tbs_Cl, @Cl_name, water_type)
                  #puts @tbs_Cl.to_s
                  @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                end
              elsif @SWG==1 #Salt water chlorine generator and low chlorine
                #puts "@SWG==1"
                #mess fprintf ('Chlorine is low.\nSalt water chlorinator needs to be checked. Supplemental chlorine can be used.\n\n')
              elsif @Cl_type==3
                #puts "@Cl_type==3"
                @wgt_trichlortab= 0.7 #Assuming trichlor 3" tablet is 0.7 oz
                @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert lbs to ounces
                @wholetab_Cl=ceil(@ozCalc_CPO_Cl/@wgt_trichlortab)
                #mess fprintf('Add #g #s to the floater(s) in the #s incrementally if necessary.\n\n', @wholetab_Cl, @Cl_name, water_type)
                #puts @wholetab_Cl.to_s
                @values.store(@Cl_nametemp, @wholetab_Cl.to_s+";tbs")
              end
            elsif @delta_FC<=0 #Check if chlorine is at or above target
              #mess fprintf ('\nChlorine is within range.\n\n')
            end
          end

          if (@daysdiff.nil? == false && @daysdiff > 3) || (@TA_i.nil? ==false && @TA_i.to_f <=0)
            @adj_Calc_CPO_NaHCO3=0
          end
          #puts @adj_Calc_CPO_NaHCO3
          #Adjusted TA Dose Report
          if (@adj_Calc_CPO_NaHCO3 >= 0.25 && @adj_Calc_CPO_NaHCO3<=(@pool_vol_gal/10000.0)*@max_NaHCO3) #Check for pounds and maximum per 10,000 gallons
            @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
            @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjustment.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
            @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
          elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<=0.25 && @adj_Calc_CPO_NaHCO3<(@pool_vol_gal/10000)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
            @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
            @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
            #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjusments.\n\n', @tbs_NaHCO3, @TA_tar)
            @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
          else
            if @adj_Calc_CPO_NaHCO3>=0.25
              @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
              @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
              @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
            elsif (@adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
              @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
              @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @tbs_NaHCO3, @TA_tar)
              @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
            end
          end

          #pH Dose Report
          if (@Calc_CPO_Na2CO3>=0.25 && @Calc_CPO_Na2CO3<=@max_Na2CO3*@pool_vol_gal/10000.0) #Check for pounds and maximum per 10,000 gallons
            @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
            @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
            #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium carbonate to increase pH \n', @wholecup_Na2CO3, @quartercup_Na2CO3)
            @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
          elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0 && @Calc_CPO_Na2CO3<=@max_Na2CO3*@pool_vol_gal/10000.0) #Check for oz. and maximum per 10,000 gallons
            @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
            @tbs_Na2CO3=(@Calc_CPO_Na2CO3*2).round #Converts oz to tbs
            #mess fprintf('Add #g tablespoon(s) of Sodium carbonate to increase pH \n', @tbs_Na2CO3)
            @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
          else #greater than maximum dose per 10,000 gallons
            if @Calc_CPO_Na2CO3>=0.25
              @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
              @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium carbonate INCREMENTALLY to increase pH \n', @wholecup_Na2CO3, @quartercup_Na2CO3)
              @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
            elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0)
              @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
              @tbs_Na2CO3=(Calc_CPO_CPO_Na2CO3*2).round #Converts oz to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium carbonate INCREMENTALLY to increase pH \n', @tbs_Na2CO3)
              @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
            end
          end

          if @acid_type==1
            if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
              @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #Assumes 1 cup = 1 lb
              @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
              @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
            elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000) #Check for ounces and max dose per 10,000 gallons
              @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
              @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite to lower pH \n', @tbs_NaHSO4)
              @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
            else #greater than maximum dose per 10,000 gallons
              if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
                @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #assumes 1 cup = 1 lb
                @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
                #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
                @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
              elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
                @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
                @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
                #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @tbs_NaHSO4)
                @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
              end
            end
          elsif @acid_type==2
            if (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
              @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
              @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
              @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
              #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
            elsif (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
              @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
              @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
              @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
              #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid INCREMENTALLY in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
            elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for cups and maximum dose per 10,000 gallons
              @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
              @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
              #mess fprintf('Spread #g cup(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
            elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0)
              @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
              @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
              #mess fprintf('Spread #g cup(s) INCREMENTALLY of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
              @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
            end
          end

          # puts @values
          # puts @messages
          # return generatedosagetextforbeta(@values)
          # return TextGenerator.new.dosage_text_for_beta(@values, @pool)
        end
        puts @values
        return { values: @values, messages: @messages }
        puts "==================================Pool Dosage ends================================="
      else
        # AUTO_PRESCRIPTION is not enabled
        return { values: @values, messages: @messages }
      end
    end

    def _spa_dosage(ph, orp, ta, pool = nil)
      puts "==================================Spa Dosage====================================="
      @values = {}
      @pool = pool || Pool.new
      @auto_pres = true
      if @auto_pres
        if @pool.isPool == false
          @messages = []
          @water_type = "spa"
          if @pool.volume.nil? == false
            @spa_vol_gal = @pool.volume
          else
            @spa_vol_gal = 0
          end
          if @pool.spabox
            @spa_box = true
          else
            @spa_box = false
          end # if spa box check

          @orp = 0
          @ph = 0
          @free_cl = 0
          @ta_i = 0
          @daysdiff = 0

          #Fetch manual readins of the customer
          @mreading = @pool.mtests.last

          if ph.nil? == false
            @ph = ph
            if @ph.to_f < 7
              @ph = 7
            end
            if @ph.to_f > 8
              @ph = 8
            end
          end

          if orp.nil? == false
            @free_cl = orp
          end

          if ta.nil? == false
            @ta_i = ta
          end

          @sanitizer = @pool.waterChemistry



          #Testing Done

          if @pool.waterChemistry == "Salt Water"
            @swg = 1 #salt water chlorinator
          else
            @swg = 2 #salt water chlorinator
          end

          #Decide Primary sanitizer
          #if @sanitizer == "Bromine"
          if @daysdiff.nil? == false && @daysdiff <= 3 && @sanitizer == "Bromine"
            @br_i = 2*(40000-Math.sqrt(6840000000-9600000*(@orp.to_f)))/5000.0
            #if @br_i.to_f < 0
            #	@br_i = 0
            #end

            @orp_i = @orp.to_f
            @primary_sanitizer = 1
          elsif @sanitizer != "Bromine" && @sanitizer != "Other" && @swg == 2
            @fc_i = (40000-Math.sqrt(6840000000-9600000*(@orp.to_f)))/5000.0
            #if @fc_i.to_f < 0
            #	@fc_i = 0
            #end
            @orp_i = @orp.to_f
            @primary_sanitizer = 2
          else
            @fc_i = 0
            @orp_i = @orp.to_f
            @primary_sanitizer = 3
          end

          if @sanitizer == "Bromine" && @daysdiff < 3
            @br_i = @free_cl
          elsif @sanitizer != "Bromine" && @sanitizer != "Other" && @swg == 2 && @daysdiff < 3
            @fc_i = @free_cl
          end

          if @pool.ozonator
            @ozone = 1 #ozonator
          else
            @ozone = 2 #ozonator
          end

          # Alkalinity Adjustment
          @d_NaHCO3=2.2.*1000 #Density sodium bicarbonate (mg/cm3)
          @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
          @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
          @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
          @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
          @Calc_CPO_NaHCO3=0

          if @swg == 1
            @ta_tar = (@base_maxTA+@base_minTA)/2 #Calculates target for the total alkalinity average of ideal range
            if @ta_i <= @base_minTA
              @delta_TA=@ta_tar-@ta_i
              @Calc_CPO_NaHCO3 = @CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA
            end
          elsif @swg==2
            @ta_tar=(@acid_maxTA+@acid_minTA)/2 #Calculates target for the total alkalinity average of ideal range
            @delta_TA=@ta_tar-@ta_i
            if @ta_i<=@acid_minTA
              @delta_TA=@ta_tar-@ta_i
              @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
            elsif @ta_i>=@acid_maxTA+20
              @messages << "Watch TA, should lower over time using tablets."
            end
          else
            @delta_TA=@ta_tar-@ta_i;
          end

          #pH Adjustment
          @Calc_CPO_Na2CO3=0
          @Calc_CPO_NaHSO4=0
          @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
          @max_NaHSO4=20/16.0 #Maximum recommended sodium bisulfate single addition per 10,000 gal

          if @ph<7.0
            @ph=7.0
            @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n"
          elsif @ph>8.0
            @ph=8.0
            @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n"
          end
          # puts @ph
          #Base
          if (@ph<7.4 && @ph>=7.2)#7.2<=@ph<7.4
            @CPO_Na2CO3=0.375
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
          elsif (@ph<7.2 && @ph>=7.0)#7.0<=@ph<7.2
            @CPO_Na2CO3=0.5
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
            #Acid
          elsif (@ph<=7.8 && @ph>=7.6)#7.6<=@ph<=7.8
            @CPO_NaHSO4=0.94
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
          elsif (@ph<=8.0 && @ph>7.8)#7.8<@ph<=8.0
            @CPO_NaHSO4=1.25
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
          end

          #Adjusting TA adjustment from pH adjustment
          if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
            @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
            @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@spa_vol_gal/10000.0)) #TA increase from pH increase
            @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
          else
            @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
          end

          if @primary_sanitizer==3 #Check for ORP input with chlorine
            @primary_sanitizer=2
          end

          if @primary_sanitizer==4 #Check for ORP input with bromine
            @primary_sanitizer=1
          end

          if @sanitizer == 'Chlorine' #Check for chlorine
            @minFC=2 # Minimum ideal FC ppm
            @maxFC=4 #Maximum ideal FC ppm
            @FC_tar=(@maxFC+@minFC)/2#Target FC
            @delta_FC=@FC_tar-@fc_i
            if @delta_FC>0 #Check if sanitizer above target
              if @swg==2 #No chlorine generator => manual chlorine
                if @pool.dichlor || @pool.esanitizer.to_f > 0
                  @Cl_type= 1 #input ('\nSelect Chlorine type by number: \n1:Dichlor 56% ACC\n2:Dichlor 62% ACC\n3:Trichlor\n4:12% Liquid Chlorine\n5:6% Liquid Chlorine (Household Bleach)\nSelection: ');
                elsif @pool.dichlor62
                  @Cl_type= 2
                elsif @pool.trichlor3
                  @Cl_type= 3
                elsif @pool.bleach6
                  @Cl_type= 5
                elsif @pool.bleach12
                  @Cl_type= 4
                elsif @pool.calhypo53
                  @Cl_type= 0
                else
                  @Cl_type= 0
                end

                if @Cl_type==1
                  @Cl_name=('Dichlor 56%')
                  @Cl_nametemp=('dichlor_56')
                  @CPO_Cl=0.15 #Dichlor 56% ACC CPO factor
                elsif @Cl_type==2
                  @Cl_name=('Dichlor 62%')
                  @Cl_nametemp=('dichlor_62')
                  @CPO_Cl=0.131 #Dichlor 62% ACC CPO factor
                elsif @Cl_type==3
                  @Cl_name=('Trichlor')
                  @Cl_nametemp=('trichlor')
                  @CPO_Cl=0.09375 #Trichlor CPO factor
                elsif @Cl_type==4
                  @Cl_name=('12% Liquid Chlorine')
                  @Cl_nametemp=('lch12')
                  @CPO_Cl=0.0836 #12% Liquid Chlorine
                elsif @Cl_type==5
                  @Cl_name=('6% Liquid Chlorine (Household Bleach)')
                  @Cl_nametemp=('lch6')
                  @CPO_Cl=0.0418 #Approximation 6% Liquid Chlorine
                elsif @Cl_type==0
                  @Cl_name='Calcium Hypochlorite'
                  @Cl_nametemp=('chyp53')
                  @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                else
                  @CPO_Cl=0
                end

                #puts "TYPE : "+@Cl_type.to_s
                #puts "NAME : "+@Cl_name
                @Calc_CPO_Cl=@CPO_Cl*@spa_vol_gal/10000.0*@delta_FC
                if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                  if @Calc_CPO_Cl>=0.25 #Check for gallons
                    @wholegal_Cl=@Calc_CPO_Cl.floor  #Number of whole gallons of chlorine
                    @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                    @messages << "\nAdd "+ @wholegal_Cl +" gallon(s) and "+ @quartergal_Cl+" quarter gallon(s) of "+ @Cl_name +" to the "+ @water_type+" evenly.\n\n"
                    @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                    @wholecup_Cl=(@Calc_CPO_Cl/16).floor #Number of whole cups of chlorine
                    @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.0625).round #Number of quarter cups of chlorine
                    @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                    @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                  elsif @Calc_CPO_Cl<0.0625
                    @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                    @messages << "\nAdd "+@wholetbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                    @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                  end
                elsif @Calc_CPO_Cl>=0.25 && @spa_box==false #Check for less than 1 pound and does not use spa box
                  @wholecup_Cl=@Calc_CPO_Cl.floor #Assumes 1 cup = 1 lb
                  @quartercup_Cl=((@Calc_CPO_Cl-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                  @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\nor by filling tab dispensers.\n\n"
                  @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @primary_sanitizer==2 #Check for greater than zero and does not use spa box
                  @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert to ounces
                  @tbs_Cl=(@ozCalc_CPO_Cl*2).ceil #Convert to tbs
                  if @spa_box==true #Uses spa box
                    @quarteroz_Cl=@ozCalc_CPO_Cl/0.25 #Total number of 0.25oz chlorine bags
                    @bags_Cl=@quarteroz_Cl.round #Number of whole 0.25oz chlorine packets
                    @messages << "\nAdd "+@bags_Cl.to_s+", 0.25 oz package(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                    @values.store(@Cl_nametemp, @bags_Cl.to_s+";pac")
                  elsif @spa_box==false #Does not use spa box
                    @messages << "\nAdd "+@tbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                    @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                  end
                elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @primary_sanitizer==1 #Check for greater than zero
                  @messages << "\nAdd 2 bromine tablets in tab dispensers.\n\n"
                  @values.store("bromine", "2")
                end
              elsif @swg==1 #Salt water chlorine generator and low chlorine
                @messages << "Chlorine is low.\nSalt water chlorinator, circulation, and salt concentration need to be checked.\n\n"
              end
            elsif @delta_FC<=0 #Check if chlorine is at or above target
              @messages << "\nChlorine is within range, re-fill tablets if needed.\n\n"
            end
          elsif @primary_sanitizer==1 #Bromine system
            @minBr=4 #Minimum ideal Br ppm
            @maxBr=6 #Maximum ideal Br ppm
            @Br_tar=(@maxBr+@minBr)/2 #Target Br
            @delta_Br=@Br_tar-@br_i #Difference of bromine target and initial concentration
            if BigDecimal(@delta_Br.to_s) > 0
              @messages << "Bromine low, refill Bromine tablets.\n\n"
            else
              @messages << "Bromine in range, check tablets.\n\n"
            end
          end
          if (@daysdiff.nil? == false && @daysdiff > 3) || (@ta_i.nil? == false && @ta_i.to_f <=0)
            @adj_Calc_CPO_NaHCO3=0
          end

          #Adjusted TA Dose Report #spaboxcondition
          if (@adj_Calc_CPO_NaHCO3>=0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3 && @spa_box == false) #Check for pounds and maximum per 10,000 gallons
            @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
            @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
            @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
          elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
            @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
            @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
            if @spa_box==true #Uses spa box
              @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
              @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
              @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\nThis will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjusments. \nThis will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
            end
          else
            if @adj_Calc_CPO_NaHCO3>=0.25 && @spa_box == false
              @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
              @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
            elsif (@adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
              @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
              @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
              if @spa_box==true #Uses spa box
                @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
                @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
                @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
              end
            end
          end

          #pH Dose Report
          if (@Calc_CPO_Na2CO3>=0.25 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for pounds and maximum per 10,000 gallons
            @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
            @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate to increase pH \n"
            @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
          elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for oz. and maximum per 10,000 gallons
            @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
            @tbs_Na2CO3=(@Calc_CPO_Na2CO3*2).round #Converts oz to tbs
            if @spa_box==true #Uses spa box
              @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
              @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
              @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate to increase pH \n"
              @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate to increase pH \n"
              @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
            end
          else #greater than maximum dose per 10,000 gallons
            if @Calc_CPO_Na2CO3>=0.25
              @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
              @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
              @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
            elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0)
              @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
              @tbs_Na2CO3=(@Calc_CPO_CPO_Na2CO3*2).round #Converts oz to tbs
              if @spa_box==true #Uses spa box
                @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
                @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
                @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
              end
            end
          end
          #puts @Calc_CPO_NaHSO4
          if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
            @wholecup_NaHSO4=(@Calc_CPO_NaHSO4/0.574).floor #Assumes 1 cup = 0.574 lb
            @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
            @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite to lower pH \n"
            @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
          elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000) #Check for ounces and max dose per 10,000 gallons
            @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16)/0.574 #Convert to ounces
            @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
            if @spa_box==true #Uses spa box
              @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
              @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
              @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite to lower pH \n"
              @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
            elsif @spa_box==false #Does not use spa box
              @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite to lower pH \n"
              @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
            end
          else #greater than maximum dose per 10,000 gallons
            if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
              @wholecup_NaHSO4=(@Calc_CPO_NaHSO4/0.574).floor #assumes 1 cup = 0.574 lb
              @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
              @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
            elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
              @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16)/0.574 #Convert to ounces
              @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
              if @spa_box==true #Uses spa box
                @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
                @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
                @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
              end
            end
          end

          # puts @values
          # puts @messages
        end #if spa check
        puts @values
        puts "==================================Spa Dosage ends================================="
        # return TextGenerator.new.dosage_text_for_beta(@values, @pool)
        # return generatedosagetextforbeta(@values)
        return { values: @values, messages: @messages }
      else
        # AUTO_PRESCRIPTION is not enabled
        return { values: @values, messages: @messages }
      end
    end

    ############### NEW ###############
    def pool_dosage(pool)
      puts "==================================Pool Dosage====================================="
      @values = {}
      @pool = pool
      @role_name = 'ANY'
      @auto_pres = true
      if @auto_pres
        if @pool.isPool
          #Inputs
          water_type="pool"

          #Get pool volume
          if @pool.volume.blank? == false
            @pool_vol_gal= @pool.volume #Gallons of pool water
          else
            @pool_vol_gal= 0
          end

          @orp = 0
          @ph = 0
          @ta_i_val = ""

          @sanitizer = @pool.waterChemistry

          #Fetch manual readings
          @mreading = @pool.mtests.last

          @reading_type = ""
          if @mreading.blank? == false
            @reading_type = "manual"
            if @mreading.pH.blank? == false
              @ph = @mreading.pH
            end
            if @mreading.freeCl.blank? == false
              @free_cl = @mreading.freeCl.to_d
            end
            if @mreading.totalAlkalinity.blank? == false
              @ta_i_val = @mreading.totalAlkalinity.to_d
            end
            @daysdiff = ((Time.now - @mreading.created_at)/1.day).round
          end
          if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
            @reading_type = "device"
            unless @pool.epools.blank?
              @pool.epools.each do |epool|
                @orp = epool.oOrp
                unless epool.points.last.blank?  || epool.nil?
                  @ph = ph(epool)
                  @orp = orp(epool)
                  @daysdiff = ((Time.now - epool.points.last.created_at)/1.day).round
                  #Rails.logger.info("DAYSDIFF : "+@daysdiff.to_s)
                end # if points check
              end # looping of devices
            end # if pool has device check
          end

          if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
            Rails.logger.info("Returning as readings are greater than 3 days")
            return ""
          end

          #decide range of the system and what to be prescribed
          @range = "notgood"
          @phrange = "notgood"
          @chlrange = "notgood"
          @tarange = ""


          if @role_name == "Lora"
            #ph decider
            if @ph.to_d >= 7.2 && @ph.to_d <= 7.8
              @phrange = "good"
              Rails.logger.info("PH RANGE GOOD")
            end
            #chlorine decider
            if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 600 && @orp.to_d <= 900))
              @chlrange = "good"
              Rails.logger.info("CH RANGE GOOD")
            end
            #ta decider
            if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
              @tarange = "good"
              Rails.logger.info("TA RANGE GOOD")
            elsif @ta_i_val.blank? == false
              @tarange = "notgood"
              Rails.logger.info("TA RANGE NOT GOOD")
            end
          else
            #ph decider
            if @ph.to_d >= 7.4 && @ph.to_d <= 7.6
              @phrange = "good"
              Rails.logger.info("PH RANGE GOOD")
            end
            #chlorine decider
            if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 650 && @orp.to_d <= 900))
              @chlrange = "good"
              Rails.logger.info("CH RANGE GOOD")
            end
            #ta decider
            if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
              @tarange = "good"
              Rails.logger.info("TA RANGE GOOD")
            elsif @ta_i_val.blank? == false
              @tarange = "notgood"
              Rails.logger.info("TA RANGE NOT GOOD")
            end
          end

          #range decider ends

          if @phrange == "good" && @chlrange == "good" && (@tarange == "good" || @tarange == "")
            deletealldosages(@pool)
            Rails.logger.info("Returning as system is in good range")
            return ""
          end

          @can_populate = false

          case @reading_type
            when "manual"
              Rails.logger.info("Manual Reading")
              #if @sanitizer != "Chlorine"
              Rails.logger.info("NOT CHLORINE")
              @FC_i = @free_cl
              @can_populate = true
            #end
            when "device"
              Rails.logger.info("Device Reading")
              #if @sanitizer != "Chlorine"
              Rails.logger.info("NOT CHLORINE")
              @FC_i = (40000-CMath.sqrt(6840000000-9600000*(@orp.to_i)))/5000.0
              @FC_i = @FC_i.real
              if @FC_i < 0
                @FC_i = 0
              end
              @can_populate = true
            #end
          end
          #puts @ph.to_s
          #puts @FC_i.to_s
          #if @ph.to_d <=0 && @FC_i.to_d <= 0
          if @can_populate == false
            #puts @ph.to_s
            #puts @FC_i.to_s
            Rails.logger.info("Returning as readings are not valid")
            return ""
          end

          @pH_i= @ph #Initial pH from 0 to 14

          @acid_type= 0 #Acid type
          if @pool.dryacid.blank? == false && @pool.dryacid && @pool.dryacidSize.blank? == false && @pool.dryacidSize.to_d > 0
            @acid_type = 1
          elsif @pool.muriatic31.blank? == false && @pool.muriatic31 && @pool.muriatic31Size.blank? == false && @pool.muriatic31Size.to_d > 0
            @acid_type = 2
          end

          #check if no chemical is available and use last used acid
          if @acid_type == 0
            Rails.logger.info("Acid name empty.. Going to use last used Acid")
            @lastused = Tester::FakeTextGenerator.new.getlastusedacid(@pool)
            @acid_type = @lastused
            Rails.logger.info("Acid name empty.. Going to use last used Acid : "+@acid_type.to_s)
          end

          @TA_i= @ta_i_val.to_d #input('What is the Total Alkalinity (ppm)?: '); #Initial Total Alkalinity (ppm)
          @CYA_i=@cya_i_val#input('What is the Cyanuric Acid level (ppm)?: '); #Initial Cyanuric acid level (ppm)
          if @pool.waterChemistry == "Salt Water"
            @sanitizer = 'Chlorine'
            @SWG = 1 #salt water chlorinator
            #puts "SWG = 1"
          else
            @SWG = 2 #salt water chlorinator
            #puts "SWG = 1"
          end

          if @pool.ozonator
            @ozone = 1 #ozonator
          else
            @ozone = 2 #ozonator
          end

          #Maximum recommended chemical amounts to be added per 10,000 gallons of pool water at any one time:
          #Sodium bicarbonate: 10 lbs.
          #Sodium carbonate (soda ash): 4 lbs.
          #Muriatic acid: 16 fl. oz.
          #Sodium bisulfate: 20 oz.

          # Alkalinity Adjustment
          @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
          @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
          @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
          @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
          @Calc_CPO_NaHCO3=0

          if @SWG==1
            @TA_tar=(@base_maxTA+@base_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
            if @TA_i<=@base_minTA
              @delta_TA=@TA_tar-@TA_i
              @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA
            else
              @delta_TA = 0
              #return @values
            end
          elsif @SWG==2
            @TA_tar=(@acid_maxTA+@acid_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
            if @TA_i<=@acid_minTA
              @delta_TA=@TA_tar-@TA_i
              @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
            elsif @TA_i>=@acid_maxTA+20
              @delta_TA = 0
              puts 'Watch TA, should lower over time using tablets.'
              #return @values
            else
              @delta_TA = 0
              #return @values
            end
          else
            @delta_TA=@TA_tar-@TA_i
          end

          #pH Adjustment
          @Calc_CPO_Na2CO3=0
          @Calc_CPO_NaHSO4=0
          @Calc_CPO_HCl=0
          @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
          @max_NaHSO4=20/16 #Maximum recommended sodium bisulfate single addition per 10,000 gal
          @max_HCl=16/128 #Maximum recommended muriatic acid single addition per 10,000 gal

          if @pH_i<7.0
            @pH_i=7.0
            #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n')
          elsif @pH_i>8.0
            @pH_i=8.0
            #mess fprintf ('pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n')
          end

          #Base
          if (@pH_i<7.4 && @pH_i>=7.2)#7.2<=@pH_i<7.4
            @CPO_Na2CO3=0.375
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)
          elsif (@pH_i<7.2 && @pH_i>=7.0)#7.0<=@pH_i<7.2
            @CPO_Na2CO3=0.5
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@pool_vol_gal/10000.0)

            #Acid
          elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==1)#7.6<=@pH_i<=7.8 and sodium bisulfite
            @CPO_NaHSO4=0.94
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)
          elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==1)#7.8<@pH_i<=8.0 and sodium bisulfite
            @CPO_NaHSO4=1.25
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@pool_vol_gal/10000.0)#7.6<=@pH_i<=7.8 and muriatic acid
          elsif (@pH_i<=7.8 && @pH_i>=7.6 && @acid_type==2)
            @CPO_HCl=0.094
            @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
          elsif (@pH_i<=8.0 && @pH_i>7.8 && @acid_type==2)#7.8<@pH_i<=8.0 and muriatic acid
            @CPO_HCl=0.125
            @Calc_CPO_HCl=@CPO_HCl*(@pool_vol_gal/10000.0)
          end

          #Adjusting TA adjustment from pH adjustment
          if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
            @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
            @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@pool_vol_gal/10000.0)) #TA increase from pH increase
            #Rails.logger.info("LOGS TRUE PRESET"+@delta_TA.to_s)
            #Rails.logger.info("LOGS TRUE PRESET"+@TAppm_Na2CO3.to_s)
            @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@pool_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
          else
            @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
          end

          #Sanitizer

          #check for chlorine range before prescribing sanitizer
          if @chlrange == "notgood"
            if @sanitizer == 'Chlorine' #Check for chlorine
              #puts "Primary Sanitizer = 1"
              @minFC=3 # Minimum ideal FC ppm
              @maxFC=4#Maximum ideal FC ppm
              @FC_tar=(@maxFC+@minFC)/2.0#Target FC

              if @FC_i.blank? == false
                @delta_FC=@FC_tar-@FC_i
              else
                @delta_FC=0
              end
              #puts "DELTA = "+@delta_FC.to_s
              if @delta_FC>0 #Check if sanitizer above target
                #puts "INSIDE DELTA CHECK"
                @divideunit = 1
                if @pool.bleach6 && @pool.bleach6Size.blank? == false && @pool.bleach6Size.to_d > 0
                  @Cl_type= 5
                elsif @pool.bleach12 && @pool.bleach12Size.blank? == false && @pool.bleach12Size.to_d > 0
                  @Cl_type= 4
                elsif (@pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0) || (@pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0) || (@pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0)
                  @Cl_type= 0
                elsif @pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d > 0
                  @Cl_type= 1
                  @divideunit = 0.5375
                elsif @pool.dichlor62 && @pool.dichlor62Size.blank? == false && @pool.dichlor62Size.to_d > 0
                  @Cl_type= 2
                  @divideunit = 0.5375
                elsif @pool.trichlor3 && @pool.trichlor3Size.blank? == false && @pool.trichlor3Size.to_d > 0
                  @Cl_type= 3
                else
                  @Cl_type = 0
                end
                #puts "CHEM TYPE = "+@Cl_type.to_s
                #mess @Cl_type= input ('\nSelect Chlorine type by number: \n1:Dichlor 56# ACC\n2:Dichlor 62# ACC\n3:Trichlor\n4:12# Liquid Chlorine\n5:6# Liquid Chlorine (Household Bleach)\nSelection: ');
                if @Cl_type==1
                  @Cl_name='Dichlor 56#'
                  @Cl_nametemp=('dichlor_56')
                  @CPO_Cl=0.15 #Dichlor 56# ACC CPO factor
                elsif @Cl_type==2
                  @Cl_name='Dichlor 62#'
                  @Cl_nametemp=('dichlor_62')
                  @CPO_Cl=0.131 #Dichlor 62# ACC CPO factor
                elsif @Cl_type==3
                  @Cl_name='3" Trichlor Tablet(s)'
                  @Cl_nametemp=('trichlor')
                  @CPO_Cl=0.09375 #Trichlor CPO factor
                elsif @Cl_type==4
                  @Cl_name='12# Liquid Chlorine'
                  @Cl_nametemp=('lch12')
                  @CPO_Cl=0.0836 #12# Liquid Chlorine
                elsif @Cl_type==5
                  @Cl_name='6# Liquid Chlorine (Household Bleach)'
                  @Cl_nametemp=('lch6')
                  @CPO_Cl=0.1672 #Approximation 6# Liquid Chlorine
                elsif @Cl_type==0
                  @Cl_name='Calcium Hypochlorite'
                  if @pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0
                    @Cl_nametemp=('chyp53')
                  elsif @pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0
                    @Cl_nametemp=('chyp65')
                  elsif @pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0
                    @Cl_nametemp=('chyp73')
                  end
                  @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                else
                  @CPO_Cl=0
                end

                @Calc_CPO_Cl=@CPO_Cl*(@pool_vol_gal/10000.0)*@delta_FC

                if @SWG==2 || @SWG==1 #No chlorine generator => manual chlorine
                  #check if no chemical is available and use last used sanitizer
                  if @Cl_nametemp.blank?
                    Rails.logger.info("Chemical name empty.. Going to use last used sanitizer")
                    @lastused = Tester::FakeTextGenerator.new.getlastusedchlorine(@pool)
                    @Cl_type = @lastused || 0
                    Rails.logger.info("Last used sanitizer : "+@lastused.to_s)
                    if @lastused==1
                      @Cl_name='Dichlor 56#'
                      @Cl_nametemp=('dichlor_56')
                      @CPO_Cl=0.15 #Dichlor 56# ACC CPO factor
                      @divideunit = 0.5375
                    elsif @lastused==2
                      @Cl_name='Dichlor 62#'
                      @Cl_nametemp=('dichlor_62')
                      @CPO_Cl=0.131 #Dichlor 62# ACC CPO factor
                      @divideunit = 0.5375
                    elsif @lastused==3
                      @Cl_name='3" Trichlor Tablet(s)'
                      @Cl_nametemp=('trichlor')
                      @CPO_Cl=0.09375 #Trichlor CPO factor
                    elsif @lastused==4
                      @Cl_name='12# Liquid Chlorine'
                      @Cl_nametemp=('lch12')
                      @CPO_Cl=0.0836 #12# Liquid Chlorine
                    elsif @lastused==5
                      @Cl_name='6# Liquid Chlorine (Household Bleach)'
                      @Cl_nametemp=('lch6')
                      @CPO_Cl=0.1672 #Approximation 6# Liquid Chlorine
                    elsif @lastused==0
                      @Cl_name='Calcium Hypochlorite'
                      if @pool.calhypo53
                        @Cl_nametemp=('chyp53')
                      elsif @pool.calhypo65
                        @Cl_nametemp=('chyp65')
                      elsif @pool.calhypo73
                        @Cl_nametemp=('chyp73')
                      end
                      @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                    else
                      @CPO_Cl=0
                    end
                    Rails.logger.info("Chemical name empty.. Going to use last used sanitizer : "+@Cl_name)
                  end

                  #puts "Chem Name = "+@Cl_name
                  #puts "CPO_Cl = "+@CPO_Cl.to_s

                  if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                    #puts "@Cl_type<=5 && @Cl_type>=4"
                    if @Calc_CPO_Cl>=0.25 #Check for gallons
                      #puts "@Calc_CPO_Cl>=0.25"
                      @wholegal_Cl=(@Calc_CPO_Cl).floor  #Number of whole gallons of chlorine
                      @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                      if @quartergal_Cl==4
                        @wholegal_Cl=1
                        @quartergal_Cl=@quartergal_Cl-4
                      end
                      #mess fprintf('\nAdd #g gallon(s) and #g quarter gallon(s) of #s to the #s evenly.\n\n', @wholegal_Cl, @quartergal_Cl, @Cl_name, water_type)
                      #puts @wholegal_Cl.to_s
                      #puts @quartergal_Cl.to_s
                      @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                    elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                      #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625"
                      @wholecup_Cl=((@Calc_CPO_Cl)*16).floor #Number of whole cups of chlorine
                      @quartercup_Cl=(((@Calc_CPO_Cl)*16-@wholecup_Cl)/0.25).round #Number of quarter cups of chlorine
                      #mess fprintf('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s evenly.\n\n', @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                      #puts @wholecup_Cl.to_s
                      #puts @quartercup_Cl.to_s
                      @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                    else
                      #puts "@Calc_CPO_Cl<0.0625"
                      @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                      #mess fprintf('\nAdd #g tablespoon(s) of #s to the #s evenly.\n\n', @wholetbs_Cl, @Cl_name, water_type)
                      #puts @wholetbs_Cl.to_s
                      @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                    end
                  elsif @Calc_CPO_Cl>=0.25 && @Cl_type<3 #Check for less than 1 pound granular
                    #puts "@Calc_CPO_Cl>=0.25 && @Cl_type<3"
                    @wholecup_Cl=(@Calc_CPO_Cl/@divideunit).floor #Assumes 1 cup = 0.5375 lb for dichlor only
                    @quartercup_Cl=(((@Calc_CPO_Cl/@divideunit)-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                    #mess fprintf ('\nAdd #g cup(s) and #g quarter cup(s) of #s to the #s by spreading granules evenly.\n\n' , @wholecup_Cl, @quartercup_Cl, @Cl_name, water_type)
                    #puts @wholecup_Cl.to_s
                    #puts @quartercup_Cl.to_s
                    @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3 #Check for greater than zero
                    #puts "@Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @Cl_type<3"
                    @tbs_Cl=((@Calc_CPO_Cl/@divideunit)*16).round #Convert to tbs
                    #mess fprintf ('\nAdd #g tablespoon(s) of #s to the #s by spreading granules evenly.\n\n' , @tbs_Cl, @Cl_name, water_type)
                    #puts @tbs_Cl.to_s
                    @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                  elsif @Cl_type==3
                    #puts "@Cl_type==3"
                    @wgt_trichlortab= 8 #Assuming trichlor 3" tablet is 8 oz
                    @ozCalc_CPO_Cl=@Calc_CPO_Cl*16 #Convert lbs to ounces
                    @wholetab_Cl=(@ozCalc_CPO_Cl/@wgt_trichlortab).ceil
                    #mess fprintf('Add #g #s to the floater(s) in the #s incrementally if necessary.\n\n', @wholetab_Cl, @Cl_name, water_type)
                    #puts @wholetab_Cl.to_s
                    @values.store(@Cl_nametemp, @wholetab_Cl.to_s+";tbs")
                  end
                  #elsif @SWG==1 #Salt water chlorine generator and low chlorine
                  #puts "@SWG==1"
                  #mess fprintf ('Chlorine is low.\nSalt water chlorinator needs to be checked. Supplemental chlorine can be used.\n\n')
                end
              elsif @delta_FC<=0 #Check if chlorine is at or above target
                #mess fprintf ('\nChlorine is within range.\n\n')
              end
            end
          end
          #chlorine range check ends

          if (@daysdiff.blank? == false && @daysdiff > 3) || (@TA_i.blank? ==false && @TA_i.to_d <=0)
            @adj_Calc_CPO_NaHCO3=0
          end

          #check for ta range before prescribing ta
          if @tarange == "notgood"
            #puts @adj_Calc_CPO_NaHCO3
            #Adjusted TA Dose Report
            if (@adj_Calc_CPO_NaHCO3 >= 0.25 && @adj_Calc_CPO_NaHCO3<=(@pool_vol_gal/10000.0)*@max_NaHCO3) #Check for pounds and maximum per 10,000 gallons
              @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
              @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjustment.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
              @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
            elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<=0.25 && @adj_Calc_CPO_NaHCO3<(@pool_vol_gal/10000.0)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
              @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
              @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
              #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate to increase TA to #g ppm with pH adjusments.\n\n', @tbs_NaHCO3, @TA_tar)
              @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
            else
              if @adj_Calc_CPO_NaHCO3>=0.25
                @adj_wholecup_NaHCO3=(@adj_Calc_CPO_NaHCO3).floor #Assumes 1 cup = 1 lb
                @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
                #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @adj_wholecup_NaHCO3, @adj_quartercup_NaHCO3, @TA_tar)
                @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
              elsif (@adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
                @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
                @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
                #mess fprintf('Add #g tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to #g ppm with pH adjustment.\n This will also increase the pH.\n\n', @tbs_NaHCO3, @TA_tar)
                @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
              end
            end
          end
          #ta range check ends

          #check for ph range before prescribing ph
          if @phrange == "notgood"
            #pH Dose Report
            if ( @Calc_CPO_Na2CO3 > (@max_Na2CO3*@pool_vol_gal/10000.0))
              @Calc_CPO_Na2CO3 = @max_Na2CO3*@pool_vol_gal/10000.0
            end

            @cups_Na2CO3 = @Calc_CPO_Na2CO3/0.574
            puts "VL : "+@cups_Na2CO3.to_s
            @wholecup_Na2CO3 = 0

            if (@cups_Na2CO3 >= 0.25) #Check for pounds and maximum per 10,000 gallons
              if (@cups_Na2CO3 >= 0.8)
                @wholecup_Na2CO3=@cups_Na2CO3.round #Assumes 1 cup = 0.574 lb
              end
              if (@wholecup_Na2CO3 < @cups_Na2CO3)
                @quartercup_Na2CO3=((@cups_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
              end
              #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium carbonate to increase pH \n', @wholecup_Na2CO3, @quartercup_Na2CO3)
              @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
            else #less than 1/4 cup
              @Calc_CPO_Na2CO3 = @cups_Na2CO3 * 16 #Convert cups to tbs
              @tbs_Na2CO3 = (@Calc_CPO_Na2CO3).round
              #mess fprintf('Add #g tablespoon(s) of Sodium carbonate to increase pH \n', @tbs_Na2CO3)
              #if @tbs_Na2CO3 > 0
              @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
              #end
            end

            if @acid_type==1
              if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
                @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #Assumes 1 cup = 1 lb
                @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
                #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
                @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
              elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<=@max_NaHSO4*@pool_vol_gal/10000.0) #Check for ounces and max dose per 10,000 gallons
                @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
                @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
                #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite to lower pH \n', @tbs_NaHSO4)
                @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
              else #greater than maximum dose per 10,000 gallons
                if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
                  @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #assumes 1 cup = 1 lb
                  @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
                  #mess fprintf('Add #g cup(s) and #g quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @wholecup_NaHSO4, quartercup_NaHSO4)
                  @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
                elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
                  @Calc_CPO_NaHSO4=@Calc_CPO_NaHSO4*16 #Convert to ounces
                  @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
                  #mess fprintf('Add #g tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n', @tbs_NaHSO4)
                  @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
                end
              end
            elsif @acid_type==2
              if (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
                @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
                @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
                @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
                #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
                @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
              elsif (@Calc_CPO_HCl>=0.25 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0) #Check for gallons and maximum dose per 10,000 gallons
                @wholegal_HCl=(@Calc_CPO_HCl).floor #Whole gallons of HCl
                @halfgal_HCl=((@Calc_CPO_HCl-@wholegal_HCl)/0.5).floor #Half gallons of HCl
                @quartergal_HCl=((@Calc_CPO_HCl-(@halfgal_HCl/2))/0.25).floor #Quarter gallons of HCl
                #mess fprintf('Spread #g gallon(s), #g half gallon(s), and #g quarter gallon(s) of muriatic acid INCREMENTALLY in the deep end of the #s away from the side of the #s.\n', @wholegal_HCl, @halfgal_HCl, @quartergal_HCl, water_type, water_type)
                @values.store("muriatic_acid", @wholegal_HCl.to_s+"~"+(@halfgal_HCl+@quartergal_HCl).to_s+";gal")
              elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl<=@max_HCl*@pool_vol_gal/10000.0) #Check for cups and maximum dose per 10,000 gallons
                @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
                @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
                #mess fprintf('Spread #g cup(s) of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
                @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
              elsif (@Calc_CPO_HCl>=0.0625 && @Calc_CPO_HCl>@max_HCl*@pool_vol_gal/10000.0)
                @Calc_CPO_HCl=@Calc_CPO_HCl*16 #Cups of HCl
                @wholecup_HCl=(@Calc_CPO_HCl).round #Whole cup of HCl
                #mess fprintf('Spread #g cup(s) INCREMENTALLY of muriatic acid in the deep end of the #s away from the side of the #s.\n', @wholecup_HCl, water_type, water_type)
                @values.store("muriatic_acid", @wholecup_HCl.to_s+";cup")
              end
            end
          end
          #ph range check ends

          puts @values
          Rails.logger.info("DOSAGE AUTO : "+@values.inspect)
          puts "==================================Pool Dosage ends================================="
          return { values: @values, messages: @messages }
        end
      else
        # AUTO_PRESCRIPTION is not enabled
        return { values: @values, messages: @messages }
      end
    end

    def spa_dosage(pool)
      puts "==================================Spa Dosage ====================================="
      @values = {}
      @pool = pool
      @role_name = 'ANY'
      @auto_pres = true
      if @auto_pres
        if @pool.isPool == false
          @messages = []
          @water_type = "spa"
          if @pool.volume.blank? == false
            @spa_vol_gal = @pool.volume
          else
            @spa_vol_gal = 0
          end
          if @pool.spabox
            @spa_box = true
          else
            @spa_box = false
          end # if spa box check
          #@spa_box = true
          @orp = 0
          @ph = 0
          @ta_i = ""
          @sanitizer = @pool.waterChemistry
          #Fetch manual readins of the customer
          @mreading = @pool.mtests.last

          @reading_type = ""
          if @mreading.blank? == false
            @reading_type = "manual"
            if @mreading.pH.blank? == false
              @ph = @mreading.pH
            end
            if @mreading.freeCl.blank? == false
              @free_cl = @mreading.freeCl.to_d
            end
            if @mreading.totalAlkalinity.blank? == false
              @ta_i = @mreading.totalAlkalinity.to_d
            end
            @daysdiff = ((Time.now - @mreading.created_at)/1.day).round
          end
          if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
            @reading_type = "device"
            unless @pool.epools.blank?
              @pool.epools.each do |epool|
                @orp = epool.oOrp
                unless epool.points.last.blank?  || epool.nil?
                  @ph = ph(epool)
                  @orp = orp(epool)
                  @daysdiff = ((Time.now - epool.points.last.created_at)/1.day).round
                end # if points check
              end # looping of devices
            end # if pool has device check
          end

          if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
            Rails.logger.info("Returning as readings are greater than 3 days")
            return ""
          end

          #decide range of the system and what to be prescribed
          @range = "notgood"
          @phrange = "notgood"
          @chlrange = "notgood"
          @tarange = ""


          if @role_name == "Lora"
            #ph decider
            if @ph.to_d >= 7.2 && @ph.to_d <= 7.8
              @phrange = "good"
              Rails.logger.info("PH RANGE GOOD")
            end
            #chlorine decider
            if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 600 && @orp.to_d <= 900))
              @chlrange = "good"
              Rails.logger.info("CH RANGE GOOD")
            end
            #ta decider
            if @ta_i.blank? == false && @ta_i > 70 && @ta_i <= 90
              @tarange = "good"
              Rails.logger.info("TA RANGE GOOD")
            elsif @ta_i.blank? == false
              @tarange = "notgood"
              Rails.logger.info("TA RANGE NOT GOOD")
            end
          else
            #ph decider
            if @ph.to_d >= 7.4 && @ph.to_d <= 7.6
              @phrange = "good"
              Rails.logger.info("PH RANGE GOOD")
            end
            #chlorine decider
            if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 650 && @orp.to_d <= 900))
              @chlrange = "good"
              Rails.logger.info("CH RANGE GOOD")
            end
            #ta decider
            if @ta_i.blank? == false && @ta_i > 70 && @ta_i <= 90
              @tarange = "good"
              Rails.logger.info("TA RANGE GOOD")
            elsif @ta_i.blank? == false
              @tarange = "notgood"
              Rails.logger.info("TA RANGE NOT GOOD")
            end
          end

          #range decider ends

          if @phrange == "good" && @chlrange == "good" && (@tarange == "good" || @tarange == "")
            deletealldosages(@pool)
            Rails.logger.info("Returning as system is in good range")
            return ""
          end

          @can_populate = false

          case @reading_type
            when "manual"
              Rails.logger.info("Manual reading")
              if @sanitizer == "Bromine"
                Rails.logger.info("BROMINE")
                @br_i = @free_cl
                @can_populate = true
              else
                Rails.logger.info("NOT BROMINE")
                if @sanitizer != "Other"
                  Rails.logger.info("NOT OTHERS")
                  @fc_i = @free_cl
                  @can_populate = true
                end
              end
            when "device"
              puts "Device reading"
              if @sanitizer == "Bromine"
                Rails.logger.info("BROMINE")
                @br_i = 2*(40000-CMath.sqrt(6840000000-9600000*(@orp.to_d)))/5000.0
                @br_i = @br_i.real
                if @br_i < 0
                  @br_i = 0
                end
                @can_populate = true
              else
                Rails.logger.info("NOT BROMINE")
                if @sanitizer != "Other"
                  Rails.logger.info("NOT OTHERS")
                  @fc_i = (40000-CMath.sqrt(6840000000-9600000*(@orp.to_d)))/5000.0
                  @fc_i = @fc_i.real
                  if @fc_i < 0
                    @fc_i = 0
                  end
                  @can_populate = true
                end
              end
          end

          #if @ph.to_d <=0 && ((@fc_i.blank? == false && @fc_i.to_d <= 0) || (@br_i.blank? == false && @br_i.to_d <= 0))
          if @can_populate == false
            Rails.logger.info("Returning as readings are not valid")
            return ""
          end
          @ta_i = @ta_i.to_d
          if @pool.waterChemistry == "Salt Water"
            @swg = 1 #salt water chlorinator
            @sanitizer = 'Chlorine'
          else
            @swg = 2 #salt water chlorinator
          end

          if @pool.ozonator
            @ozone = 1 #ozonator
          else
            @ozone = 2 #ozonator
          end

          # Alkalinity Adjustment
          @d_NaHCO3=2.2.*1000 #Density sodium bicarbonate (mg/cm3)
          @max_NaHCO3= 10 #Max single addition recommended per 10,000 gal.
          @base_minTA=60 #Minimum ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @base_maxTA=80 #Max ideal TA ppm for Calcium hypochlorite, sodium hypochlorite, and @SWG
          @acid_minTA=80 #Minimum ideal TA ppm for dichlor, trichlor and bromine
          @acid_maxTA=100 #Maximum ideal TA ppm for dichlor, trichlor, and bromine
          @CPO_NaHCO3=0.14 #CPO conversion value for 1 ppm per 10,000 gal
          @Calc_CPO_NaHCO3=0

          if @swg == 1
            @ta_tar = (@base_maxTA+@base_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
            @delta_TA=@ta_tar-@ta_i
            if @ta_i <= @base_minTA
              @delta_TA=@ta_tar-@ta_i
              @Calc_CPO_NaHCO3 = @CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA
            end
          elsif @swg==2
            @ta_tar=(@acid_maxTA+@acid_minTA)/2.0 #Calculates target for the total alkalinity average of ideal range
            @delta_TA=@ta_tar-@ta_i
            if @ta_i<=@acid_minTA
              @delta_TA=@ta_tar-@ta_i
              @Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*@delta_TA #CPO NaHCO3 calculation
            elsif @ta_i>=@acid_maxTA+20
              @messages << "Watch TA, should lower over time using tablets."
            end
          else
            @delta_TA=@ta_tar-@ta_i;
          end

          #pH Adjustment
          @Calc_CPO_Na2CO3=0
          @Calc_CPO_NaHSO4=0
          @max_Na2CO3=4 #Maximum recommended sodium carbonate single addition per 10,000 gal
          @max_NaHSO4=20/16.0 #Maximum recommended sodium bisulfate single addition per 10,000 gal

          if @ph<7.0
            @ph=7.0
            @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=7.0 \n\n"
          elsif @ph>8.0
            @ph=8.0
            @messages << "pH is outside CPO calculation range.\nAssuming conservative model for pH=8.0 \n\n"
          end
          puts @ph
          #Base
          if (@ph<7.4 && @ph>=7.2)#7.2<=@ph<7.4
            @CPO_Na2CO3=0.375
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
          elsif (@ph<7.2 && @ph>=7.0)#7.0<=@ph<7.2
            @CPO_Na2CO3=0.5
            @Calc_CPO_Na2CO3=@CPO_Na2CO3*(@spa_vol_gal/10000.0)
            #Acid
          elsif (@ph<=7.8 && @ph>=7.6)#7.6<=@ph<=7.8
            @CPO_NaHSO4=0.94
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
          elsif (@ph<=8.0 && @ph>7.8)#7.8<@ph<=8.0
            @CPO_NaHSO4=1.25
            @Calc_CPO_NaHSO4=@CPO_NaHSO4*(@spa_vol_gal/10000.0)
          end

          #Adjusting TA adjustment from pH adjustment
          if @Calc_CPO_Na2CO3>0 #Check for added pounds of sodium carbonate
            @CPO_TA_Na2CO3=0.0875 #CPO TA increase for sodium carbonate
            @TAppm_Na2CO3=@Calc_CPO_Na2CO3/(@CPO_TA_Na2CO3*(@spa_vol_gal/10000.0)) #TA increase from pH increase
            @adj_Calc_CPO_NaHCO3=@CPO_NaHCO3*(@spa_vol_gal/10000.0)*(@delta_TA-@TAppm_Na2CO3) #Adjusted TA increase
          else
            @adj_Calc_CPO_NaHCO3=@Calc_CPO_NaHCO3 #No TA correction yet if pH is not adjusted up
          end

          #check chlorine range before prescribing sanitizer
          if @chlrange == "notgood"
            if @sanitizer == 'Chlorine' #Check for chlorine
              @minFC=3 # Minimum ideal FC ppm
              @maxFC=4 #Maximum ideal FC ppm
              @FC_tar=(@maxFC+@minFC)/2.0#Target FC
              if @fc_i.blank? == false
                @delta_FC=@FC_tar-@fc_i
              else
                @delta_FC = 0
              end

              if @delta_FC>0 #Check if sanitizer above target
                @divideunit = 1
                if @pool.bleach6 && @pool.bleach6Size.blank? == false && @pool.bleach6Size.to_d > 0
                  @Cl_type= 5
                elsif @pool.bleach12 && @pool.bleach12Size.blank? == false && @pool.bleach12Size.to_d > 0
                  @Cl_type= 4
                elsif (@pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0) || (@pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0) || (@pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0)
                  @Cl_type= 0
                elsif (@pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d > 0) || (@pool.esanitizer.blank? == false && @pool.esanitizer.to_d > 0)
                  if @pool.dichlor && @pool.dichlorSize.blank? == false && @pool.dichlorSize.to_d > 0
                    @divideunit = 0.5375
                  end
                  @Cl_type= 1 #input ('\nSelect Chlorine type by number: \n1:Dichlor 56% ACC\n2:Dichlor 62% ACC\n3:Trichlor\n4:12% Liquid Chlorine\n5:6% Liquid Chlorine (Household Bleach)\nSelection: ');
                elsif @pool.dichlor62 && @pool.dichlor62Size.blank? == false && @pool.dichlor62Size.to_d > 0
                  @Cl_type= 2
                  @divideunit = 0.5375
                elsif @pool.trichlor3 && @pool.trichlor3Size.blank? == false && @pool.trichlor3Size.to_d > 0
                  @Cl_type= 3
                else
                  @Cl_type= 0
                end

                if @Cl_type==1
                  @Cl_name=('Dichlor 56%')
                  @Cl_nametemp=('dichlor_56')
                  @CPO_Cl=0.15 #Dichlor 56% ACC CPO factor
                elsif @Cl_type==2
                  @Cl_name=('Dichlor 62%')
                  @Cl_nametemp=('dichlor_62')
                  @CPO_Cl=0.131 #Dichlor 62% ACC CPO factor
                elsif @Cl_type==3
                  @Cl_name=('Trichlor')
                  @Cl_nametemp=('trichlor')
                  @CPO_Cl=0.09375 #Trichlor CPO factor
                elsif @Cl_type==4
                  @Cl_name=('12% Liquid Chlorine')
                  @Cl_nametemp=('lch12')
                  @CPO_Cl=0.0836 #12% Liquid Chlorine
                elsif @Cl_type==5
                  @Cl_name=('6% Liquid Chlorine (Household Bleach)')
                  @Cl_nametemp=('lch6')
                  @CPO_Cl=0.1672 #Approximation 6% Liquid Chlorine
                elsif @Cl_type==0
                  @Cl_name='Calcium Hypochlorite'
                  if @pool.calhypo53 && @pool.calhypo53Size.blank? == false && @pool.calhypo53Size.to_d > 0
                    @Cl_nametemp=('chyp53')
                  elsif @pool.calhypo65 && @pool.calhypo65Size.blank? == false && @pool.calhypo65Size.to_d > 0
                    @Cl_nametemp=('chyp65')
                  elsif @pool.calhypo73 && @pool.calhypo73Size.blank? == false && @pool.calhypo73Size.to_d > 0
                    @Cl_nametemp=('chyp73')
                  end
                  @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                else
                  @CPO_Cl=0
                end

                #check if no chemical is available and use last used sanitizer
                if @Cl_nametemp.blank?
                  Rails.logger.info("Chemical name empty.. Going to use last used sanitizer")
                  @lastused = Tester::FakeTextGenerator.new.getlastusedchlorine(@pool)
                  @Cl_type = @lastused
                  Rails.logger.info("Last used sanitizer : "+@lastused.to_s)
                  if @lastused==1
                    @Cl_name=('Dichlor 56%')
                    @Cl_nametemp=('dichlor_56')
                    @CPO_Cl=0.15 #Dichlor 56% ACC CPO factor
                    @divideunit = 0.5375
                  elsif @lastused==2
                    @Cl_name=('Dichlor 62%')
                    @Cl_nametemp=('dichlor_62')
                    @CPO_Cl=0.131 #Dichlor 62% ACC CPO factor
                    @divideunit = 0.5375
                  elsif @lastused==3
                    @Cl_name=('Trichlor')
                    @Cl_nametemp=('trichlor')
                    @CPO_Cl=0.09375 #Trichlor CPO factor
                  elsif @lastused==4
                    @Cl_name=('12% Liquid Chlorine')
                    @Cl_nametemp=('lch12')
                    @CPO_Cl=0.0836 #12% Liquid Chlorine
                  elsif @lastused==5
                    @Cl_name=('6% Liquid Chlorine (Household Bleach)')
                    @Cl_nametemp=('lch6')
                    @CPO_Cl=0.1672 #Approximation 6% Liquid Chlorine
                  elsif @lastused==0
                    @Cl_name='Calcium Hypochlorite'
                    if @pool.calhypo53
                      @Cl_nametemp=('chyp53')
                    elsif @pool.calhypo65
                      @Cl_nametemp=('chyp65')
                    elsif @pool.calhypo73
                      @Cl_nametemp=('chyp73')
                    end
                    @CPO_Cl=0.125 #Approximation 53% Calcium Hypochlorite
                  else
                    @CPO_Cl=0
                  end
                  Rails.logger.info("Chemical name empty.. Going to use last used sanitizer : "+@Cl_name)
                end
                if @swg==2 || @swg==1 #No chlorine generator => manual chlorine
                  #puts "TYPE : "+@Cl_type.to_s
                  #puts "NAME : "+@Cl_name
                  @Calc_CPO_Cl=@CPO_Cl*@spa_vol_gal/10000.0*@delta_FC
                  if @Cl_type<=5 && @Cl_type>=4 #Check for liquid chlorine
                    if @Calc_CPO_Cl>=0.25 #Check for gallons
                      @wholegal_Cl=@Calc_CPO_Cl.floor  #Number of whole gallons of chlorine
                      @quartergal_Cl=((@Calc_CPO_Cl-@wholegal_Cl)/0.25).round #Number of quarter gallons of chlorine
                      @messages << "\nAdd "+ @wholegal_Cl.to_s+" gallon(s) and "+ @quartergal_Cl.to_s+" quarter gallon(s) of "+ @Cl_name +" to the "+ @water_type+" evenly.\n\n"
                      @values.store(@Cl_nametemp, @wholegal_Cl.to_s+"~"+@quartergal_Cl.to_s+";gal")
                    elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>=0.0625 #Check for less than .25 gallon
                      @wholecup_Cl=(@Calc_CPO_Cl*16).floor #Number of whole cups of chlorine
                      @quartercup_Cl=(((@Calc_CPO_Cl*16)-@wholecup_Cl)/0.25).round #Number of quarter cups of chlorine
                      @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                      @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                    elsif @Calc_CPO_Cl<0.0625
                      @wholetbs_Cl=((@Calc_CPO_Cl)*256).round #Number of whole tablespoons of chlorine
                      @messages << "\nAdd "+@wholetbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" evenly.\n\n"
                      @values.store(@Cl_nametemp, @wholetbs_Cl.to_s+";tbs")
                    end
                  elsif @Calc_CPO_Cl>=0.25 && @spa_box==false #Check for less than 1 pound and does not use spa box
                    @wholecup_Cl=(@Calc_CPO_Cl/@divideunit).floor #Assumes 1 cup = 0.5375 lb only for dichlor
                    @quartercup_Cl=(((@Calc_CPO_Cl/@divideunit)-@wholecup_Cl)/0.25).round #Number of quarter cups minus whole cups
                    @messages << "\nAdd "+@wholecup_Cl.to_s+" cup(s) and "+@quartercup_Cl.to_s+" quarter cup(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\nor by filling tab dispensers.\n\n"
                    @values.store(@Cl_nametemp, @wholecup_Cl.to_s+"~"+@quartercup_Cl.to_s+";cup")
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @sanitizer == "Chlorine" #Check for greater than zero and does not use spa box
                    @ozCalc_CPO_Cl=(@Calc_CPO_Cl/@divideunit)*16 #Convert to ounces
                    @tbs_Cl=(@ozCalc_CPO_Cl*2).ceil #Convert to tbs
                    if @spa_box==true #Uses spa box
                      @quarteroz_Cl=@ozCalc_CPO_Cl/0.25 #Total number of 0.25oz chlorine bags
                      @bags_Cl=@quarteroz_Cl.ceil #Number of whole 0.25oz chlorine packets
                      @messages << "\nAdd "+@bags_Cl.to_s+", 0.25 oz package(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                      @values.store(@Cl_nametemp, @bags_Cl.to_s+";pac")
                    elsif @spa_box==false #Does not use spa box
                      @messages << "\nAdd "+@tbs_Cl.to_s+" tablespoon(s) of "+@Cl_name.to_s+" to the "+@water_type.to_s+" by spreading granules\n\n"
                      @values.store(@Cl_nametemp, @tbs_Cl.to_s+";tbs")
                    end
                  elsif @Calc_CPO_Cl<0.25 && @Calc_CPO_Cl>0 && @sanitizer == "Bromine" #Check for greater than zero
                    @messages << "\nAdd 2 bromine tablets in tab dispensers.\n\n"
                    @values.store("bromine", "2")
                  end
                  #elsif @swg==1 #Salt water chlorine generator and low chlorine
                  #  @messages << "Chlorine is low.\nSalt water chlorinator, circulation, and salt concentration need to be checked.\n\n"
                end
              elsif @delta_FC<=0 #Check if chlorine is at or above target
                @messages << "\nChlorine is within range, re-fill tablets if needed.\n\n"
              end
            elsif @sanitizer == "Bromine" #Bromine system
              @minBr=4 #Minimum ideal Br ppm
              @maxBr=6 #Maximum ideal Br ppm
              @Br_tar=(@maxBr+@minBr)/2 #Target Br
              @delta_Br=@Br_tar-@br_i #Difference of bromine target and initial concentration
              if BigDecimal(@delta_Br.to_s) > 0
                @messages << "Bromine low, refill Bromine tablets.\n\n"
              else
                @messages << "Bromine in range, check tablets.\n\n"
              end
            end
          end
          #chlorine range check ends

          if (@daysdiff.blank? == false && @daysdiff > 3) || (@ta_i.blank? == false && @ta_i.to_d <=0)
            @adj_Calc_CPO_NaHCO3=0
          end

          #check for ta range before prescribing ta
          if @tarange == "notgood"
            #Adjusted TA Dose Report #spaboxcondition
            if (@adj_Calc_CPO_NaHCO3>=0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3 && @spa_box == false) #Check for pounds and maximum per 10,000 gallons
              @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
              @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
              @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
            elsif (@adj_Calc_CPO_NaHCO3>0 && @adj_Calc_CPO_NaHCO3<0.25 && @adj_Calc_CPO_NaHCO3<(@spa_vol_gal/10000.0)*@max_NaHCO3) #Check for ounces, zero, and maximum per 10,000 gallons
              @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
              @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
              if @spa_box==true #Uses spa box
                @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
                @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
                @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\nThis will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate to increase TA to "+@TA_tar.to_s+" ppm with pH adjusments. \nThis will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
              end
            else
              if @adj_Calc_CPO_NaHCO3>=0.25 && @spa_box == false
                @adj_wholecup_NaHCO3=@adj_Calc_CPO_NaHCO3.floor #Assumes 1 cup = 1 lb
                @adj_quartercup_NaHCO3=((@adj_Calc_CPO_NaHCO3-@adj_wholecup_NaHCO3)/0.25).round #Number of quarter cups minus whole cups
                @messages << "Add "+@adj_wholecup_NaHCO3.to_s+" cup(s) and "+@adj_quartercup_NaHCO3.to_s+" quarter cup(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                @values.store("sodium_bicarbonate", @adj_wholecup_NaHCO3.to_s+"~"+@adj_quartercup_NaHCO3.to_s+";cup")
              elsif (@adj_Calc_CPO_NaHCO3>0) #Check for <1 lb and >0
                @adj_Calc_CPO_NaHCO3=@adj_Calc_CPO_NaHCO3*16 #Convert to ounces
                @tbs_NaHCO3=(@adj_Calc_CPO_NaHCO3*2).round #Convert from ounces to tbs
                if @spa_box==true #Uses spa box
                  @halfoz_NaHCO3=@adj_Calc_CPO_NaHCO3/0.5 #Total number of 0.5oz sodium bicarbonate bags
                  @bags_NaHCO3=@halfoz_NaHCO3.round #Number of whole 0.5oz sodium bicarbonate bags
                  @messages << "Add "+@bags_NaHCO3.to_s+", 0.5 oz packet(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                  @values.store("sodium_bicarbonate", @bags_NaHCO3.to_s+";pac")
                elsif @spa_box==false #Does not use spa box
                  @messages << "Add "+@tbs_NaHCO3.to_s+" tablespoon(s) of Sodium bicarbonate INCREMENTALLY to increase TA to "+@TA_tar.to_s+" ppm with pH adjustment.\n This will also increase the pH.\n\n"
                  @values.store("sodium_bicarbonate", @tbs_NaHCO3.to_s+";tbs")
                end
              end
            end
          end
          #ta range check ends

          #check for ph range before prescribing ph
          if @phrange == "notgood"
            #pH Dose Report
            if (@Calc_CPO_Na2CO3>=0.25 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for pounds and maximum per 10,000 gallons
              @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
              @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate to increase pH \n"
              @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
            elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0 && @Calc_CPO_Na2CO3<@max_Na2CO3*@spa_vol_gal/10000.0) #Check for oz. and maximum per 10,000 gallons
              @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
              @tbs_Na2CO3=(@Calc_CPO_Na2CO3*2).round #Converts oz to tbs
              if @spa_box==true #Uses spa box
                @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
                @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
                @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate to increase pH \n"
                @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate to increase pH \n"
                @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
              end
            else #greater than maximum dose per 10,000 gallons
              if @Calc_CPO_Na2CO3>=0.25
                @wholecup_Na2CO3=(@Calc_CPO_Na2CO3/0.574).floor #Assumes 1 cup = 0.574 lb
                @quartercup_Na2CO3=((@Calc_CPO_Na2CO3-@wholecup_Na2CO3)/0.25).round #Number of quarter cups minus whole cups
                @messages << "Add "+@wholecup_Na2CO3.to_s+" cup(s) and "+@quartercup_Na2CO3.to_s+" quarter cup(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                @values.store("sodium_carbonate", @wholecup_Na2CO3.to_s+"~"+@quartercup_Na2CO3.to_s+";cup")
              elsif (@Calc_CPO_Na2CO3<0.25 && @Calc_CPO_Na2CO3>0)
                @Calc_CPO_Na2CO3=(@Calc_CPO_Na2CO3*16)/0.574 #Convert to ounces
                @tbs_Na2CO3=(@Calc_CPO_CPO_Na2CO3*2).round #Converts oz to tbs
                if @spa_box==true #Uses spa box
                  @quarteroz_Na2CO3=@Calc_CPO_Na2CO3/0.25 #Total number of 0.25oz soda ash bags
                  @Bags_Na2CO3=@quarteroz_Na2CO3.round #Number of whole 0.25oz soda ash bags
                  @messages << "Add "+@Bags_Na2CO3.to_s+", 0.25 oz packet(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                  @values.store("sodium_carbonate", @Bags_Na2CO3.to_s+";pac")
                elsif @spa_box==false #Does not use spa box
                  @messages << "Add "+@tbs_Na2CO3.to_s+" tablespoon(s) of Sodium carbonate INCREMENTALLY to increase pH \n"
                  @values.store("sodium_carbonate", @tbs_Na2CO3.to_s+";tbs")
                end
              end
            end
            #puts @Calc_CPO_NaHSO4
            if (@Calc_CPO_NaHSO4>=0.25 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000.0) #Check for pounds and maximum dose per 10,000 gallons
              @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #Assumes 1 cup = 0.574 lb
              @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
              @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite to lower pH \n"
              @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
            elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0 && @Calc_CPO_NaHSO4<@max_NaHSO4*@spa_vol_gal/10000) #Check for ounces and max dose per 10,000 gallons
              @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16) #Convert to ounces
              @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
              if @spa_box==true #Uses spa box
                @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
                @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
                @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite to lower pH \n"
                @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
              elsif @spa_box==false #Does not use spa box
                @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite to lower pH \n"
                @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
              end
            else #greater than maximum dose per 10,000 gallons
              if @Calc_CPO_NaHSO4>=0.25 #Greater or equal to 1 pound
                @wholecup_NaHSO4=(@Calc_CPO_NaHSO4).floor #assumes 1 cup = 0.574 lb
                @quartercup_NaHSO4=((@Calc_CPO_NaHSO4-@wholecup_NaHSO4)/0.25).round #Number of quarter cups minus whole cups
                @messages << "Add "+@wholecup_NaHSO4.to_s+" cup(s) and "+@quartercup_NaHSO4.to_s+" quarter cup(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                @values.store("sodium_bisulfite", @wholecup_NaHSO4.to_s+"~"+@quartercup_NaHSO4.to_s+";cup")
              elsif (@Calc_CPO_NaHSO4<0.25 && @Calc_CPO_NaHSO4>0) #Less than a pound, greater than zero
                @Calc_CPO_NaHSO4=(@Calc_CPO_NaHSO4*16) #Convert to ounces
                @tbs_NaHSO4=(@Calc_CPO_NaHSO4*2).round #Converts oz to tbs
                if @spa_box==true #Uses spa box
                  @halfoz_NaHSO4=@Calc_CPO_NaHSO4/0.5 #Total number of 0.5oz sodium bisulfite packets
                  @bags_NaHSO4=@halfoz_NaHSO4.round #Number of whole 0.5oz sodium bisulfite packets
                  @messages << "Add "+@bags_NaHSO4.to_s+", 0.5 oz packet(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                  @values.store("sodium_bisulfite", @bags_NaHSO4.to_s+";pac")
                elsif @spa_box==false #Does not use spa box
                  @messages << "Add "+@tbs_NaHSO4.to_s+" tablespoon(s) of Sodium bisulfite INCREMENTALLY to lower pH \n"
                  @values.store("sodium_bisulfite", @tbs_NaHSO4.to_s+";tbs")
                end
              end
            end
          end
          #ph range check ends
          puts @messages
          puts @values
          Rails.logger.info("DOSAGE AUTO : "+@values.inspect)
        end #if spa check
        puts "==================================Spa Dosage ends================================="
        return { values: @values, messages: @messages }
      else
        # AUTO_PRESCRIPTION is not enabled
        return { values: @values, messages: @messages }
      end
    end
  end
end

# x = Tester::Dosage.new.pool_dosage(7.3, 3, 110)
# y = Tester::Dosage.new.spa_dosage(7.3, 3, 110)

# puts "====== VALUES ======"
# puts x, y
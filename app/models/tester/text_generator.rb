# require '../dosage'

module Tester
  class TextGenerator
    def dosage_text_for_beta(autoval, pool)
      @pool = pool
      @autoval = autoval
      # puts @autoval
      @dsg = ::Calculator::Dosage.new
      @count = 0
      @text = ""

      @unit = "cup(s)"
      @tablets = "Tablet(s)"
      @gallons = "Gallon(s)"
      @lbs = "lbs"
      if @pool.isPool
        @unit = "cup"
      else
        @unit = "packet(s)"
      end

      if @autoval["bromine"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["bromine"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @tempf = @autoval["bromine"].to_f
        end
        @dsg.bromine1Size  = @tempf.to_f+(@tempq.to_f*0.25)

        if @tempf > 0 && @tempq > 0
          @tablets = getunits(@tempq,@tablets)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@tablets} of Bromine, "
        elsif @tempf > 0 && @tempq <= 0
          @tablets = getunits(@tempf,@tablets)
          @text += " "+@tempf.to_s+" #{@tablets} of Bromine, "
        elsif @tempf <= 0 && @tempq > 0
          @tablets = getunits(@tempq,@tablets)
          @text += " "+@tempq.to_s+" #{@tablets} of Bromine, "
        end
      end

      if @autoval["dichlor_56"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["dichlor_56"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["dichlor_56"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["dichlor_56"].to_f/16
          else
            @tempf = @autoval["dichlor_56"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.dichlor = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          puts @unit
          puts "##"
          @dsg.esanitizer = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            puts @unit
            puts "##"
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          end
        end
      end

      if @autoval["sodium_carbonate"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["sodium_carbonate"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["sodium_carbonate"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["sodium_carbonate"].to_f/16
          else
            @tempf = @autoval["sodium_carbonate"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.phUp = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Soda Ash, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.bsodaash = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Soda Ash, "
          end
        end
      end

      if @autoval["sodium_bisulfite"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["sodium_bisulfite"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["sodium_bisulfite"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["sodium_bisulfite"].to_f/16
          else
            @tempf = @autoval["sodium_bisulfite"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.phDown  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dry Acid, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.ddryacid  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dry Acid, "
          end
        end
      end

      if @autoval["sodium_bicarbonate"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["sodium_bicarbonate"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["sodium_bicarbonate"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["sodium_bicarbonate"].to_f/16
          else
            @tempf = @autoval["sodium_bicarbonate"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.taUp = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.bbakingsoda  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          end
        end
      end

      if @autoval["trichlor"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["trichlor"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @tempf = @autoval["trichlor"].to_f
        end
        @dsg.trichlor  = @tempf.to_f+(@tempq.to_f*0.25)
        if @tempf > 0 && @tempq > 0
          @tablets = getunits(@tempq,@tablets)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@tablets} of trichlor, "
        elsif @tempf > 0 && @tempq <= 0
          @tablets = getunits(@tempf,@tablets)
          @text += " "+@tempf.to_s+" #{@tablets} of trichlor, "
        elsif @tempf <= 0 && @tempq > 0
          @tablets = getunits(@tempq,@tablets)
          @text += " "+@tempq.to_s+" #{@tablets} of trichlor, "
        end
      end

      if @autoval["lch6"]
        @tempf = 0
        @tempq = 0
        @count += 1
        #@tempauto = @autoval["lch6"].split('~')
        @t1 = @autoval["lch6"].split(";")
        @dunit = @t1[1]
        @tempauto = @t1[0].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["lch6"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @tempauto[0].to_f/16
          else
            @tempf = @tempauto[0].to_f
          end
        end

        if @dunit == "cup"
          @dunit = "cup"
        elsif @dunit == "gal"
          @dunit = "gallon"
        end

        @dsg.lch6unit = @dunit
        @dsg.lch6qunit = @dunit
        @dsg.liquidCl6  = @tempf.to_f+(@tempq.to_f*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Liquid Chlorine 6%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Liquid Chlorine 6%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Liquid Chlorine 6%, "
        end
      end

      if @autoval["lch12"]
        @tempf = 0
        @tempq = 0
        @count += 1
        #@tempauto = @autoval["lch12"].split('~')
        @t1 = @autoval["lch12"].split(";")
        @dunit = @t1[1]
        @tempauto = @t1[0].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["lch12"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @tempauto[0].to_f/16
          else
            @tempf = @tempauto[0].to_f
          end
        end

        if @dunit == "cup"
          @dunit = "cup"
        elsif @dunit == "gal"
          @dunit = "gallon"
        end

        @dsg.lch12unit = @dunit
        @dsg.lch12qunit = @dunit
        @dsg.liquidChlorine  = @tempf.to_f+(@tempq.to_f*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Liquid Chlorine 12%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf,@unit)
          @text += " "+@tempf.to_s+" #{@unit} of Liquid Chlorine 12%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq,@unit)
          @text += " "+@tempq.to_s+" #{@unit} of Liquid Chlorine 12%, "
        end
      end


      if @autoval["dichlor_62"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["dichlor_62"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["dichlor_62"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["dichlor_62"].to_f/16
          else
            @temp_arr = @autoval["dichlor_62"].split(";")
            @tablespoon = false
            if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
              @tablespoon = true
            end
            if @tablespoon
              @tempf = @autoval["dichlor_62"].to_f/16
            else
              @tempf = @autoval["dichlor_62"].to_f
            end
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.dichlor62Size  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          end
        else
          @dsg.dichlor62Size  = @tempf.to_f/16+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          end
        end
      end

      if @autoval["chyp53"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["chyp53"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["chyp53"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["chyp53"].to_f/16
          else
            @tempf = @autoval["chyp53"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.calhypo53  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.calhypo53  = @tempf.to_f/16+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          end
        end
      end

      if @autoval["chyp65"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["chyp65"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["chyp65"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["chyp65"].to_f/16
          else
            @tempf = @autoval["chyp65"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.calhypo65  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.calhypo65  = @tempf.to_f/16+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          end
        end
      end

      if @autoval["chyp73"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["chyp73"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["chyp73"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["chyp73"].to_f/16
          else
            @tempf = @autoval["chyp73"].to_f
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.calhypo73  = @tempf.to_f+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.calhypo73  = @tempf.to_f/16+(@tempq.to_f*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf,@unit)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq,@unit)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          end
        end
      end

      if @autoval["muriatic_acid"]
        @tempf = 0
        @tempq = 0
        @count += 1
        #@tempauto = @autoval["muriatic_acid"].split('~')
        @t1 = @autoval["muriatic_acid"].split(";")
        @dunit = @t1[1]
        @tempauto = @t1[0].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_f
          @tempq = @tempauto[1].to_f
        else
          @temp_arr = @autoval["muriatic_acid"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @tempauto[0].to_f/16
          else
            @tempf = @tempauto[0].to_f
          end
        end

        if @dunit == "cup"
          @dunit = "cup"
        elsif @dunit == "gal"
          @dunit = "gallon"
        end

        @dsg.macid31unit = @dunit
        @dsg.macid31qunit = @dunit
        @dsg.muriatic31  = @tempf.to_f+(@tempq.to_f*0.25)
        if @tempf > 0 && @tempq > 0
          @gallons = getunits(@tempq,@gallons)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@gallons} of muriatic acid, "
        elsif @tempf > 0 && @tempq <= 0
          @gallons = getunits(@tempf,@gallons)
          @text += " "+@tempf.to_s+" #{@gallons} of muriatic acid, "
        elsif @tempf <= 0 && @tempq > 0
          @gallons = getunits(@tempq,@gallons)
          @text += " "+@tempq.to_s+" #{@gallons} of muriatic acid, "
        end
      end

      @mtest = @pool.mtests.last
      if @mtest.nil? == false && @mtest.freeCl.nil? == false && @mtest.freeCl.to_f < 3 && @pool.waterChemistry == "Salt Water"
        @count += 1
      end

      if @count > 0
        @phdosage = nil
        @orpdosage = nil

        # @epool = Epool.find_by_pool_id(@pool.id)
        # if @epool
        #   @point = Point.find_by_sql("select * from points where epool_id = "+@epool.id.to_s+" and extAddr = '"+@epool.extAddr+"' and date(created_at) <= DATE(NOW()) order by created_at desc limit 1")
        #   @point.each do |point|
        #     if @epool.device_type == "L"
        #       @phdosage = phlorasingle(@epool,point)
        #       @orpdosage = orplorasingle(@epool,point)
        #     else
        #       @phdosage = phsingle(@epool,point)
        #       @orpdosage = orpsingle(@epool,point)
        #     end
        #   end
        # end

        if @mtest.nil? == false && @mtest.freeCl.nil? == false && @mtest.freeCl.to_f < 3 && @pool.waterChemistry == "Salt Water"
          @dsg.saltwater = true
        end
        #dsaf
        @dsg.ph = @phdosage
        @dsg.orp = @orpdosage
        # @dsg.pool_id = @pool.id
        # @dsg.customer_id = @pool.customer_id
        # @dsg.status = false
        # @dsg.approved = true
        # @dsg.save

        # senddosagependingsms(@dsg,@pool)

        #process chemical low check
        #notifylowstock("create",@pool,@dsg)
        #chemical low check ends

        #@dos = Dosage.last
        #customerdone(@dos.id)
      end

      #notify administrator if the pool in bad state and no prescription generated
      # notifyadminofthestate(@pool)

      return generatedosagetext(@dsg, @pool)
    end

    def getunits(value,unit)
      @temp_unit = ""
      if unit != "mL"
        unit = (unit.downcase).gsub(/\s+/, "")
      end
      if unit == "cup(s)" || unit == "cup" ||  unit == "cups"  ||  unit == "cups(s)"
        if value.to_f > 1
          @temp_unit = "cups"
        else
          @temp_unit = "cup"
        end
      elsif unit == "tablet(s)" || unit == "tablet" || unit == "tablets"
        if value.to_f > 1
          @temp_unit = "Tablets"
        else
          @temp_unit = "Tablet"
        end
      elsif unit  == "gallon(s)" || unit == "gallon" || unit == "gallons"
        if value.to_f > 1
          @temp_unit = "Gallons"
        else
          @temp_unit = "Gallon"
        end
      elsif unit == "packet(s)" || unit == "packet" || unit == "packets"
        if value.to_f > 1
          @temp_unit = "packets"
        else
          @temp_unit = "packet"
        end
      elsif unit == "tablespoon(s)" || unit == "tablespoon" || unit == "tablespoons"
        if value.to_f > 1
          @temp_unit = "tablespoons"
        else
          @temp_unit = "tablespoon"
        end
      elsif unit == "lbs"
        if value.to_f > 1
          @temp_unit = "lbs"
        else
          @temp_unit = "lb"
        end
      else
        if value.to_f > 1
          @temp_unit = unit.gsub("(","").gsub(")","")
        else
          @temp_unit = unit.gsub("(s)","")
        end
      end

      if @current_user_role.nil? == false && @current_user_role == "Lora" && (@temp_unit == "tablespoons" || @temp_unit == "tablespoon")
        @temp_unit = "tsp"
      end

      return @temp_unit
    end

    def generatedosagetext(dosage, pool)
      @units = "cup(s)"
      @nonSpaBoxUnit = "tablespoon(s)"
      @tablets = "Tablet(s)"
      @gallons = "Gallon(s)"
      @lbs = "lbs"
      if pool.isPool
        @units = "cup(s)"
        @nonSpaBoxUnit = "cup(s)"
      else
        @units = "packet(s)"
        @nonSpaBoxUnit = "cup(s)"
      end

      @skin = pool.chemical_skin

      @content = ""
      if @skin == "advanced"
        if dosage.trichlor.nil? == false && dosage.trichlor != "" && dosage.trichlor.to_f > 0
          if @content != ""
            @content += ", "
          end
          @tablets = getunits(dosage.trichlor,@tablets)
          @content += dosage.trichlor.to_s+' Trichlor 3\" tablets'
        end

        if dosage.phUp.nil? == false && dosage.phUp != "" && dosage.phUp.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUp,@nonSpaBoxUnit).to_s+" Soda Ash"
        end

        if dosage.phDown.nil? == false && dosage.phDown != "" && dosage.phDown.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phDown,@nonSpaBoxUnit).to_s+" Dry acid"
        end

        if dosage.taUp.nil? == false && dosage.taUp != "" && dosage.taUp.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.taUp,@nonSpaBoxUnit).to_s+" Sodium Bicarbonate"
        end

        if dosage.liquidCl6.nil? == false && dosage.liquidCl6 != "" && dosage.liquidCl6.to_f > 0
          if @content != ""
            @content += ", "
          end
          @s6_liquid = " 6% Liquid Chlorine "
          if pool.option
            @s6_liquid = " Liquid Chlorine "
          end
          @content += getchemtext(dosage.liquidCl6,dosage.lch6unit+"~"+dosage.lch6qunit).to_s+@s6_liquid
        end

        if dosage.liquidCl8.nil? == false && dosage.liquidCl8 != "" && dosage.liquidCl8.to_f > 0
          if @content != ""
            @content += ", "
          end
          @s8_liquid = " 8% Liquid Chlorine "
          if pool.option
            @s8_liquid = " Liquid Chlorine "
          end
          @content += getchemtext(dosage.liquidCl8,dosage.lch8unit+"~"+dosage.lch8qunit).to_s+@s8_liquid
        end

        if dosage.liquidChlorine.nil? == false && dosage.liquidChlorine != "" && dosage.liquidChlorine.to_f > 0
          if @content != ""
            @content += ", "
          end
          @s12_liquid = " 12% Liquid Chlorine "
          if pool.option
            @s12_liquid = " Liquid Chlorine "
          end
          @content += getchemtext(dosage.liquidChlorine,dosage.lch12unit+"~"+dosage.lch12qunit).to_s+@s12_liquid
        end

        if dosage.dichlor.nil? == false && dosage.dichlor != "" && dosage.dichlor.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c56_dichlor = " 56% Dichlor "
          if pool.option
            @c56_dichlor = " Dichlor "
          end
          @content += getchemtext(dosage.dichlor,@nonSpaBoxUnit).to_s+@c56_dichlor
        end

        if dosage.dichlor62Size.nil? == false && dosage.dichlor62Size != "" && dosage.dichlor62Size.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c62_dichlor = " 62% Dichlor "
          if pool.option
            @c62_dichlor = " Dichlor "
          end
          @content += getchemtext(dosage.dichlor62Size,@nonSpaBoxUnit).to_s+@c62_dichlor
        end

        if dosage.calhypo53.nil? == false && dosage.calhypo53 != "" && dosage.calhypo53.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c53_hypo = " 53% Cal-hypo "
          if pool.option
            @c53_hypo = " Cal-hypo "
          end
          @content += getchemtext(dosage.calhypo53,@lbs).to_s+@c53_hypo
        end

        if dosage.calhypo65.nil? == false && dosage.calhypo65 != "" && dosage.calhypo65.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c65_hypo = " 65% Cal-hypo "
          if pool.option
            @c65_hypo = " Cal-hypo "
          end
          @content += getchemtext(dosage.calhypo65,@lbs).to_s+@c65_hypo
        end

        if dosage.calhypo73.nil? == false && dosage.calhypo73 != "" && dosage.calhypo73.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c73_hypo = " 73% Cal-hypo "
          if pool.option
            @c73_hypo = " Cal-hypo "
          end
          @content += getchemtext(dosage.calhypo73,@lbs).to_s+@c73_hypo
        end

        if dosage.phUpBorax.nil? == false && dosage.phUpBorax != "" && dosage.phUpBorax.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUpBorax,@nonSpaBoxUnit).to_s+" borax"
        end

        if dosage.muriatic15.nil? == false && dosage.muriatic15 != "" && dosage.muriatic15.to_f > 0
          if @content != ""
            @content += ", "
          end
          @m73_acid = " 15.7% Muriatic Acid "
          if pool.option
            @m73_acid = " Muriatic Acid "
          end
          @content += getchemtext(dosage.muriatic15,dosage.macid15unit+"~"+dosage.macid15qunit).to_s+@m73_acid
        end

        if dosage.muriatic31.nil? == false && dosage.muriatic31 != "" && dosage.muriatic31.to_f > 0
          if @content != ""
            @content += ", "
          end
          @m31_acid = " 31% Muriatic Acid "
          if pool.option
            @m31_acid = " Muriatic Acid "
          end
          @content += getchemtext(dosage.muriatic31,dosage.macid31unit+"~"+dosage.macid31qunit).to_s+@m31_acid
        end

        if dosage.trichlor2Size.nil? == false && dosage.trichlor2Size != "" && dosage.trichlor2Size.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.trichlor2Size.to_s+' Trichlor 2\" tablets'
        end

        if dosage.bromine1Size.nil? == false && dosage.bromine1Size != "" && dosage.bromine1Size.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.bromine1Size.to_s+' Bromine 1\" tablets'
        end

        if dosage.lithiumhypoSize.nil? == false && dosage.lithiumhypoSize != "" && dosage.lithiumhypoSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.lithiumhypoSize,@nonSpaBoxUnit).to_s+" Lithium Hypo"
        end

        if dosage.calciumSize.nil? == false && dosage.calciumSize != "" && dosage.calciumSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.calciumSize,@nonSpaBoxUnit).to_s+" Calcium chloride"
        end

        if dosage.cyanuricsolidSize.nil? == false && dosage.cyanuricsolidSize != "" && dosage.cyanuricsolidSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricsolidSize,@nonSpaBoxUnit).to_s+" Cyanuric Acid Solid"
        end

        if dosage.cyanuricliquidSize.nil? == false && dosage.cyanuricliquidSize != "" && dosage.cyanuricliquidSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricliquidSize,@nonSpaBoxUnit).to_s+" Cyanuric Acid Liquid"
        end

        if dosage.esanitizer.nil? == false && dosage.esanitizer != "" && dosage.esanitizer.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.esanitizer.to_i,"packet(s)")
          @content += dosage.esanitizer.to_s+" "+@packet+" E"
        end

        if dosage.ddryacid.nil? == false && dosage.ddryacid != "" && dosage.ddryacid.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.ddryacid.to_i,"packet(s)")
          @content += dosage.ddryacid.to_s+" "+@packet+" D"
        end

        if dosage.bbakingsoda.nil? == false && dosage.bbakingsoda != "" && dosage.bbakingsoda.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bbakingsoda.to_i,"packet(s)")
          @content += dosage.bbakingsoda.to_s+" "+@packet+" A"
        end

        if dosage.bsodaash.nil? == false && dosage.bsodaash != "" && dosage.bsodaash.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bsodaash.to_i,"packet(s)")
          @content += dosage.bsodaash.to_s+" "+@packet+" B"
        end
      elsif @skin == "basic"
        if dosage.trichlor.nil? == false && dosage.trichlor != "" && dosage.trichlor.to_f > 0
          if @content != ""
            @content += ", "
          end
          @tablets = getunits(dosage.trichlor,@tablets)
          @content += dosage.trichlor.to_s+' '+getchemicaltype(pool,"Trichlor 3")+' tablets'
        end

        if dosage.phUp.nil? == false && dosage.phUp != "" && dosage.phUp.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUp,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Soda Ash")
        end

        if dosage.phDown.nil? == false && dosage.phDown != "" && dosage.phDown.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phDown,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Dry Acid")
        end

        if dosage.taUp.nil? == false && dosage.taUp != "" && dosage.taUp.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.taUp,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Sodium Bicarbonate")
        end

        if dosage.liquidCl6.nil? == false && dosage.liquidCl6 != "" && dosage.liquidCl6.to_f > 0
          if @content != ""
            @content += ", "
          end
          @s6_liquid = "6% Liquid Chlorine"
          if pool.option
            @s6_liquid = "Liquid Chlorine"
          end
          @content += getchemtext(dosage.liquidCl6,dosage.lch6unit+"~"+dosage.lch6qunit).to_s+" "+getchemicaltype(pool,@s6_liquid)
        end

        if dosage.liquidCl8.nil? == false && dosage.liquidCl8 != "" && dosage.liquidCl8.to_f > 0
          if @content != ""
            @content += ", "
          end
          @s8_liquid = "8% Liquid Chlorine"
          if pool.option
            @s8_liquid = "Liquid Chlorine"
          end
          @content += getchemtext(dosage.liquidCl8,dosage.lch8unit+"~"+dosage.lch8qunit).to_s+" "+getchemicaltype(pool,@s8_liquid)
        end

        if dosage.liquidChlorine.nil? == false && dosage.liquidChlorine != "" && dosage.liquidChlorine.to_f > 0
          if @content != ""
            @content += ", "
          end
          @s12_liquid = "12% Liquid Chlorine"
          if pool.option
            @s12_liquid = "Liquid Chlorine"
          end
          @content += getchemtext(dosage.liquidChlorine,dosage.lch12unit+"~"+dosage.lch12qunit).to_s+" "+getchemicaltype(pool,@s12_liquid)
        end

        if dosage.dichlor.nil? == false && dosage.dichlor != "" && dosage.dichlor.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c56_dichlor = "56% Dichlor"
          if pool.option
            @c56_dichlor = "Dichlor"
          end
          @content += getchemtext(dosage.dichlor,@units).to_s+" "+getchemicaltype(pool,@c56_dichlor)
        end

        if dosage.dichlor62Size.nil? == false && dosage.dichlor62Size != "" && dosage.dichlor62Size.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c62_dichlor = "62% Dichlor"
          if pool.option
            @c62_dichlor = "Dichlor"
          end
          @content += getchemtext(dosage.dichlor62Size,@units).to_s+" "+getchemicaltype(pool,@c62_dichlor)
        end

        if dosage.calhypo53.nil? == false && dosage.calhypo53 != "" && dosage.calhypo53.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c53_hypo = "53% Cal-hypo"
          if pool.option
            @c53_hypo = "Cal-hypo"
          end
          @content += getchemtext(dosage.calhypo53,@lbs).to_s+" "+getchemicaltype(pool,@c53_hypo)
        end

        if dosage.calhypo65.nil? == false && dosage.calhypo65 != "" && dosage.calhypo65.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c65_hypo = "65% Cal-hypo"
          if pool.option
            @c65_hypo = "Cal-hypo"
          end
          @content += getchemtext(dosage.calhypo65,@lbs).to_s+" "+getchemicaltype(pool,@c65_hypo)
        end

        if dosage.calhypo73.nil? == false && dosage.calhypo73 != "" && dosage.calhypo73.to_f > 0
          if @content != ""
            @content += ", "
          end
          @c73_hypo = "73% Cal-hypo"
          if pool.option
            @c73_hypo = "Cal-hypo"
          end
          @content += getchemtext(dosage.calhypo73,@lbs).to_s+" "+getchemicaltype(pool,@c73_hypo)
        end

        if dosage.phUpBorax.nil? == false && dosage.phUpBorax != "" && dosage.phUpBorax.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUpBorax,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Borax")
        end

        if dosage.muriatic15.nil? == false && dosage.muriatic15 != "" && dosage.muriatic15.to_f > 0
          if @content != ""
            @content += ", "
          end
          @m73_acid = "15.7% Muriatic Acid"
          if pool.option
            @m73_acid = "Muriatic Acid"
          end
          @content += getchemtext(dosage.muriatic15,dosage.macid15unit+"~"+dosage.macid15qunit).to_s+" "+getchemicaltype(pool,@m73_acid)
        end

        if dosage.muriatic31.nil? == false && dosage.muriatic31 != "" && dosage.muriatic31.to_f > 0
          if @content != ""
            @content += ", "
          end
          @m31_acid = "31% Muriatic Acid"
          if pool.option
            @m31_acid = "Muriatic Acid"
          end
          @content += getchemtext(dosage.muriatic31,dosage.macid31unit+"~"+dosage.macid31qunit).to_s+" "+getchemicaltype(pool,@m31_acid)
        end

        if dosage.trichlor2Size.nil? == false && dosage.trichlor2Size != "" && dosage.trichlor2Size.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.trichlor2Size.to_s+" "+getchemicaltype(pool,'Trichlor')
        end

        if dosage.bromine1Size.nil? == false && dosage.bromine1Size != "" && dosage.bromine1Size.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.bromine1Size.to_s+" "+getchemicaltype(pool,'Bromine')
        end

        if dosage.lithiumhypoSize.nil? == false && dosage.lithiumhypoSize != "" && dosage.lithiumhypoSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.lithiumhypoSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Lithium Hypo")
        end

        if dosage.calciumSize.nil? == false && dosage.calciumSize != "" && dosage.calciumSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.calciumSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Calcium chloride")
        end

        if dosage.cyanuricsolidSize.nil? == false && dosage.cyanuricsolidSize != "" && dosage.cyanuricsolidSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricsolidSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Cyanuric Acid Solid")
        end

        if dosage.cyanuricliquidSize.nil? == false && dosage.cyanuricliquidSize != "" && dosage.cyanuricliquidSize.to_f > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricliquidSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Cyanuric Acid Liquid")
        end

        if dosage.esanitizer.nil? == false && dosage.esanitizer != "" && dosage.esanitizer.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.esanitizer.to_i,"packet(s)")
          @content += dosage.esanitizer.to_s+" "+@packet+" "+getchemicaltype(pool,"E")
        end

        if dosage.ddryacid.nil? == false && dosage.ddryacid != "" && dosage.ddryacid.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.ddryacid.to_i,"packet(s)")
          @content += dosage.ddryacid.to_s+" "+@packet+" "+getchemicaltype(pool,"D")
        end

        if dosage.bbakingsoda.nil? == false && dosage.bbakingsoda != "" && dosage.bbakingsoda.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bbakingsoda.to_i,"packet(s)")
          @content += dosage.bbakingsoda.to_s+" "+@packet+" "+getchemicaltype(pool,"A")
        end

        if dosage.bsodaash.nil? == false && dosage.bsodaash != "" && dosage.bsodaash.to_f > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bsodaash.to_i,"packet(s)")
          @content += dosage.bsodaash.to_s+" "+@packet+" "+getchemicaltype(pool,"B")
        end
      end
      puts "Dosage %%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      puts @content
      puts "Dosage %%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      return @content
    end

    def getchemtext(value,unit)
      #puts unit
      #check if full and quarter units are present
      if unit.split("~").count > 1
        @parts = unit.split("~")
        @unitf = @parts[0]+"(s)"
        @unitq = @parts[1]+"(s)"
      else
        @unitf = unit
        @unitq = unit
      end

      @text = ""
      @f = getfull(value)
      @q = getquarters(value)
      @t = gettablespoons(value)
      if @f.nil? == false && @f > 0
        @temp_f = getunits(@f.to_i,@unitf)
        @text = @f.to_s+" #{@temp_f}"
      end
      if @q.nil? == false && @q > 0
        if @text.nil?
          if  @q == 2
            @temp_unit_ = getunits(1,@unitq)
            @text += "a half "+@temp_unit
          else
            @temp_unit_ = getunits(@q.to_i,@unitq)
            @text = @q.to_s+" quarter #{@temp_unit_}"
          end
        else
          if @t.nil? || @t <= 0
            if  @q == 2
              @temp_u = getunits(@f.to_i,@unitf)
              @text = @f.to_s+" #{@temp_u}"
              @temp_unit = getunits(1,@unitq)
              @text += " and a half "+@temp_unit
            else
              @temp_q = getunits(@q.to_i,@unitq)
              @text += " and "+@q.to_s+" quarter #{@temp_q}"
            end
          else
            @temp_unitq = getunits(@q.to_i,@unitq)
            @text += ", "+@q.to_s+" quarter #{@temp_unitq}"
          end
        end
      end
      if @t.nil? == false && @t > 0
        if @text.nil?
          @temp_tablespoon = getunits(@t.to_i,"tablespoon(s)")
          @text = @t.to_s+" "+@temp_tablespoon
        else
          @tablespoon = getunits(@t.to_i,"tablespoon(s)")
          @text+= " and "+@t.to_s+" "+@tablespoon
        end
      end
      return @text
    end

    def getquarters(value)
      @temptbl = 0
      @temptbl = ((value.to_f) % 1 / 0.25)
      @parts = @temptbl.to_s.split(".")
      @temptbl = @parts.count > 1 ? @parts[0].to_s : 0
      return @temptbl.to_i
    end

    def getfull(value)
      @temptbl = 0
      @temptbl = (value.to_f).div 1
      #@parts = @temptbl.to_s.split(".")
      #@temptbl = @parts.count > 1 ? @parts[0].to_s : 0
      return @temptbl.to_i
    end

    def gettablespoons(value)
      @temptbl = 0
      @temptbl = (((value.to_f) % 1 % 0.25)*16)
      @parts = @temptbl.to_s.split(".")
      @temptbl = @parts.count > 1 ? @parts[0].to_s : 0
      return @temptbl.to_i
    end
  end
end
module Tester
  class FakeTextGenerator
    def generatedosagetextforbeta(autoval, pool)
      @autoval = autoval
      puts @autoval
      @dsg = ::Calculator::Dosage.new
      @count = 0
      @text = ""
      @pool = pool

      @unit = "cup(s)"
      @tablets = "Tablet(s)"
      @gallons = "Gallon(s)"
      @lbs = "lbs"
      if @pool.isPool
        @unit = "cup"
      else
        @unit = "packet(s)"
      end

      if @autoval["bromine"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["bromine"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @tempf = @autoval["bromine"].to_d
        end
        @dsg.bromine1Size = @tempf.to_d+(@tempq.to_d*0.25)

        if @tempf > 0 && @tempq > 0
          @tablets = getunits(@tempq, @tablets)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@tablets} of Bromine, "
        elsif @tempf > 0 && @tempq <= 0
          @tablets = getunits(@tempf, @tablets)
          @text += " "+@tempf.to_s+" #{@tablets} of Bromine, "
        elsif @tempf <= 0 && @tempq > 0
          @tablets = getunits(@tempq, @tablets)
          @text += " "+@tempq.to_s+" #{@tablets} of Bromine, "
        end
      end

      if @autoval["dichlor_56"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["dichlor_56"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["dichlor_56"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["dichlor_56"].to_d/16
          else
            @tempf = @autoval["dichlor_56"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.dichlor = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          puts @unit
          puts "##"
          @dsg.esanitizer = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            puts @unit
            puts "##"
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 56%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 56%, "
          end
        end
      end

      if @autoval["sodium_carbonate"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["sodium_carbonate"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d rescue 0
        else
          @temp_arr = @autoval["sodium_carbonate"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["sodium_carbonate"].to_d/16
          else
            @tempf = @autoval["sodium_carbonate"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.phUp = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Soda Ash, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.bsodaash = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Soda Ash, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Soda Ash, "
          end
        end
      end

      if @autoval["sodium_bisulfite"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["sodium_bisulfite"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["sodium_bisulfite"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["sodium_bisulfite"].to_d/16
          else
            @tempf = @autoval["sodium_bisulfite"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.phDown = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dry Acid, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.ddryacid = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dry Acid, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dry Acid, "
          end
        end
      end

      if @autoval["sodium_bicarbonate"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["sodium_bicarbonate"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["sodium_bicarbonate"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["sodium_bicarbonate"].to_d/16
          else
            @tempf = @autoval["sodium_bicarbonate"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.taUp = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.bbakingsoda = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf.to_i, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of sodium bicarbonate, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq.to_i, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of sodium bicarbonate, "
          end
        end
      end

      if @autoval["trichlor"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["trichlor"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @tempf = @autoval["trichlor"].to_d
        end
        @dsg.trichlor = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @tablets = getunits(@tempq, @tablets)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@tablets} of trichlor, "
        elsif @tempf > 0 && @tempq <= 0
          @tablets = getunits(@tempf, @tablets)
          @text += " "+@tempf.to_s+" #{@tablets} of trichlor, "
        elsif @tempf <= 0 && @tempq > 0
          @tablets = getunits(@tempq, @tablets)
          @text += " "+@tempq.to_s+" #{@tablets} of trichlor, "
        end
      end

      if @autoval["lch6"]
        @tempf = 0
        @tempq = 0
        @count += 1
        #@tempauto = @autoval["lch6"].split('~')
        @t1 = @autoval["lch6"].split(";")
        @dunit = @t1[1]
        @tempauto = @t1[0].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["lch6"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @tempauto[0].to_d/16
          else
            @tempf = @tempauto[0].to_d
          end
        end

        if @dunit == "cup"
          @dunit = "cup"
        elsif @dunit == "gal"
          @dunit = "gallon"
        end

        @dsg.lch6unit = @dunit
        @dsg.lch6qunit = @dunit
        @dsg.liquidCl6 = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq, @unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Liquid Chlorine 6%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf, @unit)
          @text += " "+@tempf.to_s+" #{@unit} of Liquid Chlorine 6%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq, @unit)
          @text += " "+@tempq.to_s+" #{@unit} of Liquid Chlorine 6%, "
        end
      end

      if @autoval["lch12"]
        @tempf = 0
        @tempq = 0
        @count += 1
        #@tempauto = @autoval["lch12"].split('~')
        @t1 = @autoval["lch12"].split(";")
        @dunit = @t1[1]
        @tempauto = @t1[0].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["lch12"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @tempauto[0].to_d/16
          else
            @tempf = @tempauto[0].to_d
          end
        end

        if @dunit == "cup"
          @dunit = "cup"
        elsif @dunit == "gal"
          @dunit = "gallon"
        end

        @dsg.lch12unit = @dunit
        @dsg.lch12qunit = @dunit
        @dsg.liquidChlorine = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @unit = getunits(@tempq, @unit)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Liquid Chlorine 12%, "
        elsif @tempf > 0 && @tempq <= 0
          @unit = getunits(@tempf, @unit)
          @text += " "+@tempf.to_s+" #{@unit} of Liquid Chlorine 12%, "
        elsif @tempf <= 0 && @tempq > 0
          @unit = getunits(@tempq, @unit)
          @text += " "+@tempq.to_s+" #{@unit} of Liquid Chlorine 12%, "
        end
      end


      if @autoval["dichlor_62"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["dichlor_62"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["dichlor_62"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["dichlor_62"].to_d/16
          else
            @temp_arr = @autoval["dichlor_62"].split(";")
            @tablespoon = false
            if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
              @tablespoon = true
            end
            if @tablespoon
              @tempf = @autoval["dichlor_62"].to_d/16
            else
              @tempf = @autoval["dichlor_62"].to_d
            end
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.dichlor62Size = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          end
        else
          @dsg.dichlor62Size = @tempf.to_d/16+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Dichlor 62%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Dichlor 62%, "
          end
        end
      end

      if @autoval["chyp53"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["chyp53"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["chyp53"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["chyp53"].to_d/16
          else
            @tempf = @autoval["chyp53"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.calhypo53 = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.calhypo53 = @tempf.to_d/16+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 53%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 53%, "
          end
        end
      end

      if @autoval["chyp65"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["chyp65"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["chyp65"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["chyp65"].to_d/16
          else
            @tempf = @autoval["chyp65"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.calhypo65 = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.calhypo65 = @tempf.to_d/16+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @lbs)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 65%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @lbs)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 65%, "
          end
        end
      end

      if @autoval["chyp73"]
        @tempf = 0
        @tempq = 0
        @count += 1
        @tempauto = @autoval["chyp73"].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["chyp73"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @autoval["chyp73"].to_d/16
          else
            @tempf = @autoval["chyp73"].to_d
          end
        end
        if @pool.isPool || (@pool.isPool == false && @pool.spabox == false)
          @dsg.calhypo73 = @tempf.to_d+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          end
        elsif @pool.isPool == false && @pool.spabox
          @dsg.calhypo73 = @tempf.to_d/16+(@tempq.to_d*0.25)
          if @tempf > 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf > 0 && @tempq <= 0
            @unit = getunits(@tempf, @unit)
            @text += " "+@tempf.to_s+" #{@unit} of Cal-hypo 73%, "
          elsif @tempf <= 0 && @tempq > 0
            @unit = getunits(@tempq, @unit)
            @text += " "+@tempq.to_s+" #{@unit} of Cal-hypo 73%, "
          end
        end
      end

      if @autoval["muriatic_acid"]
        @tempf = 0
        @tempq = 0
        @count += 1
        #@tempauto = @autoval["muriatic_acid"].split('~')
        @t1 = @autoval["muriatic_acid"].split(";")
        @dunit = @t1[1]
        @tempauto = @t1[0].split('~')
        if @tempauto.length > 1
          @tempf = @tempauto[0].to_d
          @tempq = @tempauto[1].to_d
        else
          @temp_arr = @autoval["muriatic_acid"].split(";")
          @tablespoon = false
          if @temp_arr.length > 0 && @temp_arr[1] == "tbs"
            @tablespoon = true
          end
          if @tablespoon
            @tempf = @tempauto[0].to_d/16
          else
            @tempf = @tempauto[0].to_d
          end
        end

        if @dunit == "cup"
          @dunit = "cup"
        elsif @dunit == "gal"
          @dunit = "gallon"
        end

        @dsg.macid31unit = @dunit
        @dsg.macid31qunit = @dunit
        @dsg.muriatic31 = @tempf.to_d+(@tempq.to_d*0.25)
        if @tempf > 0 && @tempq > 0
          @gallons = getunits(@tempq, @gallons)
          @text += " "+@tempf.to_s+" and "+@tempq.to_s+" #{@gallons} of muriatic acid, "
        elsif @tempf > 0 && @tempq <= 0
          @gallons = getunits(@tempf, @gallons)
          @text += " "+@tempf.to_s+" #{@gallons} of muriatic acid, "
        elsif @tempf <= 0 && @tempq > 0
          @gallons = getunits(@tempq, @gallons)
          @text += " "+@tempq.to_s+" #{@gallons} of muriatic acid, "
        end
      end

      @mtest = @pool.mtests.last
      if @mtest.blank? == false && @mtest.freeCl.blank? == false && @mtest.freeCl.to_d < 3 && @pool.waterChemistry == "Salt Water"
        @count += 1
      end

      if @count > 0
        deletealldosages(@pool)
        @phdosage = nil
        @orpdosage = nil

        @epool = nil #Epool.find_by_pool_id(@pool.id)
        if @epool
          @point = Point.find_by_sql("select * from points where epool_id = "+@epool.id.to_s+" and extAddr = '"+@epool.extAddr+"' and date(created_at) <= DATE(NOW()) order by created_at desc limit 1")
          @point.each do |point|
            if @epool.device_type == "L"
              @phdosage = phlorasingle(@epool, point)
              @orpdosage = orplorasingle(@epool, point)
            else
              @phdosage = phsingle(@epool, point)
              @orpdosage = orpsingle(@epool, point)
            end
          end
        end

        if @mtest.blank? == false && @mtest.freeCl.blank? == false && @mtest.freeCl.to_d < 3 && @pool.waterChemistry == "Salt Water"
          @dsg.saltwater = true
        end
        #dsaf
        @dsg.ph = @phdosage
        @dsg.orp = @orpdosage
        @dsg.pool_id = @pool.id
        @dsg.customer_id = @pool.customer_id
        @dsg.status = false
        @dsg.approved = true
        @dsg.save(@pool)

        # senddosagependingsms(@dsg, @pool)

        #process chemical low check
        #notifylowstock("create",@pool,@dsg)
        #chemical low check ends

        #@dos = Dosage.last
        #customerdone(@dos.id)
      end

      #notify administrator if the pool in bad state and no prescription generated
      # notifyadminofthestate(@pool)

      return generatedosagetext(@dsg, @pool)
    end

    def senddosagependingsms(dosage, pool)
      @dosagehere = dosage
      @poolhere = pool
      # @customer = Customer.find(@poolhere.customer_id)
      # @role_id = User.where(email: @customer.email).limit(1).pluck(:role_id)
      # @user_role = Role.where(id: @role_id).limit(1).pluck(:role_name)
      @user_role = 'ANY'
      @message = nil
      if @poolhere.isPool
        @message = Message.find_by_short("POOL_DOSAGE")
      else
        @message = Message.find_by_short("SPA_DOSAGE")
      end
      if @user_role[0] == "BetaCustomer"
        customerdonedosage(@dosagehere.id)
        Rails.logger.info("APPROVE BETA DOSAGE SAVED")
        if @dosagehere.prescription.blank? == false
          @smscontent = @dosagehere.prescription
        else
          @smscontent = generatedosagetext(@dosagehere, @poolhere)

          if @message.content.blank?
            if @poolhere.isPool
              @smscontent = "Please add "+@smscontent+" around pool"
            else
              @smscontent = "Please put in "+@smscontent+" around spa"
            end
          else
            if @smscontent.blank? && @dosagehere.saltwater
              @smscontent = "Please [saltwater]"
            else
              @smscontent = @message.content.gsub! "[prescription]", @smscontent
            end
          end
        end
        if @message != nil &&@message.status
          #add chemical low text to the prescription content
          if @user_role[0] == "BetaCustomer"
            @smscontent = @smscontent+" "+notifylowstock("done", @poolhere, @dosagehere)
          else
            @smscontent = @smscontent+" "+notifylowstock("create", @poolhere, @dosagehere)
          end
          sendpressms(@smscontent, @poolhere)
          Rails.logger.info("SMS SENT")
        else
          if Rails.application.config.siteurl == "http://localhost:3000" || Rails.application.config.siteurl == "http://sutrodev-env.elasticbeanstalk.com"
            SesMailer.customeremail("andrew@mysutro.com,support@mysutro.com,gowtham@tentsoftware.com", "DEV - Dosage Text SMS content for #{@customer.email}", @smscontent).deliver
          else
            SesMailer.customeremail("andrew@mysutro.com,support@mysutro.com,gowtham@tentsoftware.com", "PROD - Dosage Text SMS content for #{@customer.email}", @smscontent).deliver
          end
        end
      else
        sendsms('DOSING_NEEDED', @poolhere)
      end
    end

    def deletealldosages(pool)
      Rails.logger.info("DELETING ALL PRESCRIPTIONS")
      # @d = Dosage.where(pool_id: pool.id, customer_id: pool.customer_id, status: 0).delete_all
      Rails.logger.info("DELETING ALL PRESCRIPTIONS DONE")
    end

    def apiprescget(data)
      @tempparams = data
      puts @tempparams
      @output = ""
      if @tempparams["phone"].blank? == false
        @ph = @tempparams["ph"].to_d
        @chlorine = @tempparams["chlorine"].to_d
        if @tempparams["ta"].blank? || @tempparams["ta"].downcase == "skip"
          @ta = nil
        else
          @ta = @tempparams["ta"].to_d
        end
        Rails.logger.info("PHONE PARAM : "+@tempparams["phone"].to_s)
        @phone = getonlynumbers(params["phone"])
        Rails.logger.info("PHONE CONVR : "+@phone.to_s)
        @customer = Customer.find_by_sql("select * from customers where replace(replace(replace(replace(replace(replace(replace(mPhone,' ',''),'-',''),'(',''),')',''),'.',''),' ',''),'+1','') = '"+@phone+"'")
        if @customer.blank? == false
          @pool = Pool.find_by_customer_id(@customer[0]["id"])
          if @pool.blank? == false
            Rails.logger.info("PH : "+@ph.to_s)
            Rails.logger.info("ORP : "+@chlorine.to_s)
            Rails.logger.info("TA : "+@ta.to_s)
            @mtest = Mtest.new
            @mtest.pool_id = @pool.id
            @mtest.pH = @ph
            @mtest.freeCl = @chlorine
            @mtest.totalAlkalinity = @ta

            if @mtest.save
              if @pool.isPool
                if (@ph.to_d < 7.2 || @ph.to_d > 7.6) || @chlorine.to_d <= 2
                  @output = pooldosagecalc(@pool.id)
                  generatedosagetextforbeta(@output)
                else
                  @output = "poolgood"
                end
              else
                if (@ph.to_d < 7.2 || @ph.to_d > 7.6) || @chlorine.to_d <= 3
                  @output = spadosagecalc(@pool.id)
                  generatedosagetextforbeta(@output)
                else
                  @output = "spagood"
                end
              end
              sendbetamtestemail("BETA_MANUAL_TEST", @pool)
            end
          end
        end #if customer blank
      end #if email blank
      puts @output
      #respond_to do |format|
      render :json => @output
      #end
    end

    #for other controllers to access dosage module using pool id
    def populatepresc(data)
      @tempparams = data
      puts @tempparams
      @output = ""
      if @tempparams["pool_id"].blank? == false
        @ph = 0
        @orp = 0
        @ta = 0
        if @tempparams["ph"].blank? == false
          @ph = @tempparams["ph"].to_d
        end

        if @tempparams["orp"].blank? == false
          @orp = @tempparams["orp"].to_d
        end

        if @tempparams["ta"].blank? == false
          @ta = @tempparams["ta"].to_d
        end

        @pool = Pool.find(@tempparams["pool_id"])
        if @pool.blank? == false
          if @pool.isPool
            @output = pooldosagecalc(@pool.id)
          else
            @output = spadosagecalc(@pool.id)
          end
          generatedosagetextforbeta(@output)
        end
      end #if pool id blank
      puts "POPULATED AUTO PRESC : "+@output.inspect
      #respond_to do |format|
      #render :json => @output
      return @output
      #end
    end


    def getloradayformat(date)
      #@day = date.strftime("%A")
      #@today = Date.todaystrftime("%A")
      @ret = ""
      @diff = ((Time.zone.now - date) / 1.day).to_i
      if @diff.blank? == false
        if @diff == 0
          @ret = "Today"
        elsif @diff == 1
          @ret = "Yesterday"
        elsif @diff == 2
          @ret = "2 days ago"
        elsif @diff == 3
          @ret = "3 days ago"
        elsif @diff == 4
          @ret = "4 days ago"
        elsif @diff == 5
          @ret = "5 days ago"
        elsif @diff > 5 && @diff <= 29
          @ret = "a month ago"
        elsif @diff > 29 && @diff < 365
          @ret = (@diff/31).to_s+" months ago"
        elsif @diff > 365
          @ret = (@diff/365).to_s+" years ago"
        end
      end
      return @ret
    end

    def getbetadayformat(date)
      #@day = date.strftime("%A")
      #@today = Date.todaystrftime("%A")
      @ret = ""
      @diff = ((Time.zone.now - date) / 1.day).to_i
      if @diff.blank? == false
        if @diff == 0
          @ret = "Today"
        elsif @diff == 1
          @ret = "Yesterday"
        elsif @diff == 2
          @ret = "2 Days ago"
        elsif @diff == 3
          @ret = "3 Days ago"
        elsif @diff == 4
          @ret = "4 Days ago"
        elsif @diff == 5
          @ret = "5 Days ago"
        elsif @diff > 5 && @diff <= 31
          @ret = "A Month ago"
        elsif @diff > 31 && @diff < 365
          if @diff/31 > 1
            @ret = (@diff/31).to_s+" Months ago"
          else
            @ret = "A Month ago"
          end
        elsif @diff > 365
          @ret = (@diff/365).to_s+" Years ago"
        end
      end
      return @ret
    end

    def getchemlength(pool)
      @pool = pool
      @count = 0
      if @pool.trichlor2 && @pool.trichlor2Size.nil? == false && @pool.trichlor2Size.to_d > 0
        @count += 1
      end

      if @pool.bromine1 && @pool.bromine1Size.nil? == false && @pool.bromine1Size.to_d > 0
        @count += 1
      end

      if @pool.lithiumhypo && @pool.lithiumhypoSize.nil? == false && @pool.lithiumhypoSize.to_d > 0
        @count += 1
      end

      if @pool.calcium && @pool.calciumSize.nil? == false && @pool.calciumSize.to_d > 0
        @count += 1
      end

      if @pool.cyanuricsolid && @pool.cyanuricsolidSize.nil? == false && @pool.cyanuricsolidSize.to_d > 0
        @count += 1
      end

      if @pool.cyanuricliquid && @pool.cyanuricliquidSize.nil? == false && @pool.cyanuricliquidSize.to_d > 0
        @count += 1
      end

      if @pool.spabox && @pool.esanitizer.nil? == false && @pool.esanitizer.to_d > 0
        @count += 1
      end

      if @pool.spabox && @pool.ddryacid.nil? == false && @pool.ddryacid.to_d > 0
        @count += 1
      end

      if @pool.spabox && @pool.bbakingsoda.nil? == false && @pool.bbakingsoda.to_d > 0
        @count += 1
      end

      if @pool.spabox && @pool.bsodaash.nil? == false && @pool.bsodaash.to_d > 0
        @count += 1
      end
      if @pool.trichlor3 && @pool.trichlor3Size.nil? == false && @pool.trichlor3Size.to_d > 0
        @count += 1
      end

      if @pool.sodaash && @pool.sodaashSize.nil? == false && @pool.sodaashSize.to_d > 0
        @count += 1
      end

      if @pool.dryacid && @pool.dryacidSize.nil? == false && @pool.dryacidSize.to_d > 0
        @count += 1
      end

      if @pool.sodium && @pool.sodiumSize.nil? == false && @pool.sodiumSize.to_d > 0
        @count += 1
      end

      if @pool.bleach6 && @pool.bleach6Size.nil? == false && @pool.bleach6Size.to_d > 0
        @count += 1
      end

      if @pool.bleach8 && @pool.bleach8Size.nil? == false && @pool.bleach8Size.to_d > 0
        @count += 1
      end

      if @pool.bleach12 && @pool.bleach12Size.nil? == false && @pool.bleach12Size.to_d > 0
        @count += 1
      end

      if @pool.dichlor && @pool.dichlorSize.nil? == false && @pool.dichlorSize.to_d > 0
        @count += 1
      end

      if @pool.dichlor62 && @pool.dichlor62Size.nil? == false && @pool.dichlor62Size.to_d > 0
        @count += 1
      end

      if @pool.calhypo53 && @pool.calhypo53Size.nil? == false && @pool.calhypo53Size.to_d > 0
        @count += 1
      end

      if @pool.calhypo65 && @pool.calhypo65Size.nil? == false && @pool.calhypo65Size.to_d > 0
        @count += 1
      end

      if @pool.calhypo73 && @pool.calhypo73Size.nil? == false && @pool.calhypo73Size.to_d > 0
        @count += 1
      end

      if @pool.borax && @pool.boraxSize.nil? == false && @pool.boraxSize.to_d > 0
        @count += 1
      end

      if @pool.muriatic15 && @pool.muriatic15Size.nil? == false && @pool.muriatic15Size.to_d > 0
        @count += 1
      end

      if @pool.muriatic31 && @pool.muriatic31Size.nil? == false && @pool.muriatic31Size.to_d > 0
        @count += 1
      end
      return @count
    end


    #return last used chlorine
    def getlastusedchlorine(pool)
      @pool = pool
      @lastdose = pool.dosages.last
      if @lastdose.blank? == false
        if pool.isPool == false
          if @lastdose.liquidCl6.blank? == false && @lastdose.liquidCl6.to_d > 0
            @Cl_type= 5
          elsif @lastdose.liquidChlorine.blank? == false && @lastdose.liquidChlorine.to_d > 0
            @Cl_type= 4
          elsif (@lastdose.calhypo53.blank? == false && @lastdose.calhypo53.to_d > 0) || (@lastdose.calhypo65.blank? == false && @lastdose.calhypo65.to_d > 0) || (@lastdose.calhypo73.blank? == false && @lastdose.calhypo73.to_d > 0)
            @Cl_type= 0
          elsif (@lastdose.dichlor.blank? == false && @lastdose.dichlor.to_d > 0) || (@lastdose.esanitizer.blank? == false && @lastdose.esanitizer.to_d > 0)
            @Cl_type= 1
          elsif @lastdose.dichlor62Size.blank? == false && @lastdose.dichlor62Size.to_d > 0
            @Cl_type= 2
          elsif @lastdose.trichlor.blank? == false && @lastdose.trichlor.to_d > 0
            @Cl_type= 3
          else
            @Cl_type = 0
          end
        else
          if @lastdose.liquidCl6.blank? == false && @lastdose.liquidCl6.to_d > 0
            @Cl_type= 5
          elsif @lastdose.liquidChlorine.blank? == false && @lastdose.liquidChlorine.to_d > 0
            @Cl_type= 4
          elsif (@lastdose.calhypo53.blank? == false && @lastdose.calhypo53.to_d > 0) || (@lastdose.calhypo65.blank? == false && @lastdose.calhypo65.to_d > 0) || (@lastdose.calhypo73.blank? == false && @lastdose.calhypo73.to_d > 0)
            @Cl_type= 0
          elsif @lastdose.dichlor.blank? == false && @lastdose.dichlor.to_d > 0
            @Cl_type= 1
          elsif @lastdose.dichlor62Size.blank? == false && @lastdose.dichlor62Size.to_d > 0
            @Cl_type= 2
          elsif @lastdose.trichlor.blank? == false && @lastdose.trichlor.to_d > 0
            @Cl_type= 3
          else
            @Cl_type = 0
          end
        end
      end

      return @Cl_type
    end

    #return last used acid
    def getlastusedacid(pool)
      @pool = pool
      @lastdose = Dosage.where(:pool_id => @pool.id).last
      @type = 0
      if @lastdose.blank? == false
        if @lastdose.phDown.blank? == false && @lastdose.phDown.to_d > 0
          @type = 1
        elsif @lastdose.muriatic31.blank? == false && @lastdose.muriatic31.to_d > 0
          @type = 2
        end
      end
      return @type
    end

    def notifyadminofthestate(pool)
      @pool = pool
      @customer = Customer.find(@pool.customer_id)
      @role_name = getuserrole(@customer)

      #decide range
      @orp = 0
      @ph = 0
      @ta_i_val = ""

      #Fetch manual readings
      @mreading = @pool.mtests.last
      @daysdiff = ""
      @reading_type = ""
      if @mreading.blank? == false
        @reading_type = "manual"
        if @mreading.pH.blank? == false
          @ph = @mreading.pH
        end
        if @mreading.freeCl.blank? == false
          @free_cl = @mreading.freeCl.to_d
        end
        if @mreading.totalAlkalinity.blank? == false
          @ta_i_val = @mreading.totalAlkalinity.to_d
        end
        @daysdiff = ((Time.now - @mreading.created_at)/1.day).round
      end
      if @daysdiff.blank? || (@daysdiff.blank? == false && @daysdiff >= 3)
        @reading_type = "device"
        unless @pool.epools.blank?
          @pool.epools.each do |epool|
            @orp = epool.oOrp
            unless epool.points.last.blank? || epool.nil?
              @ph = ph(epool)
              @orp = orp(epool)
              @daysdiff = ((Time.now - epool.points.last.created_at)/1.day).round
              #Rails.logger.info("DAYSDIFF : "+@daysdiff.to_s)
            end # if points check
          end # looping of devices
        end # if pool has device check
      end

      if @daysdiff.blank? == false && @daysdiff >= 3
        Rails.logger.info("NOTIFYADMIN - Returning as readings are greater than 3 days")
        return ""
      end

      #decide range of the system and what to be prescribed
      @range = "notgood"
      @phrange = "notgood"
      @chlrange = "notgood"
      @tarange = ""


      if @role_name == "Lora"
        #ph decider
        if @ph.to_d >= 7.2 && @ph.to_d <= 7.8
          @phrange = "good"
          Rails.logger.info("NOTIFYADMIN - PH RANGE GOOD")
        end
        #chlorine decider
        if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 600 && @orp.to_d <= 900))
          @chlrange = "good"
          Rails.logger.info("NOTIFYADMIN - CH RANGE GOOD")
        end
        #ta decider
        if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
          @tarange = "good"
          Rails.logger.info("NOTIFYADMIN - TA RANGE GOOD")
        elsif @ta_i_val.blank? == false
          @tarange = "notgood"
          Rails.logger.info("NOTIFYADMIN - TA RANGE NOT GOOD")
        end
      else
        #ph decider
        if @ph.to_d >= 7.4 && @ph.to_d <= 7.6
          @phrange = "good"
          Rails.logger.info("NOTIFYADMIN - PH RANGE GOOD")
        end
        #chlorine decider
        if ((@free_cl.blank? == false && @free_cl.to_d >= 3) || (@orp.blank? == false && @orp.to_d >= 650 && @orp.to_d <= 900))
          @chlrange = "good"
          Rails.logger.info("NOTIFYADMIN - CH RANGE GOOD")
        end
        #ta decider
        if @ta_i_val.blank? == false && @ta_i_val > 70 && @ta_i_val <= 90
          @tarange = "good"
          Rails.logger.info("NOTIFYADMIN - TA RANGE GOOD")
        elsif @ta_i_val.blank? == false
          @tarange = "notgood"
          Rails.logger.info("NOTIFYADMIN - TA RANGE NOT GOOD")
        end
      end

      #range decider ends

      if @phrange == "good" && @chlrange == "good" && (@tarange == "good" || @tarange == "")
        deletealldosages(@pool)
        Rails.logger.info("NOTIFYADMIN - Returning as system is in good range")
        return ""
      else
        Rails.logger.info("NOTIFYADMIN - NOT GOOD RANGE")
        @dosage = Dosage.where(pool_id: @pool.id).where(status: false).limit(1)
        @count = getnoofchemicalsprescribed(@dosage[0])
        Rails.logger.info("NOTIFYADMIN - CHEMICALS COUNT: "+@count.to_s)

        if @count <= 0
          #send admin notification
          sendnotgoodnodosageemail("BAD_STATE_NO_DOSAGE", @pool, @reading_type)
        end
      end
    end

    #return no of chemicals prescribed in a dosage
    def getnoofchemicalsprescribed(dosage)
      @count = 0
      if dosage.blank? == false
        if dosage.trichlor2Size.blank? == false
          @count += 1
        end
        if dosage.bromine1Size.blank? == false
          @count += 1
        end
        if dosage.lithiumhypoSize.blank? == false
          @count += 1
        end
        if dosage.calciumSize.blank? == false
          @count += 1
        end
        if dosage.cyanuricsolidSize.blank? == false
          @count += 1
        end
        if dosage.cyanuricliquidSize.blank? == false
          @count += 1
        end

        #Extra chemicals section ends

        if dosage.trichlor.blank? == false
          @count += 1
        end
        if dosage.phUp.blank? == false
          @count += 1
        end
        if dosage.phDown.blank? == false
          @count += 1
        end
        if dosage.taUp.blank? == false
          @count += 1
        end
        if dosage.liquidCl6.blank? == false
          @count += 1
        end
        if dosage.liquidCl8.blank? == false
          @count += 1
        end
        if dosage.liquidChlorine.blank? == false
          @count += 1
        end
        if dosage.dichlor.blank? == false
          @count += 1
        end
        if dosage.dichlor62Size.blank? == false
          @count += 1
        end
        if dosage.calhypo53.blank? == false
          @count += 1
        end
        if dosage.calhypo65.blank? == false
          @count += 1
        end
        if dosage.calhypo73.blank? == false
          @count += 1
        end
        if dosage.phUpBorax.blank? == false
          @count += 1
        end
        if dosage.muriatic15.blank? == false
          @count += 1
        end
        if dosage.muriatic31.blank? == false
          @count += 1
        end

        #For spa
        if dosage.esanitizer.blank? == false
          @count += 1
        end
        if dosage.ddryacid.blank? == false
          @count += 1
        end
        if dosage.bbakingsoda.blank? == false
          @count += 1
        end

        if dosage.bsodaash.blank? == false
          @count += 1
        end
      end

      return @count
    end


    def sendnotgoodnodosageemail(emailid, pool, readingtype)
      @emails = Emails.find_by_short(emailid)
      if @emails.status
        @customer = Customer.find(pool.customer_id)

        @user = User.find_by_email(@customer.email)

        @greeting = @customer.firstName+" "+@customer.lastName
        @greeting_first = @customer.firstName
        @greeting_last = @customer.lastName
        @email = @customer.email
        @phone = @customer.mPhone

        @ph = ""
        @orp = ""
        @freeCl = ""
        @ta = ""
        @temprtr = ""

        if readingtype == "device"
          unless pool.epools.blank?
            pool.epools.each do |epool|
              unless epool.points.last.blank? || epool.nil?
                @ph = ph(epool)
                @orp = orp(epool)
                @temprtr = temperature(epool)
              end
            end
          end
        elsif readingtype == "manual"
          @manual_measurement = pool.mtests.last
          if @manual_measurement.nil? == false
            @ph = @manual_measurement.pH
            @freeCl = @manual_measurement.freeCl
            @ta = @manual_measurement.totalAlkalinity
          end
        end
        @pool_url = Rails.application.config.siteurl+url_for("/trguytbyhe/encs?app="+Base64.encode64("?customer_id="+@customer.id.to_s+"?pool_id="+pool.id.to_s))
        @subject = @emails.subject.to_s
        @subject = @subject.gsub("[greeting]", @greeting)
        @subject = @subject.gsub("[greeting_first]", @greeting_first)
        @subject = @subject.gsub("[greeting_last]", @greeting_last)

        @content = @emails.content
        @content = @content.to_s
        @content = @content.gsub("[greeting]", @greeting)
        @content = @content.gsub("[greeting_first]", @greeting_first)
        @content = @content.gsub("[greeting_last]", @greeting_last)
        @content = @content.gsub("[email]", @email)
        @content = @content.gsub("[phone]", @phone.to_s)

        @reading_details = ""
        if readingtype == "device"
          @reading_details += "<b><h4>Device readings</h4></b>"
          @reading_details += "<p><b>pH : </b>"+@ph.to_s
          @reading_details += "</p><p><b>ORP : </b>"+@orp.to_s
          @reading_details += "</p><p><b>Temperature : </b>"+@temprtr.to_s+"</p>"
        elsif readingtype == "manual"
          @reading_details += "<b><h4>Manual readings</h4></b>"
          @reading_details += "<p><b>pH : </b>"+@ph.to_s
          @reading_details += "</p><p><b>Free Chlorine : </b>"+@freeCl.to_s
          @reading_details += "</p><p><b>Total Alkalinity : </b>"+@ta.to_s+"</p>"
        end

        @content = @content.gsub("[readingdetails]", @reading_details)
        @content = @content.gsub("[url]", @pool_url)

        SesMailer.customeremail(@emails.toaddress, @subject, @content).deliver
      else
        smslogger.info "===========Email not sent. #{emailid}  email is  not enabled================"
      end
    end


    #function adds the contact to the low contact list
    def updatelowstockcontact(emailid, content)
      Rails.logger.info("Trigger for Low Stock "+emailid)
      @details = getchemicallowcontentfortrigger(content)

      @list_id = "contactlist_1D3FBC5E-706F-4FEC-B414-047439DD7726" # Prefix "contactlist_" to the list id

      @body = '{
			"contact":{
				"FirstName":"",
				"LastName": "",
			    "Email": "'+emailid+'",
			    "custom": {
			      "string--LowChem1": "'+@details[0]+'",
			      "string--LowChem2": "'+@details[1]+'",
			      "string--LowChem3": "'+@details[2]+'",
			      "string--LowChem4": "'+@details[3]+'",
			      "string--LowChem5": "'+@details[4]+'"
			    }
			}
		}'
      response = HTTParty.post("https://api2.autopilothq.com/v1/contact", :headers => {"autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7", "Content-Type" => "application/json"}, :body => @body, :verify => false)
      #puts response
      response = HTTParty.post("https://api2.autopilothq.com/v1/list/"+@list_id+"/contact/"+emailid, :headers => {"autopilotapikey" => "b6f2e3661bc04743a2e401da6af403d7", "Content-Type" => "application/json"}, :verify => false)
      #puts response
    end

    #process the raw low chemical content and make it for tirgger
    def getchemicallowcontentfortrigger(content)
      @contenthere = content.split("~")
      @text = []
      @iter = 0
      @contenthere.each_with_index do |chem, index|
        if chem != ""
          #puts chem.inspect
          @chem_name = chem.split(":")[0]
          #puts @chem_name
          if @chem_name == "Trichlor"
            @text[@iter] = "chlor-T1"
          elsif @chem_name == "Bromine"
            @text[@iter] = "chlor-TB3"
          elsif @chem_name == "Lithium Hypo"
            @text[@iter] = "LiShock-G"
          elsif @chem_name == "Calcium"
            @text[@iter] = "chup-G"
          elsif @chem_name == "Cyanuric Solid"
            @text[@iter] = "CYA"
          elsif @chem_name == "Cyanuric Liquid"
            @text[@iter] = "STB"
          elsif @chem_name == "Trichlor 3"
            @text[@iter] = "chlor-T3"
          elsif @chem_name == "Soda Ash"
            @text[@iter] = "pHup-A"
          elsif @chem_name == "Dry Acid"
            @text[@iter] = "pHdown-G"
          elsif @chem_name == "Sodium"
            @text[@iter] = "BiCarb-G"
          elsif @chem_name == "Liquid Chlorine 6%"
            @text[@iter] = "chlor-L6"
          elsif @chem_name == "Liquid Chlorine 8%"
            @text[@iter] = "chlor-L8"
          elsif @chem_name == "Liquid Chlorine 12%"
            @text[@iter] = "chlor-L12"
          elsif @chem_name == "Dichlor"
            @text[@iter] = "chlor-G56"
          elsif @chem_name == "Dichlor 62"
            @text[@iter] = "chlor-G62"
          elsif @chem_name == "Cal Hypo 53"
            @text[@iter] = "CalShock-G53"
          elsif @chem_name == "Cal Hypo 65"
            @text[@iter] = "CalShock-G65"
          elsif @chem_name == "Cal Hypo 73"
            @text[@iter] = "CalShock-G73"
          elsif @chem_name == "Borax"
            @text[@iter] = "pHup-B"
          elsif @chem_name == "Muriatic Acid 15%"
            @text[@iter] = "pHdown-L15"
          elsif @chem_name == "Muriatic Acid 31%"
            @text[@iter] = "pHdown-L31"
          elsif @chem_name == "Packet E"
            @text[@iter] = "SpaChlor"
          elsif @chem_name == "Packet D"
            @text[@iter] = "SpaDown"
          elsif @chem_name == "Packet B"
            @text[@iter] = "SpaUp"
          elsif @chem_name == "Packet A"
            @text[@iter] = "SpaTA"
          end

          @iter += 1
        end
      end
      if @iter < 5
        while @iter <= 4 do
          @text[@iter] = ""
          @iter += 1
        end
      end

      return @text
    end

    #process the chemical and return its type
    def getchemicallowtype(chemical)
      @chem_name = chemical
      if @chem_name != ""
        if @chem_name == "Trichlor"
          @text = "Sanitizer"
        elsif @chem_name == "Bromine"
          @text = "Sanitizer"
        elsif @chem_name == "Lithium Hypo"
          @text = "Sanitizer"
        elsif @chem_name == "Calcium"
          @text = "Calcuim Hardness Increaser"
        elsif @chem_name == "Cyanuric Solid"
          @text = "Stabilizer"
        elsif @chem_name == "Cyanuric Liquid"
          @text = "Stabilizer"
        elsif @chem_name == "Trichlor 3"
          @text = "Sanitizer"
        elsif @chem_name == "Soda Ash"
          @text = "pH Up"
        elsif @chem_name == "Dry Acid"
          @text = "pH Down"
        elsif @chem_name == "Sodium"
          @text = "TA Up"
        elsif @chem_name == "Liquid Chlorine 6%"
          @text = "Sanitizer"
        elsif @chem_name == "Liquid Chlorine 8%"
          @text = "Sanitizer"
        elsif @chem_name == "Liquid Chlorine 12%"
          @text = "Sanitizer"
        elsif @chem_name == "Dichlor"
          @text = "Sanitizer"
        elsif @chem_name == "Dichlor 62"
          @text = "Sanitizer"
        elsif @chem_name == "Cal Hypo 53"
          @text = "Sanitizer"
        elsif @chem_name == "Cal Hypo 65"
          @text = "Sanitizer"
        elsif @chem_name == "Cal Hypo 73"
          @text = "Sanitizer"
        elsif @chem_name == "Borax"
          @text = "pH Up"
        elsif @chem_name == "Muriatic Acid 15%"
          @text = "pH Down"
        elsif @chem_name == "Muriatic Acid 31%"
          @text = "pH Down"
        elsif @chem_name == "Packet E"
          @text = "SpaChlor"
        elsif @chem_name == "Packet D"
          @text = "SpaDown"
        elsif @chem_name == "Packet B"
          @text = "SpaUp"
        elsif @chem_name == "Packet A"
          @text = "SpaTA"
        end
      end

      return @text
    end

    #process the chemical and return its type
    #this method is specifically used for generating the dosage text def generatedosagetext()
    def getchemicaltype(pool, chemical)
      @sanitizer = "Pool Shock"
      @phup = "pH Up"
      @phdown = "pH Down"
      @taup = "TA Up"
      @san = "Pool Shock"
      if pool.isPool == false
        @san = "Sanitizer"
        @sanitizer = "E"
        @phup = "B"
        @phdown = "D"
        @taup = "A"
      end
      @chem_name = chemical
      if @chem_name != ""
        if @chem_name == "Trichlor"
          @text = "Sanitizer"
        elsif @chem_name == "Bromine"
          @text = "Sanitizer"
        elsif @chem_name == "Lithium Hypo"
          @text = "Sanitizer"
        elsif @chem_name == "Calcium chloride"
          @text = "Calcuim Hardness Increaser"
        elsif @chem_name == "Cyanuric Acid Solid"
          @text = "Stabilizer"
        elsif @chem_name == "Cyanuric Acid Liquid"
          @text = "Stabilizer"
        elsif @chem_name == "Trichlor 3"
          @text = "Sanitizer"
        elsif @chem_name == "Soda Ash"
          @text = "pH Up"
        elsif @chem_name == "Dry Acid" || @chem_name == "Vinegar"
          @text = "pH Down"
        elsif @chem_name == "Sodium Bicarbonate"
          @text = "TA Up"
        elsif @chem_name == "6% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
          @text = "Liquid Chlorine"
        elsif @chem_name == "8% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
          @text = "Liquid Chlorine"
        elsif @chem_name == "12% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
          @text = "Liquid Chlorine"
        elsif @chem_name == "56% Dichlor" || @chem_name == "Dichlor"
          @text = @san
        elsif @chem_name == "62% Dichlor" || @chem_name == "Dichlor"
          @text = @san
        elsif @chem_name == "53% Cal-hypo" || @chem_name == "Cal-hypo"
          @text = @san
        elsif @chem_name == "65% Cal-hypo" || @chem_name == "Cal-hypo"
          @text = @san
        elsif @chem_name == "73% Cal-hypo" || @chem_name == "Cal-hypo"
          @text = @san
        elsif @chem_name == "Borax"
          @text = "pH Up"
        elsif @chem_name == "15.7% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "15% Muriatic Acid"
          @text = "pH Down"
        elsif @chem_name == "31% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "31.45% Muriatic Acid"
          @text = "pH Down"
        elsif @chem_name == "E"
          @text = @sanitizer
        elsif @chem_name == "D"
          @text = @phdown
        elsif @chem_name == "B"
          @text = @phup
        elsif @chem_name == "A"
          @text = @taup
        end
      end

      return @text
    end

    #process the chemical and return its type
    #this method is specifically used for generating the dosage text def generatedosagetext()
    def getchemicaltypeforskin(pool, chemical)
      @chem_name = chemical
      @san = "Pool Shock"
      @sanitizer = "Pool Shock"
      @phup = "pH Up"
      @phdown = "pH Down"
      @taup = "TA Up"
      @skin = @pool.chemical_skin
      if pool.isPool == false
        @san = "Sanitizer"
        @sanitizer = "E"
        @phup = "B"
        @phdown = "D"
        @taup = "A"
      end
      if @chem_name != ""
        if @skin == "basic"
          if @chem_name == "Trichlor"
            @text = "Sanitizer"
          elsif @chem_name == "Bromine"
            @text = "Sanitizer"
          elsif @chem_name == "Lithium Hypo"
            @text = "Sanitizer"
          elsif @chem_name == "Calcium chloride"
            @text = "Calcuim Hardness Increaser"
          elsif @chem_name == "Cyanuric Acid Solid"
            @text = "Stabilizer"
          elsif @chem_name == "Cyanuric Acid Liquid"
            @text = "Stabilizer"
          elsif @chem_name == "Trichlor 3"
            @text = "Sanitizer"
          elsif @chem_name == "Soda Ash"
            @text = "pH Up"
          elsif @chem_name == "Dry Acid" || @chem_name == "Vinegar"
            @text = "pH Down"
          elsif @chem_name == "Sodium Bicarbonate"
            @text = "TA Up"
          elsif @chem_name == "6% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
            @text = "Liquid Chlorine"
          elsif @chem_name == "8% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
            @text = "Liquid Chlorine"
          elsif @chem_name == "12% Liquid Chlorine" || @chem_name == "Liquid Chlorine"
            @text = "Liquid Chlorine"
          elsif @chem_name == "56% Dichlor" || @chem_name == "Dichlor"
            @text = @san
          elsif @chem_name == "62% Dichlor" || @chem_name == "Dichlor"
            @text = @san
          elsif @chem_name == "53% Cal-hypo" || @chem_name == "Cal-hypo"
            @text = @san
          elsif @chem_name == "65% Cal-hypo" || @chem_name == "Cal-hypo"
            @text = @san
          elsif @chem_name == "73% Cal-hypo" || @chem_name == "Cal-hypo"
            @text = @san
          elsif @chem_name == "Borax"
            @text = "pH Up"
          elsif @chem_name == "15.7% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "15% Muriatic Acid"
            @text = "pH Down"
          elsif @chem_name == "31% Muriatic Acid" || @chem_name == "Muriatic Acid" || @chem_name == "31.45% Muriatic Acid"
            @text = "pH Down"
          elsif @chem_name == "E"
            @text = @sanitizer
          elsif @chem_name == "D"
            @text = @phdown
          elsif @chem_name == "B"
            @text = @phup
          elsif @chem_name == "A"
            @text = @taup
          end
        elsif @skin == "advanced"
          @text = @chem_name
        end
      end

      return @text
    end

    #function returns chemical code for given chemical
    #this codes and chemical names are only used for refill button click redirect on beta dashboard
    def getchemicalcode(chemical)
      @text = ""
      if chemical != ""
        #puts chem.inspect
        @chem_name = chemical.downcase
        #puts @chem_name
        if @chem_name == "trichlor"
          @text = "chlor-T"
        elsif @chem_name == "bromine"
          @text = "chlor-TB"
        elsif @chem_name == "lithium hypo"
          @text = "lishock"
        elsif @chem_name == "calcium"
          @text = "chup-G"
        elsif @chem_name == "cyanuric solid"
          @text = "CYA"
        elsif @chem_name == "cyanuric liquid"
          @text = "STB"
        elsif @chem_name == "trichlor 3"
          @text = "chlor-T"
        elsif @chem_name == "soda ash"
          @text = "phup-A"
        elsif @chem_name == "dry acid"
          @text = "phdown-G"
        elsif @chem_name == "sodium"
          @text = "Bicarb-G"
        elsif @chem_name == "bleach 6%"
          @text = "chlor-L"
        elsif @chem_name == "bleach 8%"
          @text = "chlor-L"
        elsif @chem_name == "bleach 12%"
          @text = "chlor-L"
        elsif @chem_name == "dichlor"
          @text = "chlor-G"
        elsif @chem_name == "dichlor 62"
          @text = "chlor-G"
        elsif @chem_name == "cal-hypo 53"
          @text = "calshock"
        elsif @chem_name == "cal-hypo 65"
          @text = "calshock"
        elsif @chem_name == "cal-hypo 73"
          @text = "calshock"
        elsif @chem_name == "borax"
          @text = "phup-B"
        elsif @chem_name == "muriatic acid 15%"
          @text = "phdown-L"
        elsif @chem_name == "muriatic acid 31%"
          @text = "phdown-L"
        elsif @chem_name == "packets e"
          @text = "spachlor"
        elsif @chem_name == "packets d"
          @text = "spadown"
        elsif @chem_name == "packets b"
          @text = "spaup"
        elsif @chem_name == "packets a"
          @text = "spata"
        end
      end

      return @text
    end


    #check and notify about the chemical stock low
    def islowstock(pool)
      @content = ""
      @content = processchemstock(pool)
      return @content
    end

    #return beta upload directory
    def getsourceuploadfolder
      return "/local/PoolUploads/images"
    end

    #return beta upload directory
    def getdestinationuploadfolder
      return "/var/app/current/public/images/uploads"
    end

    #Remove from stock when manual dosing is done by a customer
    #this is used by ony senddosagependingsms function here
    def customerdonedosage(presc_id)
      @dosage = Dosage.find(presc_id)

      #Reduce dosage chemical amount from the customer warehouse
      @pool = Pool.find(@dosage.pool_id)

      #find measurement based on pool type [pool/spa]
      @unit = 1
      @nonSpaBoxUnit = 0.5
      @bleach = 16
      @muriatic = 16
      @soda_ash_cup = 0.574
      @dichlor = 0.5375 #1 cup == 0.5375 lb == 16 tbs
      @spabox = 16 #1lbs = oz
      # Spa
      @bsodaash_pac = 0.25
      @ddryacid_pac = 0.5
      @bbakingsoda_pac = 0.5
      @esanitizer_pac = 0.25

      if @pool.isPool
        @unit = 1
        @nonSpaBoxUnit = 1
      else
        @unit = 1
        @nonSpaBoxUnit = 1
      end


      #Store extra chemicals for spa details
      if @pool.trichlor2Size.blank? == false && @dosage.trichlor2Size.blank? == false
        @pool.trichlor2Size = @pool.trichlor2Size.to_d - (@dosage.trichlor2Size.to_d*0.0375)
        if @pool.trichlor2Size.to_d < 0
          @pool.trichlor2Size = 0
        end
      end
      if @pool.bromine1Size.blank? == false && @dosage.bromine1Size.blank? == false
        @pool.bromine1Size = @pool.bromine1Size.to_d - (@dosage.bromine1Size.to_d*0.03125)
        if @pool.bromine1Size.to_d < 0
          @pool.bromine1Size = 0
        end
      end
      if @pool.lithiumhypoSize.blank? == false && @dosage.lithiumhypoSize.blank? == false
        @pool.lithiumhypoSize = @pool.lithiumhypoSize.to_d - (@dosage.lithiumhypoSize.to_d)/@nonSpaBoxUnit
        if @pool.lithiumhypoSize.to_d < 0
          @pool.lithiumhypoSize = 0
        end
      end
      if @pool.calciumSize.blank? == false && @dosage.calciumSize.blank? == false
        @pool.calciumSize = @pool.calciumSize.to_d - (@dosage.calciumSize.to_d)/@nonSpaBoxUnit
        if @pool.calciumSize.to_d < 0
          @pool.calciumSize = 0
        end
      end
      if @pool.cyanuricsolidSize.blank? == false && @dosage.cyanuricsolidSize.blank? == false
        @pool.cyanuricsolidSize = @pool.cyanuricsolidSize.to_d - (@dosage.cyanuricsolidSize.to_d)/@nonSpaBoxUnit
        if @pool.cyanuricsolidSize.to_d < 0
          @pool.cyanuricsolidSize = 0
        end
      end
      if @pool.cyanuricliquidSize.blank? == false && @dosage.cyanuricliquidSize.blank? == false
        @pool.cyanuricliquidSize = @pool.cyanuricliquidSize.to_d - (@dosage.cyanuricliquidSize.to_d)/@nonSpaBoxUnit
        if @pool.cyanuricliquidSize.to_d < 0
          @pool.cyanuricliquidSize = 0
        end
      end

      #Extra chemicals section ends

      if @pool.trichlor3Size.blank? == false && @dosage.trichlor.blank? == false
        @pool.trichlor3Size = @pool.trichlor3Size.to_d - (@dosage.trichlor.to_d*0.04375)
        if @pool.trichlor3Size.to_d < 0
          @pool.trichlor3Size = 0
        end
      end
      if @pool.sodaashSize.blank? == false && @dosage.phUp.blank? == false
        @pool.sodaashSize = @pool.sodaashSize.to_d - (@dosage.phUp.to_d)*@soda_ash_cup
        if @pool.sodaashSize.to_d < 0
          @pool.sodaashSize = 0
        end
      end
      if @pool.dryacidSize.blank? == false && @dosage.phDown.blank? == false
        @pool.dryacidSize = @pool.dryacidSize.to_d - (@dosage.phDown.to_d)/@nonSpaBoxUnit
        if @pool.dryacidSize.to_d < 0
          @pool.dryacidSize = 0
        end
      end
      if @pool.sodiumSize.blank? == false && @dosage.taUp.blank? == false
        @pool.sodiumSize = @pool.sodiumSize.to_d - (@dosage.taUp.to_d)/@nonSpaBoxUnit
        if @pool.sodiumSize.to_d < 0
          @pool.sodiumSize = 0
        end
      end
      if @pool.bleach6Size.blank? == false && @dosage.liquidCl6.blank? == false
        if @dosage.lch6unit.downcase == "cup" && @dosage.lch6qunit.downcase == "cup"
          @pool.bleach6Size = @pool.bleach6Size.to_d - (@dosage.liquidCl6.to_d)/@bleach
        elsif @dosage.lch6unit.downcase == "gallon" && @dosage.lch6qunit.downcase == "gallon"
          @pool.bleach6Size = @pool.bleach6Size.to_d - (@dosage.liquidCl6.to_d)
        else
          @pool.bleach6Size = @pool.bleach6Size.to_d - (@dosage.liquidCl6.to_d)
        end
        if @pool.bleach6Size.to_d < 0
          @pool.bleach6Size = 0
        end
      end
      if @pool.bleach8Size.blank? == false && @dosage.liquidCl8.blank? == false
        if @dosage.lch8unit.downcase == "cup" && @dosage.lch8qunit.downcase == "cup"
          @pool.bleach8Size = @pool.bleach8Size.to_d - (@dosage.liquidCl8.to_d)/@bleach
        elsif @dosage.lch8unit.downcase == "gallon" && @dosage.lch8qunit.downcase == "gallon"
          @pool.bleach8Size = @pool.bleach8Size.to_d - (@dosage.liquidCl8.to_d)
        else
          @pool.bleach8Size = @pool.bleach8Size.to_d - (@dosage.liquidCl8.to_d)
        end
        if @pool.bleach8Size.to_d < 0
          @pool.bleach8Size = 0
        end
      end
      if @pool.bleach12Size.blank? == false && @dosage.liquidChlorine.blank? == false
        if @dosage.lch12unit.downcase == "cup" && @dosage.lch12qunit.downcase == "cup"
          @pool.bleach12Size = @pool.bleach12Size.to_d - (@dosage.liquidChlorine.to_d)/@bleach
        elsif @dosage.lch12unit.downcase == "gallon" && @dosage.lch12qunit.downcase == "gallon"
          @pool.bleach12Size = @pool.bleach12Size.to_d - (@dosage.liquidChlorine.to_d)
        else
          @pool.bleach12Size = @pool.bleach12Size.to_d - (@dosage.liquidChlorine.to_d)
        end
        if @pool.bleach12Size.to_d < 0
          @pool.bleach12Size = 0
        end
      end
      if @pool.dichlorSize.blank? == false && @dosage.dichlor.blank? == false
        #puts @pool.dichlorSize.to_d
        #puts @dosage.dichlor.to_d
        #puts @pool.dichlorSize.to_d - (@dosage.dichlor.to_d)/@nonSpaBoxUnit
        @pool.dichlorSize = @pool.dichlorSize.to_d - (@dosage.dichlor.to_d*@dichlor)/@nonSpaBoxUnit
        if @pool.dichlorSize.to_d < 0
          @pool.dichlorSize = 0
        end
      end
      if @pool.dichlor62Size.blank? == false && @dosage.dichlor62Size.blank? == false
        #puts @pool.dichlor62Size.to_d
        #puts @dosage.dichlor62Size.to_d
        #puts @pool.dichlor62Size.to_d - (@dosage.dichlor62Size.to_d)/@nonSpaBoxUnit
        @pool.dichlor62Size = @pool.dichlor62Size.to_d - (@dosage.dichlor62Size.to_d*@dichlor)/@nonSpaBoxUnit
        if @pool.dichlor62Size.to_d < 0
          @pool.dichlor62Size = 0
        end
      end
      if @pool.calhypo53Size.blank? == false && @dosage.calhypo53.blank? == false
        @pool.calhypo53Size = @pool.calhypo53Size.to_d - (@dosage.calhypo53.to_d)/@nonSpaBoxUnit
        if @pool.calhypo53Size.to_d < 0
          @pool.calhypo53Size = 0
        end
      end
      if @pool.calhypo65Size.blank? == false && @dosage.calhypo65.blank? == false
        @pool.calhypo65Size = @pool.calhypo65Size.to_d - (@dosage.calhypo65.to_d)/@nonSpaBoxUnit
        if @pool.calhypo65Size.to_d < 0
          @pool.calhypo65Size = 0
        end
      end
      if @pool.calhypo73Size.blank? == false && @dosage.calhypo73.blank? == false
        @pool.calhypo73Size = @pool.calhypo73Size.to_d - (@dosage.calhypo73.to_d)/@nonSpaBoxUnit
        if @pool.calhypo73Size.to_d < 0
          @pool.calhypo73Size = 0
        end
      end
      if @pool.boraxSize.blank? == false && @dosage.phUpBorax.blank? == false
        @pool.boraxSize = @pool.boraxSize.to_d - (@dosage.phUpBorax.to_d)/@nonSpaBoxUnit
        if @pool.boraxSize.to_d < 0
          @pool.boraxSize = 0
        end
      end
      if @pool.muriatic15Size.blank? == false && @dosage.muriatic15.blank? == false
        if @dosage.macid15unit.downcase == "cup" && @dosage.macid15qunit.downcase == "cup"
          @pool.muriatic15Size = @pool.muriatic15Size.to_d - (@dosage.muriatic15.to_d) / @muriatic
        elsif @dosage.macid15unit.downcase == "gallon" && @dosage.macid15qunit.downcase == "gallon"
          @pool.muriatic15Size = @pool.muriatic15Size.to_d - (@dosage.muriatic15.to_d)
        else
          @pool.muriatic15Size = @pool.muriatic15Size.to_d - (@dosage.muriatic15.to_d)
        end
        if @pool.muriatic15Size.to_d < 0
          @pool.muriatic15Size = 0
        end
      end
      if @pool.muriatic31Size.blank? == false && @dosage.muriatic31.blank? == false
        if @dosage.macid31unit.downcase == "cup" && @dosage.macid31qunit.downcase == "cup"
          @pool.muriatic31Size = @pool.muriatic31Size.to_d - (@dosage.muriatic31.to_d) / @muriatic
        elsif @dosage.macid31unit.downcase == "gallon" && @dosage.macid31qunit.downcase == "gallon"
          @pool.muriatic31Size = @pool.muriatic31Size.to_d - (@dosage.muriatic31.to_d)
        else
          @pool.muriatic31Size = @pool.muriatic31Size.to_d - (@dosage.muriatic31.to_d)
        end
        if @pool.muriatic31Size.to_d < 0
          @pool.muriatic31Size = 0
        end
      end

      #For spa
      if @pool.isPool == false && @pool.spabox
        if @pool.esanitizer.blank? == false && @dosage.esanitizer.blank? == false
          @pool.esanitizer = @pool.esanitizer.to_d - (@dosage.esanitizer.to_d*@esanitizer_pac)/@spabox
          if @pool.esanitizer.to_d < 0
            @pool.esanitizer = 0
          end
        end
        if @pool.ddryacid.blank? == false && @dosage.ddryacid.blank? == false
          @pool.ddryacid = @pool.ddryacid.to_d - (@dosage.ddryacid.to_d*@ddryacid_pac)/@spabox
          if @pool.ddryacid.to_d < 0
            @pool.ddryacid = 0
          end
        end
        if @pool.bbakingsoda.blank? == false && @dosage.bbakingsoda.blank? == false
          @pool.bbakingsoda = @pool.bbakingsoda.to_d - (@dosage.bbakingsoda.to_d*@bbakingsoda_pac)/@spabox
          if @pool.bbakingsoda.to_d < 0
            @pool.bbakingsoda = 0
          end
        end

        if @pool.bsodaash.blank? == false && @dosage.bsodaash.blank? == false
          @pool.bsodaash = @pool.bsodaash.to_d - (@dosage.bsodaash.to_d*@bsodaash_pac)/@spabox
          if @pool.bsodaash.to_d < 0
            @pool.bsodaash = 0
          end
        end
      end

      @pool.save
    end

    def getunits(value,unit)
      @temp_unit = ""
      if unit != "mL"
        unit = (unit.downcase).gsub(/\s+/, "")
      end
      if unit == "cup(s)" || unit == "cup" ||  unit == "cups"  ||  unit == "cups(s)"
        if value.to_d > 1
          @temp_unit = "cups"
        else
          @temp_unit = "cup"
        end
      elsif unit == "tablet(s)" || unit == "tablet" || unit == "tablets"
        if value.to_d > 1
          @temp_unit = "Tablets"
        else
          @temp_unit = "Tablet"
        end
      elsif unit  == "gallon(s)" || unit == "gallon" || unit == "gallons"
        if value.to_d > 1
          @temp_unit = "Gallons"
        else
          @temp_unit = "Gallon"
        end
      elsif unit == "packet(s)" || unit == "packet" || unit == "packets"
        if value.to_d > 1
          @temp_unit = "packets"
        else
          @temp_unit = "packet"
        end
      elsif unit == "tablespoon(s)" || unit == "tablespoon" || unit == "tablespoons"
        if value.to_d > 1
          @temp_unit = "tablespoons"
        else
          @temp_unit = "tablespoon"
        end
      elsif unit == "lbs"
        if value.to_d > 1
          @temp_unit = "lbs"
        else
          @temp_unit = "lb"
        end
      else
        if value.to_d > 1
          @temp_unit = unit.gsub("(","").gsub(")","")
        else
          @temp_unit = unit.gsub("(s)","")
        end
      end

      if @current_user_role.blank? == false && @current_user_role == "Lora" && (@temp_unit == "tablespoons" || @temp_unit == "tablespoon")
        @temp_unit = "tsp"
      end

      return @temp_unit
    end

    def generatedosagetext(dosage,pool)
      @units = "cup(s)"
      @nonSpaBoxUnit = "tablespoon(s)"
      @tablets = "Tablet(s)"
      @gallons = "Gallon(s)"
      @lbs = "lbs"
      if pool.isPool
        @units = "cup(s)"
        @nonSpaBoxUnit = "cup(s)"
      else
        @units = "packet(s)"
        @nonSpaBoxUnit = "cup(s)"
      end

      @skin = pool.chemical_skin

      @content = ""
      if @skin == "advanced"
        if dosage.trichlor.blank? == false && dosage.trichlor != "" && dosage.trichlor.to_d > 0
          if @content != ""
            @content += ", "
          end
          @tablets = getunits(dosage.trichlor,@tablets)
          @content += dosage.trichlor.to_s+' Trichlor 3\" tablets'
        end

        if dosage.phUp.blank? == false && dosage.phUp != "" && dosage.phUp.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUp,@nonSpaBoxUnit).to_s+" Soda Ash"
        end

        if dosage.phDown.blank? == false && dosage.phDown != "" && dosage.phDown.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phDown,@nonSpaBoxUnit).to_s+" Dry acid"
        end

        if dosage.taUp.blank? == false && dosage.taUp != "" && dosage.taUp.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.taUp,@nonSpaBoxUnit).to_s+" Sodium Bicarbonate"
        end

        if dosage.liquidCl6.blank? == false && dosage.liquidCl6 != "" && dosage.liquidCl6.to_d > 0
          if @content != ""
            @content += ", "
          end
          @s6_liquid = " 6% Liquid Chlorine "
          if pool.option
            @s6_liquid = " Liquid Chlorine "
          end
          @content += getchemtext(dosage.liquidCl6,dosage.lch6unit+"~"+dosage.lch6qunit).to_s+@s6_liquid
        end

        if dosage.liquidCl8.blank? == false && dosage.liquidCl8 != "" && dosage.liquidCl8.to_d > 0
          if @content != ""
            @content += ", "
          end
          @s8_liquid = " 8% Liquid Chlorine "
          if pool.option
            @s8_liquid = " Liquid Chlorine "
          end
          @content += getchemtext(dosage.liquidCl8,dosage.lch8unit+"~"+dosage.lch8qunit).to_s+@s8_liquid
        end

        if dosage.liquidChlorine.blank? == false && dosage.liquidChlorine != "" && dosage.liquidChlorine.to_d > 0
          if @content != ""
            @content += ", "
          end
          @s12_liquid = " 12% Liquid Chlorine "
          if pool.option
            @s12_liquid = " Liquid Chlorine "
          end
          @content += getchemtext(dosage.liquidChlorine,dosage.lch12unit+"~"+dosage.lch12qunit).to_s+@s12_liquid
        end

        if dosage.dichlor.blank? == false && dosage.dichlor != "" && dosage.dichlor.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c56_dichlor = " 56% Dichlor "
          if pool.option
            @c56_dichlor = " Dichlor "
          end
          @content += getchemtext(dosage.dichlor,@nonSpaBoxUnit).to_s+@c56_dichlor
        end

        if dosage.dichlor62Size.blank? == false && dosage.dichlor62Size != "" && dosage.dichlor62Size.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c62_dichlor = " 62% Dichlor "
          if pool.option
            @c62_dichlor = " Dichlor "
          end
          @content += getchemtext(dosage.dichlor62Size,@nonSpaBoxUnit).to_s+@c62_dichlor
        end

        if dosage.calhypo53.blank? == false && dosage.calhypo53 != "" && dosage.calhypo53.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c53_hypo = " 53% Cal-hypo "
          if pool.option
            @c53_hypo = " Cal-hypo "
          end
          @content += getchemtext(dosage.calhypo53,@lbs).to_s+@c53_hypo
        end

        if dosage.calhypo65.blank? == false && dosage.calhypo65 != "" && dosage.calhypo65.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c65_hypo = " 65% Cal-hypo "
          if pool.option
            @c65_hypo = " Cal-hypo "
          end
          @content += getchemtext(dosage.calhypo65,@lbs).to_s+@c65_hypo
        end

        if dosage.calhypo73.blank? == false && dosage.calhypo73 != "" && dosage.calhypo73.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c73_hypo = " 73% Cal-hypo "
          if pool.option
            @c73_hypo = " Cal-hypo "
          end
          @content += getchemtext(dosage.calhypo73,@lbs).to_s+@c73_hypo
        end

        if dosage.phUpBorax.blank? == false && dosage.phUpBorax != "" && dosage.phUpBorax.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUpBorax,@nonSpaBoxUnit).to_s+" borax"
        end

        if dosage.muriatic15.blank? == false && dosage.muriatic15 != "" && dosage.muriatic15.to_d > 0
          if @content != ""
            @content += ", "
          end
          @m73_acid = " 15.7% Muriatic Acid "
          if pool.option
            @m73_acid = " Muriatic Acid "
          end
          @content += getchemtext(dosage.muriatic15,dosage.macid15unit+"~"+dosage.macid15qunit).to_s+@m73_acid
        end

        if dosage.muriatic31.blank? == false && dosage.muriatic31 != "" && dosage.muriatic31.to_d > 0
          if @content != ""
            @content += ", "
          end
          @m31_acid = " 31% Muriatic Acid "
          if pool.option
            @m31_acid = " Muriatic Acid "
          end
          @content += getchemtext(dosage.muriatic31,dosage.macid31unit+"~"+dosage.macid31qunit).to_s+@m31_acid
        end

        if dosage.trichlor2Size.blank? == false && dosage.trichlor2Size != "" && dosage.trichlor2Size.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.trichlor2Size.to_s+' Trichlor 2\" tablets'
        end

        if dosage.bromine1Size.blank? == false && dosage.bromine1Size != "" && dosage.bromine1Size.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.bromine1Size.to_s+' Bromine 1\" tablets'
        end

        if dosage.lithiumhypoSize.blank? == false && dosage.lithiumhypoSize != "" && dosage.lithiumhypoSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.lithiumhypoSize,@nonSpaBoxUnit).to_s+" Lithium Hypo"
        end

        if dosage.calciumSize.blank? == false && dosage.calciumSize != "" && dosage.calciumSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.calciumSize,@nonSpaBoxUnit).to_s+" Calcium chloride"
        end

        if dosage.cyanuricsolidSize.blank? == false && dosage.cyanuricsolidSize != "" && dosage.cyanuricsolidSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricsolidSize,@nonSpaBoxUnit).to_s+" Cyanuric Acid Solid"
        end

        if dosage.cyanuricliquidSize.blank? == false && dosage.cyanuricliquidSize != "" && dosage.cyanuricliquidSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricliquidSize,@nonSpaBoxUnit).to_s+" Cyanuric Acid Liquid"
        end

        if dosage.esanitizer.blank? == false && dosage.esanitizer != "" && dosage.esanitizer.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.esanitizer.to_i,"packet(s)")
          @content += dosage.esanitizer.to_s+" "+@packet+" E"
        end

        if dosage.ddryacid.blank? == false && dosage.ddryacid != "" && dosage.ddryacid.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.ddryacid.to_i,"packet(s)")
          @content += dosage.ddryacid.to_s+" "+@packet+" D"
        end

        if dosage.bbakingsoda.blank? == false && dosage.bbakingsoda != "" && dosage.bbakingsoda.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bbakingsoda.to_i,"packet(s)")
          @content += dosage.bbakingsoda.to_s+" "+@packet+" A"
        end

        if dosage.bsodaash.blank? == false && dosage.bsodaash != "" && dosage.bsodaash.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bsodaash.to_i,"packet(s)")
          @content += dosage.bsodaash.to_s+" "+@packet+" B"
        end
      elsif @skin == "basic"
        if dosage.trichlor.blank? == false && dosage.trichlor != "" && dosage.trichlor.to_d > 0
          if @content != ""
            @content += ", "
          end
          @tablets = getunits(dosage.trichlor,@tablets)
          @content += dosage.trichlor.to_s+' '+getchemicaltype(pool,"Trichlor 3")+' tablets'
        end

        if dosage.phUp.blank? == false && dosage.phUp != "" && dosage.phUp.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUp,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Soda Ash")
        end

        if dosage.phDown.blank? == false && dosage.phDown != "" && dosage.phDown.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phDown,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Dry Acid")
        end

        if dosage.taUp.blank? == false && dosage.taUp != "" && dosage.taUp.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.taUp,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Sodium Bicarbonate")
        end

        if dosage.liquidCl6.blank? == false && dosage.liquidCl6 != "" && dosage.liquidCl6.to_d > 0
          if @content != ""
            @content += ", "
          end
          @s6_liquid = "6% Liquid Chlorine"
          if pool.option
            @s6_liquid = "Liquid Chlorine"
          end
          @content += getchemtext(dosage.liquidCl6,dosage.lch6unit+"~"+dosage.lch6qunit).to_s+" "+getchemicaltype(pool,@s6_liquid)
        end

        if dosage.liquidCl8.blank? == false && dosage.liquidCl8 != "" && dosage.liquidCl8.to_d > 0
          if @content != ""
            @content += ", "
          end
          @s8_liquid = "8% Liquid Chlorine"
          if pool.option
            @s8_liquid = "Liquid Chlorine"
          end
          @content += getchemtext(dosage.liquidCl8,dosage.lch8unit+"~"+dosage.lch8qunit).to_s+" "+getchemicaltype(pool,@s8_liquid)
        end

        if dosage.liquidChlorine.blank? == false && dosage.liquidChlorine != "" && dosage.liquidChlorine.to_d > 0
          if @content != ""
            @content += ", "
          end
          @s12_liquid = "12% Liquid Chlorine"
          if pool.option
            @s12_liquid = "Liquid Chlorine"
          end
          @content += getchemtext(dosage.liquidChlorine,dosage.lch12unit+"~"+dosage.lch12qunit).to_s+" "+getchemicaltype(pool,@s12_liquid)
        end

        if dosage.dichlor.blank? == false && dosage.dichlor != "" && dosage.dichlor.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c56_dichlor = "56% Dichlor"
          if pool.option
            @c56_dichlor = "Dichlor"
          end
          @content += getchemtext(dosage.dichlor,@units).to_s+" "+getchemicaltype(pool,@c56_dichlor)
        end

        if dosage.dichlor62Size.blank? == false && dosage.dichlor62Size != "" && dosage.dichlor62Size.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c62_dichlor = "62% Dichlor"
          if pool.option
            @c62_dichlor = "Dichlor"
          end
          @content += getchemtext(dosage.dichlor62Size,@units).to_s+" "+getchemicaltype(pool,@c62_dichlor)
        end

        if dosage.calhypo53.blank? == false && dosage.calhypo53 != "" && dosage.calhypo53.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c53_hypo = "53% Cal-hypo"
          if pool.option
            @c53_hypo = "Cal-hypo"
          end
          @content += getchemtext(dosage.calhypo53,@lbs).to_s+" "+getchemicaltype(pool,@c53_hypo)
        end

        if dosage.calhypo65.blank? == false && dosage.calhypo65 != "" && dosage.calhypo65.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c65_hypo = "65% Cal-hypo"
          if pool.option
            @c65_hypo = "Cal-hypo"
          end
          @content += getchemtext(dosage.calhypo65,@lbs).to_s+" "+getchemicaltype(pool,@c65_hypo)
        end

        if dosage.calhypo73.blank? == false && dosage.calhypo73 != "" && dosage.calhypo73.to_d > 0
          if @content != ""
            @content += ", "
          end
          @c73_hypo = "73% Cal-hypo"
          if pool.option
            @c73_hypo = "Cal-hypo"
          end
          @content += getchemtext(dosage.calhypo73,@lbs).to_s+" "+getchemicaltype(pool,@c73_hypo)
        end

        if dosage.phUpBorax.blank? == false && dosage.phUpBorax != "" && dosage.phUpBorax.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.phUpBorax,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Borax")
        end

        if dosage.muriatic15.blank? == false && dosage.muriatic15 != "" && dosage.muriatic15.to_d > 0
          if @content != ""
            @content += ", "
          end
          @m73_acid = "15.7% Muriatic Acid"
          if pool.option
            @m73_acid = "Muriatic Acid"
          end
          @content += getchemtext(dosage.muriatic15,dosage.macid15unit+"~"+dosage.macid15qunit).to_s+" "+getchemicaltype(pool,@m73_acid)
        end

        if dosage.muriatic31.blank? == false && dosage.muriatic31 != "" && dosage.muriatic31.to_d > 0
          if @content != ""
            @content += ", "
          end
          @m31_acid = "31% Muriatic Acid"
          if pool.option
            @m31_acid = "Muriatic Acid"
          end
          @content += getchemtext(dosage.muriatic31,dosage.macid31unit+"~"+dosage.macid31qunit).to_s+" "+getchemicaltype(pool,@m31_acid)
        end

        if dosage.trichlor2Size.blank? == false && dosage.trichlor2Size != "" && dosage.trichlor2Size.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.trichlor2Size.to_s+" "+getchemicaltype(pool,'Trichlor')
        end

        if dosage.bromine1Size.blank? == false && dosage.bromine1Size != "" && dosage.bromine1Size.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += dosage.bromine1Size.to_s+" "+getchemicaltype(pool,'Bromine')
        end

        if dosage.lithiumhypoSize.blank? == false && dosage.lithiumhypoSize != "" && dosage.lithiumhypoSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.lithiumhypoSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Lithium Hypo")
        end

        if dosage.calciumSize.blank? == false && dosage.calciumSize != "" && dosage.calciumSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.calciumSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Calcium chloride")
        end

        if dosage.cyanuricsolidSize.blank? == false && dosage.cyanuricsolidSize != "" && dosage.cyanuricsolidSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricsolidSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Cyanuric Acid Solid")
        end

        if dosage.cyanuricliquidSize.blank? == false && dosage.cyanuricliquidSize != "" && dosage.cyanuricliquidSize.to_d > 0
          if @content != ""
            @content += ", "
          end
          @content += getchemtext(dosage.cyanuricliquidSize,@nonSpaBoxUnit).to_s+" "+getchemicaltype(pool,"Cyanuric Acid Liquid")
        end

        if dosage.esanitizer.blank? == false && dosage.esanitizer != "" && dosage.esanitizer.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.esanitizer.to_i,"packet(s)")
          @content += dosage.esanitizer.to_s+" "+@packet+" "+getchemicaltype(pool,"E")
        end

        if dosage.ddryacid.blank? == false && dosage.ddryacid != "" && dosage.ddryacid.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.ddryacid.to_i,"packet(s)")
          @content += dosage.ddryacid.to_s+" "+@packet+" "+getchemicaltype(pool,"D")
        end

        if dosage.bbakingsoda.blank? == false && dosage.bbakingsoda != "" && dosage.bbakingsoda.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bbakingsoda.to_i,"packet(s)")
          @content += dosage.bbakingsoda.to_s+" "+@packet+" "+getchemicaltype(pool,"A")
        end

        if dosage.bsodaash.blank? == false && dosage.bsodaash != "" && dosage.bsodaash.to_d > 0
          if @content != ""
            @content += ", "
          end
          @packet = getunits(dosage.bsodaash.to_i,"packet(s)")
          @content += dosage.bsodaash.to_s+" "+@packet+" "+getchemicaltype(pool,"B")
        end
      end
      puts "Dosage %%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      puts @content
      puts "Dosage %%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      return @content
    end

    def getchemtext(value,unit)
      #puts unit
      #check if full and quarter units are present
      if unit.split("~").count > 1
        @parts = unit.split("~")
        @unitf = @parts[0]+"(s)"
        @unitq = @parts[1]+"(s)"
      else
        @unitf = unit
        @unitq = unit
      end

      @text = ""
      @f = getfull(value)
      @q = getquarters(value)
      @t = gettablespoons(value)
      if @f.blank? == false && @f > 0
        @temp_f = getunits(@f.to_i,@unitf)
        @text = @f.to_s+" #{@temp_f}"
      end
      if @q.blank? == false && @q > 0
        if @text.blank?
          if  @q == 2
            @temp_unit_ = getunits(1,@unitq)
            @text += "a half "+@temp_unit
          else
            @temp_unit_ = getunits(@q.to_i,@unitq)
            @text = @q.to_s+" quarter #{@temp_unit_}"
          end
        else
          if @t.blank? || @t <= 0
            if  @q == 2
              @temp_u = getunits(@f.to_i,@unitf)
              @text = @f.to_s+" #{@temp_u}"
              @temp_unit = getunits(1,@unitq)
              @text += " and a half "+@temp_unit
            else
              @temp_q = getunits(@q.to_i,@unitq)
              @text += " and "+@q.to_s+" quarter #{@temp_q}"
            end
          else
            @temp_unitq = getunits(@q.to_i,@unitq)
            @text += ", "+@q.to_s+" quarter #{@temp_unitq}"
          end
        end
      end
      if @t.blank? == false && @t > 0
        if @text.blank?
          @temp_tablespoon = getunits(@t.to_i,"tablespoon(s)")
          @text = @t.to_s+" "+@temp_tablespoon
        else
          @tablespoon = getunits(@t.to_i,"tablespoon(s)")
          @text+= " and "+@t.to_s+" "+@tablespoon
        end
      end
      return @text
    end

    def getfull(value)
      @temptbl = 0
      @temptbl = (value.to_d).div 1
      #@parts = @temptbl.to_s.split(".")
      #@temptbl = @parts.count > 1 ? @parts[0].to_s : 0
      return @temptbl.to_i
    end

    def getquarters(value)
      @temptbl = 0
      @temptbl = ((value.to_d) % 1 / 0.25)
      @parts = @temptbl.to_s.split(".")
      @temptbl = @parts.count > 1 ? @parts[0].to_s : 0
      return @temptbl.to_i
    end

    def gettablespoons(value)
      @temptbl = 0
      @temptbl = (((value.to_d) % 1 % 0.25)*16)
      @parts = @temptbl.to_s.split(".")
      @temptbl = @parts.count > 1 ? @parts[0].to_s : 0
      return @temptbl.to_i
    end

    #return last used acid
    def getlastusedacid(pool)
      @pool = pool
      @lastdose = @pool.dosages.last
      @type = 0
      if @lastdose.blank? == false
        if @lastdose.phDown.blank? == false && @lastdose.phDown.to_d > 0
          @type = 1
        elsif @lastdose.muriatic31.blank? == false && @lastdose.muriatic31.to_d > 0
          @type = 2
        end
      end
      return @type
    end
  end
end
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :quick_test do
    collection do
      post 'test'
      post 'old_test'
    end
  end
end

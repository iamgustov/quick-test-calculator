class CreatePools < ActiveRecord::Migration[5.1]
  def change
    create_table :pools do |t|
      t.string   "streetAddress"
      t.string   "city"
      t.string   "zipCode"
      t.string   "wifiName"
      t.string   "wifiPassword"
      t.boolean  "isPool"
      t.string   "waterChemistry"
      t.string   "sanitizer"
      t.string   "phIncreaser"
      t.string   "phReducer"
      t.string   "otherChemicals"
      t.integer  "volume"
      t.string   "pumpModel"
      t.string   "pumpManufacturer"
      t.string   "heaterManufacturer"
      t.string   "heaterModel"
      t.string   "filterManufacturer"
      t.string   "filterModel"
      t.boolean  "cover"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "state"
      t.string   "finishType"
      t.integer  "customer_id"
      t.string   "photos"
      t.integer  "chemicalFloaters"
      t.string   "poolType"
      t.string   "saltPoundage"
      t.boolean  "bleach6"
      t.decimal  "bleach6Size",        precision: 10, scale: 2
      t.boolean  "bleach8"
      t.decimal  "bleach8Size",        precision: 10, scale: 2
      t.boolean  "bleach12"
      t.decimal  "bleach12Size",       precision: 10, scale: 2
      t.boolean  "trichlor3"
      t.decimal  "trichlor3Size",      precision: 10, scale: 2
      t.boolean  "trichlor2"
      t.decimal  "trichlor2Size",      precision: 10, scale: 2
      t.boolean  "bromine1"
      t.decimal  "bromine1Size",       precision: 10, scale: 2
      t.boolean  "dichlor"
      t.decimal  "dichlorSize",        precision: 10, scale: 2
      t.boolean  "calhypo53"
      t.decimal  "calhypo53Size",      precision: 10, scale: 2
      t.boolean  "calhypo73"
      t.decimal  "calhypo73Size",      precision: 10, scale: 2
      t.boolean  "lithiumhypo"
      t.decimal  "lithiumhypoSize",    precision: 10, scale: 2
      t.boolean  "muriatic31"
      t.decimal  "muriatic31Size",     precision: 10, scale: 2
      t.boolean  "muriatic15"
      t.decimal  "muriatic15Size",     precision: 10, scale: 2
      t.boolean  "dryacid"
      t.decimal  "dryacidSize",        precision: 10, scale: 2
      t.boolean  "sodaash"
      t.decimal  "sodaashSize",        precision: 10, scale: 2
      t.boolean  "borax"
      t.decimal  "boraxSize",          precision: 10, scale: 2
      t.boolean  "sodium",                                      default: false
      t.decimal  "sodiumSize",         precision: 10, scale: 2
      t.boolean  "calcium",                                     default: false
      t.decimal  "calciumSize",        precision: 10, scale: 2
      t.boolean  "cyanuricsolid",                               default: false
      t.decimal  "cyanuricsolidSize",  precision: 10, scale: 2
      t.boolean  "cyanuricliquid",                              default: false
      t.decimal  "cyanuricliquidSize", precision: 10, scale: 2
      t.boolean  "spabox"
      t.decimal  "esanitizer",         precision: 10, scale: 2
      t.decimal  "ddryacid",           precision: 10, scale: 2
      t.decimal  "bbakingsoda",        precision: 10, scale: 2
      t.boolean  "calhypo65"
      t.decimal  "calhypo65Size",      precision: 10, scale: 2
      t.string   "water_change_time"
      t.boolean  "ozonator"
      t.boolean  "dichlor62"
      t.decimal  "dichlor62Size",      precision: 10, scale: 2
      t.decimal  "bsodaash",           precision: 10, scale: 2
      t.boolean  "option",                                      default: false
      t.boolean  "bakingsoda",                                  default: false
      t.string   "bakingsodaSize"
      t.string   "chemical_skin",                               default: "basic"
      t.timestamps
    end
  end
end

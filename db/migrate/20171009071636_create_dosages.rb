class CreateDosages < ActiveRecord::Migration[5.1]
  def change
    create_table :dosages do |t|
      t.integer  "customer_id"
      t.integer  "pool_id"
      t.string   "trichlor"
      t.string   "phUp"
      t.string   "phDown"
      t.string   "taUp"
      t.string   "liquidCl6"
      t.string   "liquidCl8"
      t.string   "liquidChlorine"
      t.string   "dichlor"
      t.string   "calhypo53"
      t.string   "calhypo65"
      t.string   "calhypo73"
      t.string   "phUpBorax"
      t.string   "muriatic15"
      t.string   "muriatic31"
      t.boolean  "status"
      t.text     "prescription"
      t.string   "trichlor2Size"
      t.string   "bromine1Size"
      t.string   "lithiumhypoSize"
      t.string   "calciumSize"
      t.string   "cyanuricsolidSize"
      t.string   "cyanuricliquidSize"
      t.string   "esanitizer"
      t.string   "ddryacid"
      t.string   "bbakingsoda"
      t.float    "ph",                 limit: 24
      t.float    "orp",                limit: 24
      t.float    "ph_complete",        limit: 24
      t.float    "orp_complete",       limit: 24
      t.string   "dichlor62Size"
      t.string   "bsodaash"
      t.boolean  "approved"
      t.string   "lch6unit",                      default: "gallon"
      t.string   "lch8unit",                      default: "gallon"
      t.string   "lch12unit",                     default: "gallon"
      t.string   "macid15unit",                   default: "gallon"
      t.string   "macid31unit",                   default: "gallon"
      t.string   "lch6qunit",                     default: "gallon"
      t.string   "lch8qunit",                     default: "gallon"
      t.string   "lch12qunit",                    default: "gallon"
      t.string   "macid15qunit",                  default: "gallon"
      t.string   "macid31qunit",                  default: "gallon"
      t.boolean  "saltwater",                     default: false
      t.timestamps
    end
  end
end

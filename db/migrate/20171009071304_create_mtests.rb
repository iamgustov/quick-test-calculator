class CreateMtests < ActiveRecord::Migration[5.1]
  def change
    create_table :mtests do |t|
      t.references :pool, index: true
      t.float :pH
      t.string :freeCl
      t.float :combinedCl
      t.integer :totalAlkalinity
      t.integer :calciumHardness
      t.integer :cyanuricAcid
      t.timestamps
    end
  end
end

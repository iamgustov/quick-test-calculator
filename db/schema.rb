# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171009071636) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dosages", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "pool_id"
    t.string "trichlor"
    t.string "phUp"
    t.string "phDown"
    t.string "taUp"
    t.string "liquidCl6"
    t.string "liquidCl8"
    t.string "liquidChlorine"
    t.string "dichlor"
    t.string "calhypo53"
    t.string "calhypo65"
    t.string "calhypo73"
    t.string "phUpBorax"
    t.string "muriatic15"
    t.string "muriatic31"
    t.boolean "status"
    t.text "prescription"
    t.string "trichlor2Size"
    t.string "bromine1Size"
    t.string "lithiumhypoSize"
    t.string "calciumSize"
    t.string "cyanuricsolidSize"
    t.string "cyanuricliquidSize"
    t.string "esanitizer"
    t.string "ddryacid"
    t.string "bbakingsoda"
    t.float "ph"
    t.float "orp"
    t.float "ph_complete"
    t.float "orp_complete"
    t.string "dichlor62Size"
    t.string "bsodaash"
    t.boolean "approved"
    t.string "lch6unit", default: "gallon"
    t.string "lch8unit", default: "gallon"
    t.string "lch12unit", default: "gallon"
    t.string "macid15unit", default: "gallon"
    t.string "macid31unit", default: "gallon"
    t.string "lch6qunit", default: "gallon"
    t.string "lch8qunit", default: "gallon"
    t.string "lch12qunit", default: "gallon"
    t.string "macid15qunit", default: "gallon"
    t.string "macid31qunit", default: "gallon"
    t.boolean "saltwater", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mtests", force: :cascade do |t|
    t.bigint "pool_id"
    t.float "pH"
    t.string "freeCl"
    t.float "combinedCl"
    t.integer "totalAlkalinity"
    t.integer "calciumHardness"
    t.integer "cyanuricAcid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pool_id"], name: "index_mtests_on_pool_id"
  end

  create_table "pools", force: :cascade do |t|
    t.string "streetAddress"
    t.string "city"
    t.string "zipCode"
    t.string "wifiName"
    t.string "wifiPassword"
    t.boolean "isPool"
    t.string "waterChemistry"
    t.string "sanitizer"
    t.string "phIncreaser"
    t.string "phReducer"
    t.string "otherChemicals"
    t.integer "volume"
    t.string "pumpModel"
    t.string "pumpManufacturer"
    t.string "heaterManufacturer"
    t.string "heaterModel"
    t.string "filterManufacturer"
    t.string "filterModel"
    t.boolean "cover"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
    t.string "finishType"
    t.integer "customer_id"
    t.string "photos"
    t.integer "chemicalFloaters"
    t.string "poolType"
    t.string "saltPoundage"
    t.boolean "bleach6"
    t.decimal "bleach6Size", precision: 10, scale: 2
    t.boolean "bleach8"
    t.decimal "bleach8Size", precision: 10, scale: 2
    t.boolean "bleach12"
    t.decimal "bleach12Size", precision: 10, scale: 2
    t.boolean "trichlor3"
    t.decimal "trichlor3Size", precision: 10, scale: 2
    t.boolean "trichlor2"
    t.decimal "trichlor2Size", precision: 10, scale: 2
    t.boolean "bromine1"
    t.decimal "bromine1Size", precision: 10, scale: 2
    t.boolean "dichlor"
    t.decimal "dichlorSize", precision: 10, scale: 2
    t.boolean "calhypo53"
    t.decimal "calhypo53Size", precision: 10, scale: 2
    t.boolean "calhypo73"
    t.decimal "calhypo73Size", precision: 10, scale: 2
    t.boolean "lithiumhypo"
    t.decimal "lithiumhypoSize", precision: 10, scale: 2
    t.boolean "muriatic31"
    t.decimal "muriatic31Size", precision: 10, scale: 2
    t.boolean "muriatic15"
    t.decimal "muriatic15Size", precision: 10, scale: 2
    t.boolean "dryacid"
    t.decimal "dryacidSize", precision: 10, scale: 2
    t.boolean "sodaash"
    t.decimal "sodaashSize", precision: 10, scale: 2
    t.boolean "borax"
    t.decimal "boraxSize", precision: 10, scale: 2
    t.boolean "sodium", default: false
    t.decimal "sodiumSize", precision: 10, scale: 2
    t.boolean "calcium", default: false
    t.decimal "calciumSize", precision: 10, scale: 2
    t.boolean "cyanuricsolid", default: false
    t.decimal "cyanuricsolidSize", precision: 10, scale: 2
    t.boolean "cyanuricliquid", default: false
    t.decimal "cyanuricliquidSize", precision: 10, scale: 2
    t.boolean "spabox"
    t.decimal "esanitizer", precision: 10, scale: 2
    t.decimal "ddryacid", precision: 10, scale: 2
    t.decimal "bbakingsoda", precision: 10, scale: 2
    t.boolean "calhypo65"
    t.decimal "calhypo65Size", precision: 10, scale: 2
    t.string "water_change_time"
    t.boolean "ozonator"
    t.boolean "dichlor62"
    t.decimal "dichlor62Size", precision: 10, scale: 2
    t.decimal "bsodaash", precision: 10, scale: 2
    t.boolean "option", default: false
    t.boolean "bakingsoda", default: false
    t.string "bakingsodaSize"
    t.string "chemical_skin", default: "basic"
  end

end
